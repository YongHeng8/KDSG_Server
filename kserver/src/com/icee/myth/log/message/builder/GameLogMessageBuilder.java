/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.log.message.builder;

import com.icee.myth.common.charInfo.CharDetailInfo;
import com.icee.myth.log.message.DBBehaviorGameLogMessage;
import com.icee.myth.log.message.DBSilverGameLogMessage;
import com.icee.myth.log.message.DBCashGameLogMessage;
import com.icee.myth.log.message.DBCreateCharGameLogMessage;
import com.icee.myth.log.message.FileDBDownCacheGameLogMessage;
import com.icee.myth.log.message.DBLoginLogoutGameLogMessage;
import com.icee.myth.log.message.DBOnlineNumGameLogMessage;
import com.icee.myth.log.message.DBGoldGameLogMessage;
import com.icee.myth.log.message.FileCYGameLogMessage;
import com.icee.myth.log.message.FileDBErrorGameLogMessage;
import com.icee.myth.log.message.FileDebugGameLogMessage;
import com.icee.myth.log.message.FileNetErrorGameLogMessage;
import com.icee.myth.log.message.GameLogMessage;
import com.icee.myth.log.message.GameLogMessage.GameLogType;
import com.icee.myth.protobuf.DBDownCacheProtocol.BillDBDownCacheDataProto;
import com.icee.myth.protobuf.DBDownCacheProtocol.CharInfoDBDownCacheDataProto;
import com.icee.myth.protobuf.DBDownCacheProtocol.CharOccupyInfoDBDownCacheDataProto;
import com.icee.myth.protobuf.DBDownCacheProtocol.ConsumerLogDBDownCacheDataProto;
import com.icee.myth.protobuf.DBDownCacheProtocol.MailDBDownCacheDataProto;
import com.icee.myth.protobuf.DBDownCacheProtocol.CouponDBDownCacheDataProto;
import com.icee.myth.protobuf.DBDownCacheProtocol.DBDownCacheProto;
import com.icee.myth.protobuf.DBDownCacheProtocol.RelationDBDownCacheDataProto;
import com.icee.myth.server.GameServer;
import com.icee.myth.server.message.dbMessage.ConsumerLogDBMessage;
import com.icee.myth.server.message.dbMessage.GetCouponDBMessage;
import com.icee.myth.server.message.dbMessage.SaveBillDBMessage;
import com.icee.myth.server.message.dbMessage.SaveCharInfoDBMessage;
import com.icee.myth.server.message.dbMessage.SaveCharOccupyInfoDBMessage;
import com.icee.myth.server.message.dbMessage.SaveMailDBMessage;
import com.icee.myth.server.message.dbMessage.SaveRelationDBMessage;
import com.icee.myth.utils.Consts;
import java.util.List;

/**
 *
 * @author liuxianke
 */
public class GameLogMessageBuilder {

    public static GameLogMessage buildFileDebugGameLogMessage(FileDebugGameLogMessage.DebugLogType type, String content) {
        if (type.compareTo(FileDebugGameLogMessage.LOGLEVEL) <= 0) {
            return new FileDebugGameLogMessage(type, content);
        } else {
            return null;
        }
    }

    public static GameLogMessage buildFileDBErrorGameLogMessage(String content) {
        return new FileDBErrorGameLogMessage(content);
    }

    public static GameLogMessage buildFileNetErrorGameLogMessage(String content) {
        return new FileNetErrorGameLogMessage(content);
    }

    public static GameLogMessage buildFileCYGameLogMessage(List<String> content, GameLogType gameLogType) {
        return new FileCYGameLogMessage(content, gameLogType);
    }

    public static GameLogMessage buildDBCashGameLogMessage(int playerId, int cash) {
        return new DBCashGameLogMessage(playerId, cash, GameServer.INSTANCE.getCurrentTime());
    }

    public static GameLogMessage buildDBCreateCharGameLogMessage(int playerId, int leaderCardId) {
        return new DBCreateCharGameLogMessage(playerId, leaderCardId, GameServer.INSTANCE.getCurrentTime());
    }

    public static GameLogMessage buildDBLoginLogoutGameLogMessage(int playerId, int lv, boolean isLogin, int ip, long duration) {
        return new DBLoginLogoutGameLogMessage(playerId, lv, isLogin, ip, duration, GameServer.INSTANCE.getCurrentTime());
    }

    public static GameLogMessage buildDBOnlineNumGameLogMessage(int currentNum) {
        return new DBOnlineNumGameLogMessage(currentNum, GameServer.INSTANCE.getCurrentTime());
    }

    public static GameLogMessage buildDBGoldGameLogMessage(int playerId, int mainType, int subType, int change1, int change2, int remain1, int remain2, int lv) {
        return new DBGoldGameLogMessage(playerId, mainType, subType, change1, change2, remain1, remain2, lv, GameServer.INSTANCE.getCurrentTime());
    }

    public static GameLogMessage buildDBSilverGameLogMessage(int playerId, int mainType, int subType, int change, long remain) {
        return new DBSilverGameLogMessage(playerId, mainType, subType, change, remain, GameServer.INSTANCE.getCurrentTime());
    }

    // =================== DB Down Cache Log ====================//
    public static GameLogMessage buildSaveCharInfoDBDownCacheGameLogMessage(SaveCharInfoDBMessage message) {
        CharDetailInfo charInfo = message.charDetailInfo;
        DBDownCacheProto.Builder builder1 = DBDownCacheProto.newBuilder();
        builder1.setType(Consts.DB_DOWN_CACHE_MESSAGE_TYPE_CHARINFO);
        builder1.setTime(GameServer.INSTANCE.getCurrentTime());

        CharInfoDBDownCacheDataProto.Builder builder2 = CharInfoDBDownCacheDataProto.newBuilder();
        builder2.setId(charInfo.mid);
        builder2.setLv(charInfo.level);
        builder2.setExp(charInfo.experience);
        builder2.setRankLv(charInfo.rankLevel);
        builder2.setRankExp(charInfo.rankExperience);
        builder2.setGold1(charInfo.gold1);
        builder2.setGold2(charInfo.gold2);
        builder2.setSilver(charInfo.silver);
        builder2.setEnergy(charInfo.energy);
        builder2.setToken(charInfo.token);
        builder2.setLeaderCardId(charInfo.leaderCardId);
        builder2.setLeaderCardLevel(charInfo.leaderCardLevel);
        builder2.setVipLevel(charInfo.vipLevel);
        builder2.setVipExperience(charInfo.vipExperience);
        builder2.setMaxPower(charInfo.maxPower);
        builder2.setDetail(charInfo.detail);
        builder2.setMailInfo(charInfo.mailInfo);
        builder2.setLeaveTime(charInfo.leaveTime);
        builder2.setTotalOnlineTime(charInfo.totalOnlineTime);

        builder1.setData(builder2.build().toByteString());
        return new FileDBDownCacheGameLogMessage(builder1.build().toByteArray());
    }

    public static GameLogMessage buildSaveCharOccupyInfoDBDownCacheGameLogMessage(SaveCharOccupyInfoDBMessage message) {
        DBDownCacheProto.Builder builder1 = DBDownCacheProto.newBuilder();
        builder1.setType(Consts.DB_DOWN_CACHE_MESSAGE_TYPE_CHAROCCUPYINFO);
        builder1.setTime(GameServer.INSTANCE.getCurrentTime());

        CharOccupyInfoDBDownCacheDataProto.Builder builder2 = CharOccupyInfoDBDownCacheDataProto.newBuilder();
        builder2.setId(message.playerId);
        builder2.setOccupyInfo(message.occupyInfoProto);

        builder1.setData(builder2.build().toByteString());
        return new FileDBDownCacheGameLogMessage(builder1.build().toByteArray());
    }

    public static GameLogMessage buildGetCouponDBDownCacheGameLogMessage(GetCouponDBMessage couponDBMessage) {
        DBDownCacheProto.Builder builder1 = DBDownCacheProto.newBuilder();
        builder1.setType(Consts.DB_DOWN_CACHE_MESSAGE_TYPE_COUPON);
        builder1.setTime(GameServer.INSTANCE.getCurrentTime());

        CouponDBDownCacheDataProto.Builder builder2 = CouponDBDownCacheDataProto.newBuilder();
        builder2.setPlayerId(couponDBMessage.playerId);
        builder2.setPassport(couponDBMessage.passport);
        builder2.setCode(couponDBMessage.code);

        builder1.setData(builder2.build().toByteString());
        return new FileDBDownCacheGameLogMessage(builder1.build().toByteArray());
    }

    public static GameLogMessage buildSaveRelationDBDownCacheGameLogMessage(SaveRelationDBMessage message) {
        DBDownCacheProto.Builder builder1 = DBDownCacheProto.newBuilder();
        builder1.setType(Consts.DB_DOWN_CACHE_MESSAGE_TYPE_RELATION);
        builder1.setTime(GameServer.INSTANCE.getCurrentTime());

        RelationDBDownCacheDataProto.Builder builder2 = RelationDBDownCacheDataProto.newBuilder();
        builder2.setId(message.playerId);
        builder2.setRelation(message.relationProto);

        builder1.setData(builder2.build().toByteString());
        return new FileDBDownCacheGameLogMessage(builder1.build().toByteArray());
    }

    public static GameLogMessage buildSaveBillDBDownCacheGameLogMessage(SaveBillDBMessage saveBillDBMessage) {
        DBDownCacheProto.Builder builder1 = DBDownCacheProto.newBuilder();
        builder1.setType(Consts.DB_DOWN_CACHE_MESSAGE_TYPE_BILL);
        builder1.setTime(GameServer.INSTANCE.getCurrentTime());

        BillDBDownCacheDataProto.Builder builder2 = BillDBDownCacheDataProto.newBuilder();
        builder2.setPlayerId(saveBillDBMessage.playerId);
        builder2.setPassport(saveBillDBMessage.passport);
        builder2.setOrderno(saveBillDBMessage.order.no);
        builder2.setCurrencyId(saveBillDBMessage.order.currencyId);
        builder2.setAmount((int) saveBillDBMessage.order.amount);
        builder2.setMemo(saveBillDBMessage.order.memo);
        builder2.setStatus(saveBillDBMessage.status);
        builder2.setStep(saveBillDBMessage.step);

        builder1.setData(builder2.build().toByteString());
        return new FileDBDownCacheGameLogMessage(builder1.build().toByteArray());
    }

    public static GameLogMessage buildSaveConsumerLogDBDownCacheGameLogMessage(ConsumerLogDBMessage consumerLogDBMessage) {
        DBDownCacheProto.Builder builder1 = DBDownCacheProto.newBuilder();
        builder1.setType(Consts.DB_DOWN_CACHE_MESSAGE_TYPE_CONSUMRE_LOG);
        builder1.setTime(GameServer.INSTANCE.getCurrentTime());

        ConsumerLogDBDownCacheDataProto.Builder builder2 = ConsumerLogDBDownCacheDataProto.newBuilder();
        builder2.setPlayerId(consumerLogDBMessage.playerId);
        builder2.setConsumertype(consumerLogDBMessage.consumerType);
        builder2.setGoldnum(consumerLogDBMessage.goldNum);
        builder2.setGoldtype(consumerLogDBMessage.goldType);
        builder2.setProductid(consumerLogDBMessage.productId);
        builder2.setProductnum(consumerLogDBMessage.productNum);

        builder1.setData(builder2.build().toByteString());
        return new FileDBDownCacheGameLogMessage(builder1.build().toByteArray());
    }

    public static GameLogMessage buildSaveMailDBDownCacheGameLogMessage(SaveMailDBMessage saveMailDBMessage) {
        DBDownCacheProto.Builder builder1 = DBDownCacheProto.newBuilder();
        builder1.setType(Consts.DB_DOWN_CACHE_MESSAGE_TYPE_MAIL);
        builder1.setTime(GameServer.INSTANCE.getCurrentTime());

        MailDBDownCacheDataProto.Builder builder2 = MailDBDownCacheDataProto.newBuilder();
        builder2.setMailId(saveMailDBMessage.mailId);
        builder2.setTargetId(saveMailDBMessage.targetId);
        builder2.setTitle(saveMailDBMessage.title);
        builder2.setDescription(saveMailDBMessage.description);
        if (saveMailDBMessage.rewardInfo != null) {
            builder2.setReward(saveMailDBMessage.rewardInfo);
        }

        builder1.setData(builder2.build().toByteString());
        return new FileDBDownCacheGameLogMessage(builder1.build().toByteArray());
    }

    // =================== behavior log =========================//
    public static GameLogMessage buildStrengthenCardDBBehaviorGameLogMessage(int playerId, int cardInstId, int foodId1, int foodId2, int foodId3, int foodId4, int foodId5, int foodId6, int foodId7, int foodId8, int totalEatExp) {
        return new DBBehaviorGameLogMessage(playerId, Consts.BEHAVIOR_LOG_TYPE_STRENGTHEN_CARD, GameServer.INSTANCE.getCurrentTime(), cardInstId, foodId1, foodId2, foodId3, foodId4, foodId5, foodId6, foodId7, foodId8, totalEatExp, 0, 0, null, null, null);
    }

    public static GameLogMessage buildTransformCardDBBehaviorGameLogMessage(int playerId, int cardInstId, int foodId1, int foodId2, int foodId3, int foodId4, int foodId5, int foodId6, int foodId7, int foodId8, int targetCardId) {
        return new DBBehaviorGameLogMessage(playerId, Consts.BEHAVIOR_LOG_TYPE_TRANSFORM_CARD, GameServer.INSTANCE.getCurrentTime(), cardInstId, foodId1, foodId2, foodId3, foodId4, foodId5, foodId6, foodId7, foodId8, targetCardId, 0, 0, null, null, null);
    }

    public static GameLogMessage buildSoldCardDBBehaviorGameLogMessage(int playerId, int cardInstId, int soldPrice) {
        return new DBBehaviorGameLogMessage(playerId, Consts.BEHAVIOR_LOG_TYPE_SOLD_CARD, GameServer.INSTANCE.getCurrentTime(), cardInstId, soldPrice, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, null, null, null);
    }

    public static GameLogMessage buildDrawCardDBBehaviorGameLogMessage(int playerId, int drawType, int cardInstId1, int cardInstId2, int cardInstId3, int cardInstId4, int cardInstId5, int cardInstId6) {
        return new DBBehaviorGameLogMessage(playerId, Consts.BEHAVIOR_LOG_TYPE_DRAW_CARD, GameServer.INSTANCE.getCurrentTime(), drawType, cardInstId1, cardInstId2, cardInstId3, cardInstId4, cardInstId5, cardInstId6, 0, 0, 0, 0, 0, null, null, null);
    }

    public static GameLogMessage buildEnterStageDBBehaviorGameLogMessage(int playerId, int stageType, int stageId, int helperCardId, int helperCardLever, byte[] sandbox) {
        return new DBBehaviorGameLogMessage(playerId, Consts.BEHAVIOR_LOG_TYPE_ENTER_STAGE, GameServer.INSTANCE.getCurrentTime(), stageType, stageId, helperCardId, helperCardLever, 0, 0, 0, 0, 0, 0, 0, 0, null, null, sandbox);
    }

    public static GameLogMessage buildLeaveStageDBBehaviorGameLogMessage(int playerId, int stageType, int stageId) {
        return new DBBehaviorGameLogMessage(playerId, Consts.BEHAVIOR_LOG_TYPE_LEAVE_STAGE, GameServer.INSTANCE.getCurrentTime(), stageType, stageId, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, null, null, null);
    }

    public static GameLogMessage buildFinishStageDBBehaviorGameLogMessage(int playerId, int stageType, int stageId) {
        return new DBBehaviorGameLogMessage(playerId, Consts.BEHAVIOR_LOG_TYPE_FINISH_STAGE, GameServer.INSTANCE.getCurrentTime(), stageType, stageId, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, null, null, null);
    }

    public static GameLogMessage buildStageRevivedDBBehaviorGameLogMessage(int playerId, int stageType, int stageId, int revivePrice) {
        return new DBBehaviorGameLogMessage(playerId, Consts.BEHAVIOR_LOG_TYPE_STAGE_REVIVED, GameServer.INSTANCE.getCurrentTime(), stageType, stageId, revivePrice, 0, 0, 0, 0, 0, 0, 0, 0, 0, null, null, null);
    }
    
    public static GameLogMessage buildPrivateTalkDBBehaviorGameLogMessage(int playerId, int targetId, String message) {
        return new DBBehaviorGameLogMessage(playerId, Consts.BEHAVIOR_LOG_TYPE_PRIVATE_TALK, GameServer.INSTANCE.getCurrentTime(), targetId, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, message, null, null);
    }

    public static GameLogMessage buildBroadcastDBBehaviorGameLogMessage(int playerId, String message) {
        return new DBBehaviorGameLogMessage(playerId, Consts.BEHAVIOR_LOG_TYPE_BROADCAST, GameServer.INSTANCE.getCurrentTime(), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, message, null, null);
    }

    public static GameLogMessage buildAddConcernDBBehaviorGameLogMessage(int playerId, int concernId) {
        return new DBBehaviorGameLogMessage(playerId, Consts.BEHAVIOR_LOG_TYPE_ADD_CONCERN, GameServer.INSTANCE.getCurrentTime(), concernId, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, null, null, null);
    }

    public static GameLogMessage buildGetPlayerInfoDBBehaviorGameLogMessage(int playerId, int targetId) {
        return new DBBehaviorGameLogMessage(playerId, Consts.BEHAVIOR_LOG_TYPE_GET_PLAYER_INFO, GameServer.INSTANCE.getCurrentTime(), targetId, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, null, null, null);
    }

    public static GameLogMessage buildLevelupDBBehaviorGameLogMessage(int playerId, int oldLevel, int level) {
        return new DBBehaviorGameLogMessage(playerId, Consts.BEHAVIOR_LOG_TYPE_LEVELUP, GameServer.INSTANCE.getCurrentTime(), oldLevel, level, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, null, null, null);
    }

    public static GameLogMessage buildRankLevelupDBBehaviorGameLogMessage(int playerId, int oldRankLevel, int rankLevel) {
        return new DBBehaviorGameLogMessage(playerId, Consts.BEHAVIOR_LOG_TYPE_RANK_LEVELUP, GameServer.INSTANCE.getCurrentTime(), oldRankLevel, rankLevel, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, null, null, null);
    }

    public static GameLogMessage buildRefreshBigStageDBBehaviorGameLogMessage(int playerId, int yellowsoul) {
        return new DBBehaviorGameLogMessage(playerId, Consts.BEHAVIOR_LOG_TYPE_REFRESH_BIGSTAGE, GameServer.INSTANCE.getCurrentTime(), yellowsoul, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, null, null, null);
    }

    public static GameLogMessage buildGetExperienceDBBehaviorGameLogMessage(int playerId, int type, int subType, int num) {
        return new DBBehaviorGameLogMessage(playerId, Consts.BEHAVIOR_LOG_TYPE_GET_EXPERIENCE, GameServer.INSTANCE.getCurrentTime(), type, subType, num, 0, 0, 0, 0, 0, 0, 0, 0, 0, null, null, null);
    }

    public static GameLogMessage buildGetRankExperienceDBBehaviorGameLogMessage(int playerId, int type, int subType, int num) {
        return new DBBehaviorGameLogMessage(playerId, Consts.BEHAVIOR_LOG_TYPE_GET_RANK_EXPERIENCE, GameServer.INSTANCE.getCurrentTime(), type, subType, num, 0, 0, 0, 0, 0, 0, 0, 0, 0, null, null, null);
    }

    public static GameLogMessage buildNoCharEnterDBBehaviorGameLogMessage(int playerId) {
        return new DBBehaviorGameLogMessage(playerId, Consts.BEHAVIOR_LOG_TYPE_NO_CHAR_ENTER, GameServer.INSTANCE.getCurrentTime(), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, null, null, null);
    }

    public static GameLogMessage buildGetNormalActivityItemRewardDBBehaviorGameLogMessage(int playerId, int activityId, int itemId) {
        return new DBBehaviorGameLogMessage(playerId, Consts.BEHAVIOR_LOG_TYPE_NORMAL_ACTIVITY_ITEM_REWARD, GameServer.INSTANCE.getCurrentTime(), activityId, itemId, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, null, null, null);
    }

    public static GameLogMessage buildGetCardExperienceDBBehaviorGameLogMessage(int playerId, int cardInstId, int cardLevel, int cardExp, int num) {
        return new DBBehaviorGameLogMessage(playerId, Consts.BEHAVIOR_LOG_TYPE_GET_CARD_EXPERIENCE, GameServer.INSTANCE.getCurrentTime(), cardInstId, cardLevel, cardExp, num, 0, 0, 0, 0, 0, 0, 0, 0, null, null, null);
    }

    public static GameLogMessage buildCardLevelupDBBehaviorGameLogMessage(int playerId, int cardInstId, int cardOldLevel, int cardLevel) {
        return new DBBehaviorGameLogMessage(playerId, Consts.BEHAVIOR_LOG_TYPE_CARD_LEVELUP, GameServer.INSTANCE.getCurrentTime(), cardInstId, cardOldLevel, cardLevel, 0, 0, 0, 0, 0, 0, 0, 0, 0, null, null, null);
    }

    public static GameLogMessage buildSubmitQuestDBBehaviorGameLogMessage(int playerId, int questId) {
        return new DBBehaviorGameLogMessage(playerId, Consts.BEHAVIOR_LOG_TYPE_QUEST_SUBMIT, GameServer.INSTANCE.getCurrentTime(), questId, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, null, null, null);
    }

    public static GameLogMessage buildCumulativeSignRewardDBBehaviorGameLogMessage(int playerId, int day, int num) {
        return new DBBehaviorGameLogMessage(playerId, Consts.BEHAVIOR_LOG_TYPE_CUMULATIVE_SIGN_REWARD, GameServer.INSTANCE.getCurrentTime(), day, num, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, null, null, null);
    }

    public static GameLogMessage buildConsecutiveSignRewardDBBehaviorGameLogMessage(int playerId, int day) {
        return new DBBehaviorGameLogMessage(playerId, Consts.BEHAVIOR_LOG_TYPE_CONSECUTIVE_SIGN_REWARD, GameServer.INSTANCE.getCurrentTime(), day, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, null, null, null);
    }

    public static GameLogMessage buildLivenessRewardDBBehaviorGameLogMessage(int playerId, int index) {
        return new DBBehaviorGameLogMessage(playerId, Consts.BEHAVIOR_LOG_TYPE_LIVENESS_REWARD, GameServer.INSTANCE.getCurrentTime(), index, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, null, null, null);
    }

    public static GameLogMessage buildVipGiftRewardDBBehaviorGameLogMessage(int playerId, int vip) {
        return new DBBehaviorGameLogMessage(playerId, Consts.BEHAVIOR_LOG_TYPE_VIPGIFT_REWARD, GameServer.INSTANCE.getCurrentTime(), vip, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, null, null, null);
    }

    public static GameLogMessage buildBuyEnergyDBBehaviorGameLogMessage(int playerId, int buyEnergyCount) {
        return new DBBehaviorGameLogMessage(playerId, Consts.BEHAVIOR_LOG_TYPE_BUY_ENERGY, GameServer.INSTANCE.getCurrentTime(), buyEnergyCount, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, null, null, null);
    }

    public static GameLogMessage buildBuyTokenDBBehaviorGameLogMessage(int playerId, int increaseNum, int token) {
        return new DBBehaviorGameLogMessage(playerId, Consts.BEHAVIOR_LOG_TYPE_BUY_TOKEN, GameServer.INSTANCE.getCurrentTime(), increaseNum, token, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, null, null, null);
    }

    public static GameLogMessage buildCombineItemDBBehaviorGameLogMessage(int playerId, int itemId) {
        return new DBBehaviorGameLogMessage(playerId, Consts.BEHAVIOR_LOG_TYPE_COMBINE_ITEM, GameServer.INSTANCE.getCurrentTime(), itemId, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, null, null, null);
    }

    public static GameLogMessage buildHandleMailDBBehaviorGameLogMessage(int playerId, long mailId) {
        return new DBBehaviorGameLogMessage(playerId, Consts.BEHAVIOR_LOG_TYPE_HANDLE_MAIL, GameServer.INSTANCE.getCurrentTime(), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, mailId, 0, null, null, null);
    }

    public static GameLogMessage buildRefreshHegemonyDBBehaviorGameLogMessage(int playerId, int refreshCount) {
        return new DBBehaviorGameLogMessage(playerId, Consts.BEHAVIOR_LOG_TYPE_REFRESH_HEGEMONY, GameServer.INSTANCE.getCurrentTime(), refreshCount, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, null, null, null);
    }

}
