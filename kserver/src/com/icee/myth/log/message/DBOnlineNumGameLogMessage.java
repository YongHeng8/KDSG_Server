/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.log.message;

/**
 *
 * @author liuxianke
 */
public class DBOnlineNumGameLogMessage extends GameLogMessage {
    public final int onlineNum;
    public final long time;

    public DBOnlineNumGameLogMessage(int onlineNum, long time) {
        super(GameLogType.GAMELOGTYPE_DB_ONLINENUM);
        this.onlineNum = onlineNum;
        this.time = time;
    }
}
