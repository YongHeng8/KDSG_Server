/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.log.message;

/**
 *
 * @author liuxianke
 */
public class FileDebugGameLogMessage extends GameLogMessage {
    public enum DebugLogType {DEBUGLOGTYPE_ERROR, DEBUGLOGTYPE_WARN, DEBUGLOGTYPE_INFO, DEBUGLOGTYPE_DEBUG};
    public static final DebugLogType LOGLEVEL = DebugLogType.DEBUGLOGTYPE_DEBUG;
    public DebugLogType subType;
    public String msg;

    public FileDebugGameLogMessage(DebugLogType subType, String msg) {
        super(GameLogType.GAMELOGTYPE_F_DEBUG);
        this.subType = subType;
        this.msg = msg;
    }
}
