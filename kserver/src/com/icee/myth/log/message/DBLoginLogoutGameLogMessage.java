/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.log.message;

/**
 *
 * @author liuxianke
 */
public class DBLoginLogoutGameLogMessage extends GameLogMessage {
    public final int playerId;
    public final int lv;
    public final boolean islogin;
    public final int ip;
    public final long duration;
    public final long time;

    public DBLoginLogoutGameLogMessage(int playerId, int lv, boolean islogin, int ip, long duration, long time) {
        super(GameLogType.GAMELOGTYPE_DB_LOGINLOGOUT);
        this.playerId = playerId;
        this.lv = lv;
        this.islogin = islogin;
        this.ip = ip;
        this.duration = duration;
        this.time = time;
    }
}
