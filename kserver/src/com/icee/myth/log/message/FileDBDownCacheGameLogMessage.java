/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.log.message;

/**
 *
 * @author liuxianke
 */
public class FileDBDownCacheGameLogMessage extends GameLogMessage {
    public final byte[] data;

    public FileDBDownCacheGameLogMessage(byte[] data) {
        super(GameLogType.GAMELOGTYPE_F_DBDOWN_CACHE);
        this.data = data;
    }
}
