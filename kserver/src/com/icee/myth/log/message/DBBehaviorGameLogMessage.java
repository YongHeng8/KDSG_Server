/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.log.message;

/**
 * 
 * @author liuxianke
 */
public class DBBehaviorGameLogMessage extends GameLogMessage {
    public final int playerId;
    public final int action;
    public final long time;
    public final int i1;
    public final int i2;
    public final int i3;
    public final int i4;
    public final int i5;
    public final int i6;
    public final int i7;
    public final int i8;
    public final int i9;
    public final int i10;
    public final long l1;
    public final long l2;
    public final String str1;
    public final String str2;
    public final byte[] bytes;

    public DBBehaviorGameLogMessage(int playerId, int action, long time, 
                                        int i1, int i2, int i3, int i4, int i5,
                                        int i6, int i7, int i8, int i9, int i10,
                                        long l1, long l2, String str1, String str2, byte[] bytes) {
        super(GameLogType.GAMELOGTYPE_DB_BEHAVIOR);

        this.playerId = playerId;
        this.action = action;
        this.time = time;
        this.i1 = i1;
        this.i2 = i2;
        this.i3 = i3;
        this.i4 = i4;
        this.i5 = i5;
        this.i6 = i6;
        this.i7 = i7;
        this.i8 = i8;
        this.i9 = i9;
        this.i10 = i10;
        this.l1 = l1;
        this.l2 = l2;
        this.str1 = str1;
        this.str2 = str2;
        this.bytes = bytes;
    }
}
