/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.config;

import com.icee.myth.server.GameServer;
import com.icee.myth.utils.Consts;
import com.icee.myth.utils.JSONHelper;

/**
 *
 * @author liuxianke
 */
public class MapConfig {

    public static MapConfig INSTANCE = JSONHelper.parseFileNoException(GameServer.INSTANCE.serverId + "/" + Consts.CONFIG_FILEPATH, MapConfig.class);
    public String fileLogPath = "log/";     // 日志根路径
    public int fileLogFlushInterval = 1000; // 文件日志刷新间隔
    public int fileLogBufferSize = 8192;    // 文件日志缓存大小
    public boolean needLogConsolePrint = true;  // 文件日志内容需向控制台打印
    public int maxStrangeHelperNum = 5;     // 最大助战陌生人数
    public int maxConcernNum = 5;         // 最大关注数
    public int maxMailNum = 30;             // 邮箱中最大邮件数
    public int maxTokenNum = 3;             // 最大军令数
    public int tokenPrice = 20;             // 军令价格
    public int tokenRecoverPeriod = 1200000;  // 军令恢复周期（单位毫秒）
    public int energyRecoverPeriod = 180000;  // 体力恢复周期（单位毫秒）
    public int bigStageEnterNumPerDay = 2;  // 每一精英副本每日进入次数
    public int initQuestId = 0;     // 初始任务号
    public float damXstr = 0.2f;    // 武力影响伤害的系数
    public float damXwis = 0.2f;    // 智力影响伤害的系数
    public float damXsta = 0.1f;    // 统帅影响伤害的系数
    public float[][] unitsRestraint;    // 兵种克制
    public VipNumGold[] buyEnergyGolds;
    public VipNumGold[] refreshHegemonyGolds;
    public int increaseEnergyAtTwelve = 50;
    public int increaseEnergyAtEighteen = 50;
    public int[] vipLevelUpConfig = {100, 500, 1000, 2000, 50000, 10000, 20000, 50000, 100000, 200000, 500000, 1000000};
    public int[] vipCardSlotNums = {30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150};
    public float eatCardExperienceRatio = 0.5f;     // 吃卡经验折算系数
    public float reviveCardPriceRatio = 0.3f;       // 复活卡价格折算系数
    public int strengthenCardPriceRatio = 150;      // 强化卡片价格系数
    public int[] initCardIds = {40102, 60702, 80102, 15002, 20402}; // 新玩家创建角色可选的卡片列表
    public float[] discounts = {0.88f, 0.78f, 0.68f, 0.58f}; // 折扣配置
    public int hegemonySmallCardDrawId = 0;         // 争霸小宝箱（卡包号）
    public int hegemonyBigCardDrawId = 6;           // 争霸大宝箱（卡包号）

    public int mineBaseRatio = 3;                       // 基础银矿系数
    public float mineTaxRatio = 0.3f;                   // 银矿税率
    public int minePeriod = Consts.MILSECOND_10MINITE;  // 产矿周期
    public int maxOccupyTime = Consts.MILSECOND_TWO_DAY;  // 解放时间
    public int mineFullTime = Consts.MILSECOND_8HOUR;   // 银矿满时间

    public int openHegemonyLevel = 10;              // 争霸开启等级
    public int openMineLevel = 20;                  // 银矿开启等级（注意：即臣属关系开启）
    public int openTrainingLevel = 20;              // 城墙开启等级
    public int openBarrackLevel = 20;               // 兵营开启等级
    public int openOrdnanceLevel = 20;              // 军械所开启等级
    public int openCouncilLevel = 20;               // 军机处开启等级

    public int baseTargetsRefreshCooldown = 5000;          // 据点征服目标刷新冷却时间（单位毫秒）

    public boolean useCYLog = false;
    public int gameId = 0;

    private MapConfig() {
    }
}
