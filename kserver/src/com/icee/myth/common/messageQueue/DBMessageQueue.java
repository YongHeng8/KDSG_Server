/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.common.messageQueue;

import com.icee.myth.common.message.dbMessage.DBMessage;
import com.icee.myth.utils.LinkedTransferQueue;


/**
 * 数据库消息队列
 * 所有的数据消息暂时先放入此堵塞队列中，以便DB线程处理
 * @author liuxianke
 */
public class DBMessageQueue {
    private static final LinkedTransferQueue<DBMessage> messageQueue = new LinkedTransferQueue<DBMessage>();

    public static LinkedTransferQueue<DBMessage> queue() {
        return messageQueue;
    }
}
