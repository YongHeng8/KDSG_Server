/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.common.messageQueue;

import com.icee.myth.common.message.serverMessage.Message;
import com.icee.myth.utils.LinkedTransferQueue;


/**
 *
 * @author liuxianke
 */
public class ServerMessageQueue {
    // The messageQueue queue is a resource shared by channel handler thread and the game thread.
    // Channel handler thread put received messages in the queue and the game thread get
    // messages out of the queue.
    private static final LinkedTransferQueue<Message> messageQueue = new LinkedTransferQueue<Message>();

    public static LinkedTransferQueue<Message> queue() {
        return messageQueue;
    }
}
