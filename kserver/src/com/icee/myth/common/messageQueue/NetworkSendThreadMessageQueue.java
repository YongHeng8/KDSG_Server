/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.common.messageQueue;

import com.icee.myth.common.message.networkSendThreadMessage.NetworkSendThreadMessage;
import com.icee.myth.utils.LinkedTransferQueue;

/**
 * 网络发送线程队列
 * @author liuxianke
 */
public class NetworkSendThreadMessageQueue {
    private static final LinkedTransferQueue<NetworkSendThreadMessage> messageQueue = new LinkedTransferQueue<NetworkSendThreadMessage>();

    public static LinkedTransferQueue<NetworkSendThreadMessage> queue() {
        return messageQueue;
    }
}
