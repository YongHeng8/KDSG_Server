/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.common.charInfo;

import com.icee.myth.protobuf.InternalCommonProtocol.DBMailsProto;
import com.icee.myth.protobuf.InternalCommonProtocol.PlayerDetailProto;

/**
 * 玩家的详细角色信息
 * @author liuxianke
 */
public class CharDetailInfo {
    public int mid;
    public String name;
    public int gold1;
    public int gold2;
    public long silver;
    public int energy;
    public int token;
    public int level;
    public int experience;
    public int rankLevel;
    public int rankExperience;
    public int leaderCardId;
    public int leaderCardLevel;
    public int vipLevel;
    public int vipExperience;
    public int maxPower;

    public PlayerDetailProto detail;
    public DBMailsProto mailInfo;

    public long totalOnlineTime;
    public long leaveTime;
}
