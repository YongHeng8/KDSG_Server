/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.common;

import com.icee.myth.server.GameServer;
import org.jboss.netty.channel.Channel;


/**
 * 用于记录刚建立连接但未登录的channel
 * @author liuxianke
 */
public class UninitializeChannel {

    public long createTime;
    public Channel channel;

    public UninitializeChannel(Channel channel) {
        this.channel = channel;
        createTime = GameServer.INSTANCE.getCurrentTime();
    }
}