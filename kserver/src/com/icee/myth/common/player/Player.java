/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.common.player;

import com.icee.myth.common.message.serverMessage.Message;

/**
 *
 * @author liuxianke
 */
public interface Player {
    public int getId();
    public long getLastActiveTime();
    public boolean handleMessage(Message message);
}
