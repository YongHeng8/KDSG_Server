/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.common.message.networkSendThreadMessage;

/**
 *
 * @author liuxianke
 */
public class SimpleNetworkSendThreadMessage implements NetworkSendThreadMessage {
    private final NetworkSendThreadMessageType type;

    public SimpleNetworkSendThreadMessage(NetworkSendThreadMessageType type) {
        this.type = type;
    }

    @Override
    public NetworkSendThreadMessageType getType() {
        return type;
    }
}
