/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.common.message.serverMessage.builder;

import com.icee.myth.server.message.serverMessage.ConsoleGmMessage;
import com.icee.myth.common.message.serverMessage.ExternalPlayerMessage;
import com.icee.myth.common.message.serverMessage.InternalChannelMessage;
import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;
import com.icee.myth.common.message.serverMessage.Message;
import com.icee.myth.common.message.serverMessage.Message.MessageType;
import com.icee.myth.common.message.serverMessage.SimpleMessage;
import com.icee.myth.protobuf.InternalCommonProtocol.M2SGmProto;
import org.jboss.netty.channel.Channel;

/**
 *
 * @author liuxianke
 */
public class MessageBuilder {
    public static Message buildShutdownMessage() {
        return new SimpleMessage(MessageType.ALL_SHUTDOWN);
    }

    public static Message buildManagerHeartbeatMessage() {
        return new SimpleMessage(MessageType.ALL_MANAGERHEARTBEAT);
    }

    public static Message buildClientConnectMessage(Channel channel) {
        return new ExternalPlayerMessage(MessageType.ALL_CLIENTCONNECT,channel);
    }

    public static Message buildManagerConnectMessage(Channel channel) {
        return new InternalChannelMessage(MessageType.ALL_MANAGERCONNECT, channel);
    }

    public static Message buildClientCloseMessage(Channel channel) {
        return new ExternalPlayerMessage(MessageType.ALL_CLIENTCLOSE, channel);
    }

    public static Message buildManagerCloseMessage(Channel channel) {
        return new InternalChannelMessage(MessageType.ALL_MANAGERCLOSE, channel);
    }

    public static Message buildManagerDownMessage() {
        return new SimpleMessage(MessageType.ALL_MANAGERDOWN);
    }

    public static Message buildGmMessage(M2SGmProto gmProto) {
        return new ConsoleGmMessage(gmProto);
    }

    public static Message buildBillNotifyMessage(Integer playerId) {
        return new InternalPlayerMessage(MessageType.MAP_BILLNOTIFY, playerId);
    }
}
