/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.utils;

import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author liuxianke
 */
public class InternalIDGenerator {
    public final static InternalIDGenerator INSTANCE = new InternalIDGenerator();

    // TODO: 是否需要考虑ID溢出问题（注意：定期维护服时id会重新开始）

    // 动态NPC ID计数
    // 100000以前的ID为静态NPC的ID空间
    private AtomicInteger nextGenerateDynamicNpcId = new AtomicInteger(100000);

    // 动态Vehicle ID计数
    private AtomicInteger nextGenerateDynamicVehicleId = new AtomicInteger(0);

    // 动态Seat ID计数
    private AtomicInteger nextGenerateDynamicSeatId = new AtomicInteger(0);

    // 动态AreaEffect ID计数
    private AtomicInteger nextGenerateDynamicAreaEffectId = new AtomicInteger(0);

    // 动态场景ID计数
    // 10000以前的ID为静态场景的ID空间
    // 静态场景ID = 场景模板ID
    // 全服的静态场景数不超过10000
    private AtomicInteger nextGenerateDynamicSceneId = new AtomicInteger(20000);

    private InternalIDGenerator() {
    }

//    public static InternalIDGenerator INSTANCE {
//      return INSTANCE;
//    }

    public int generateDynamicNpcId() {
        return nextGenerateDynamicNpcId.getAndIncrement();
    }
    
    public int generateDynamicVehicleId() {
        return nextGenerateDynamicVehicleId.getAndIncrement();
    }

    public int generateDynamicSeatId() {
        return nextGenerateDynamicSeatId.getAndIncrement();
    }

    public int generateDynamicAreaEffectId() {
        return nextGenerateDynamicAreaEffectId.getAndIncrement();
    }

    public int generateDynamicSceneId() {
        return nextGenerateDynamicSceneId.getAndIncrement();
    }

}
