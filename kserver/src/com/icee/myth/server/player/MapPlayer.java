/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.server.player;


import com.icee.myth.common.channelContext.ChannelContext;
import com.icee.myth.common.player.AbstractPlayer;
import com.icee.myth.server.actor.Human;
import com.icee.myth.server.message.serverMessage.LoginMessage;
import com.icee.myth.server.player.state.Login1PlayerState;
import org.jboss.netty.channel.Channel;

/**
 * 玩家上下文数据
 * @author liuxianke
 */
public class MapPlayer extends AbstractPlayer {
    public final String passport;
    public final ChannelContext channelContext;
    public Human human;
    public boolean auth;
    public int privilege;
    public long endForbidTalkTime;

    public LoginMessage loginMessage;   // 重复登陆时，记录后登陆的消息，等待该MapPlayer对象被删除的时候再处理

    public MapPlayer(int id, String passport, boolean auth, int privilege, long endForbidTalkTime, Channel channel) {
        super(id, Login1PlayerState.INSTANCE);
        this.passport = passport;
        this.auth = auth;
        this.privilege = privilege;
        this.endForbidTalkTime = endForbidTalkTime*1000; //秒转成毫秒
        this.channelContext = new ChannelContext();
        this.channelContext.setChannel(channel);
    }

}
