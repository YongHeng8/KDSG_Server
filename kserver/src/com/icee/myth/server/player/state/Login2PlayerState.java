/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.server.player.state;

import com.icee.myth.common.messageQueue.DBMessageQueue;
import com.icee.myth.common.player.Player;
import com.icee.myth.common.player.state.PlayerState;
import com.icee.myth.common.message.serverMessage.Message;
import com.icee.myth.common.message.serverMessage.Message.MessageType;
import com.icee.myth.config.MapConfig;
import com.icee.myth.server.message.dbMessage.builder.MapDBMessageBuilder;
import com.icee.myth.server.message.serverMessage.CreateCharMessage;
import com.icee.myth.server.player.MapPlayer;
import com.icee.myth.protobuf.builder.ClientToMapBuilder;
import com.icee.myth.log.GameLogger;
import com.icee.myth.log.message.FileDebugGameLogMessage;
import com.icee.myth.log.message.GameLogMessage.GameLogType;
import com.icee.myth.log.message.builder.GameLogMessageBuilder;
import com.icee.myth.protobuf.ExternalCommonProtocol.CreateCharProto;
import java.util.ArrayList;
import java.util.List;

/**
 * Login3State是玩家登录后已获取角色信息的状态
 * @author liuxianke
 */
public final class Login2PlayerState implements PlayerState {

    public final static Login2PlayerState INSTANCE = new Login2PlayerState();

    private Login2PlayerState() {
    }

    @Override
    public boolean handleMessage(Player player, Message message) {
        // 说明：当遇到不该在此状态处理的消息时，说明玩家状态异常，将玩家状态设置为空，
        // 告诉主线程切断玩家连接并清理玩家上下文
        MapPlayer p = (MapPlayer)player;
        int playerId = p.getId();
        MessageType msgType = message.getType();
        switch (msgType) {
            case MAP_CREATE_CHAR: {
                // 处理创建角色请求
                // TODO: 验证是否允许创建角色，以及所创建角色信息的正确性，通过验证后向产生CreateCharDBMessage到DB message queue
                CreateCharMessage createCharMsg = (CreateCharMessage)message;
                if(isValidate(createCharMsg.charInfo)) {
                    GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                            FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_DEBUG,
                            "Player["+playerId+"] startOnlyOnce create char."));

                    if (MapConfig.INSTANCE.useCYLog == true){
                        List<String> cyLogList = new ArrayList<String>();
                        cyLogList.add("registerMC");
                        cyLogList.add(String.valueOf(MapConfig.INSTANCE.gameId));
                        cyLogList.add("KDTY");
                        cyLogList.add(String.valueOf(playerId));
                        cyLogList.add(String.valueOf(createCharMsg.charInfo.getName()));
                        cyLogList.add("");
                        cyLogList.add("");
                        cyLogList.add("");
                        cyLogList.add("0");
                        GameLogger.getlogger().log(GameLogMessageBuilder.buildFileCYGameLogMessage(cyLogList, GameLogType.GAMELOGTYPE_CY_REGISTER));
                    }

                    DBMessageQueue.queue().offer(MapDBMessageBuilder.buildCreateCharDBMessage(playerId, createCharMsg.charInfo));
                    p.state = CreatingCharPlayerState.INSTANCE;
                } else {
                    // TODO: 回复客户端无法创建角色
                    p.channelContext.write(ClientToMapBuilder.buildCreateCharError());
                }
                return true;
            }
            default: {
                // 错误消息类型（不作处理），记录日志
                GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                        FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                        "Player["+playerId+"] Login3State handle error message type["+msgType+"]."));

                return false;
            }
        }
    }

    public boolean isValidate(CreateCharProto charInfo) {
        // 验证玩家输入信息

        // 验证名字长度
        String name = charInfo.getName();
        int nameLenth = name.length();
        if ((nameLenth<2) || (nameLenth>8)) {
            GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                        FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                        "Player Name Too Long"));
            return false;
        }

        // 验证职业
        int jobId = charInfo.getJob();
        return jobId>=0 && jobId<5;
    }
}
