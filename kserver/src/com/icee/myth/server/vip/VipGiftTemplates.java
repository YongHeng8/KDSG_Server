/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.vip;

import com.icee.myth.server.GameServer;
import com.icee.myth.server.reward.CertainRewardInfo;
import com.icee.myth.utils.Consts;
import com.icee.myth.utils.JSONHelper;

/**
 *
 * @author yangyi
 */
public class VipGiftTemplates {
    public CertainRewardInfo[] rewardInfos;

    public static VipGiftTemplates INSTANCE = JSONHelper.parseFileNoException(GameServer.INSTANCE.serverId + "/" + Consts.VIPREWARD_TEMPLATES_FILEPATH, VipGiftTemplates.class);

    private VipGiftTemplates(){
    }
    
}
