/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.vip;

import com.icee.myth.config.MapConfig;
import com.icee.myth.protobuf.ExternalCommonProtocol.VipProto;
import com.icee.myth.protobuf.builder.ClientToMapBuilder;
import com.icee.myth.server.GameServer;
import com.icee.myth.server.actor.Human;
import com.icee.myth.utils.Consts;

/**
 *
 * @author liuxianke
 */
public class Vip {
    public Human human;
    public int level;
    public int exp;
    public int lastDayExp;  // 最后充值日累计充值
    public int lastExpDay;  // 最后一次充值日期

    public VipProto buildVipProto(){
        VipProto.Builder builder = VipProto.newBuilder();
        builder.setExp(exp);
        builder.setLevel(level);
        builder.setLastDayExp(lastDayExp);
        builder.setLastExpDay(lastExpDay);
        return builder.build();
    }

    public Vip(Human human, int level, int exp, int lastDayExp, int lastExpDay){
        this.human = human;

        this.exp = exp;
        this.level = level;
        this.lastDayExp = lastDayExp;
        this.lastExpDay = lastExpDay;
        
        caculateLevel(exp);
    }

    private void caculateLevel(int exp){
        int[] levelUpExps = MapConfig.INSTANCE.vipLevelUpConfig;
        for (int lv = level; lv < levelUpExps.length; lv++) {
            if(exp < levelUpExps[lv]){
                level = (level < lv)?lv:level ;
                return;
            }
        }

        level = levelUpExps.length;
    }

    public void increaseVipExp(int num, boolean needSend){
        if(num > 0){
            exp += num;

            int currDay = (int) ((GameServer.INSTANCE.getCurrentTime() + Consts.JET_LAG) / Consts.MILSECOND_ONE_DAY);
            if (currDay > lastExpDay) {
                lastDayExp = 0;
            }

            lastDayExp += num;
            lastExpDay = currDay;

            caculateLevel(exp);
            if(needSend){
                human.sendMessage(ClientToMapBuilder.buildVipChange(buildVipProto()));
            }
        }
    }

    public void clearVipLevel(boolean needSend) {
        exp = 0;
        level = 0;
        caculateLevel(exp);
        
        if(needSend){
            human.sendMessage(ClientToMapBuilder.buildVipChange(buildVipProto()));
        }
    }
}
