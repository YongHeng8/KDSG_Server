/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.channelHandler;

import com.icee.myth.server.message.serverMessage.builder.MapMessageBuilder;
import com.icee.myth.common.messageQueue.ServerMessageQueue;
import com.icee.myth.common.protobufmessage.ProtobufMessageType;
import com.icee.myth.protobuf.InternalCommonProtocol.M2SGmProto;
import com.icee.myth.log.GameLogger;
import com.icee.myth.log.message.builder.GameLogMessageBuilder;
import com.icee.myth.protobuf.ExternalCommonProtocol.IntValueProto;
import com.icee.myth.utils.StackTraceUtil;
import java.io.IOException;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBufferInputStream;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelStateEvent;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelUpstreamHandler;

/**
 *
 * @author liuxianke
 */
public class ManagerConnectHandler extends SimpleChannelUpstreamHandler {

    @Override
    public void channelConnected(
            ChannelHandlerContext ctx, ChannelStateEvent e) {
        // Add GW channel connected message to message queue
        ServerMessageQueue.queue().offer(MapMessageBuilder.buildManagerConnectMessage(e.getChannel()));
    }

    @Override
    public void channelClosed(
            ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        // Add GW channel close message to message queue
        ServerMessageQueue.queue().offer(MapMessageBuilder.buildManagerCloseMessage(e.getChannel()));
    }

    @Override
    public void messageReceived(
            ChannelHandlerContext ctx, MessageEvent e) {
        try {
            // Add GW channel received message to message queue
            ChannelBuffer cb = (ChannelBuffer) e.getMessage();
            short type = cb.readShort();
            short length = cb.readShort();

            switch (type) {
                case ProtobufMessageType.MANAGER2SERVER_HEARTBEAT: {
                    ServerMessageQueue.queue().offer(MapMessageBuilder.buildManagerHeartbeatMessage());
                    break;
                }
                case ProtobufMessageType.MANAGER2SERVER_SHUTDOWN: {
                    ServerMessageQueue.queue().offer(MapMessageBuilder.buildShutdownMessage());
                    break;
                }
                case ProtobufMessageType.MANAGER2SERVER_GM: {
                    M2SGmProto gmProto = M2SGmProto.getDefaultInstance().newBuilderForType().mergeFrom(new ChannelBufferInputStream(cb)).build();
                    ServerMessageQueue.queue().offer(MapMessageBuilder.buildGmMessage(gmProto));
                    break;
                }
                case ProtobufMessageType.MANAGER2SERVER_BILLNOTIFY: {
                    IntValueProto intValue = IntValueProto.getDefaultInstance().newBuilderForType().mergeFrom(new ChannelBufferInputStream(cb)).build();
                    ServerMessageQueue.queue().offer(MapMessageBuilder.buildBillNotifyMessage(intValue.getValue()));
                    break;
                }
                case ProtobufMessageType.MANAGER2CLUSTER_GETSERVERVERSION:{

                }
                // TODO: other message
            }
       } catch (IOException ex) {
            GameLogger.getlogger().log(GameLogMessageBuilder.buildFileNetErrorGameLogMessage(StackTraceUtil.getStackTrace(ex)));
        }

    }

    @Override
    public void exceptionCaught(
            ChannelHandlerContext ctx, ExceptionEvent e) {

        GameLogger.getlogger().log(GameLogMessageBuilder.buildFileNetErrorGameLogMessage(StackTraceUtil.getStackTrace(e.getCause())));
        // TODO: 发生异常是否需要关闭与GW的连接？
//        e.getChannel().close();
    }
}
