/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.dbMessage;

import com.icee.myth.common.message.dbMessage.SimpleDBMessage;

/**
 *
 * @author yangyi
 */
public class ChangePlayerNameDBMessage extends SimpleDBMessage{
    public final int playerId;
    public final String newName;

    public ChangePlayerNameDBMessage(int playerId, String name) {
        super(DBMessageType.CHANGE_NAME);
        this.playerId = playerId;
        this.newName = name;
    }

}
