/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.dbMessage;

import com.icee.myth.common.message.dbMessage.SimpleDBMessage;

/**
 *
 * @author lidonglin
 */
public class ConsumerLogDBMessage extends SimpleDBMessage{
    public final int playerId;
    public final int consumerType;
    public final int goldNum;
    public final int goldType;
    public final String productId;
    public final int productNum;

    public ConsumerLogDBMessage(int playerId, int consumerType, int goldNum, int goldType, String productId, int productNum) {
        super(DBMessageType.CONSUMER_LOG);
        this.playerId = playerId;
        this.consumerType = consumerType;
        this.goldNum = goldNum;
        this.goldType = goldType;
        this.productId = productId;
        this.productNum = productNum;
    }
}
