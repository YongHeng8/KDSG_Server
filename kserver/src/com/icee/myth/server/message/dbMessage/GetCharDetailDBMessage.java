/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.dbMessage;

import com.icee.myth.common.message.dbMessage.SimpleDBMessage;

/**
 *
 * @author liuxianke
 */
public class GetCharDetailDBMessage extends SimpleDBMessage {
    public final int playerId;

    public GetCharDetailDBMessage(int playerId) {
        super(DBMessageType.GET_CHAR_DETAIL);
        this.playerId = playerId;
    }
}
