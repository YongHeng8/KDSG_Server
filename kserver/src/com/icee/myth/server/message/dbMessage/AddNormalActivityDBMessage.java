/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.dbMessage;

import com.icee.myth.common.message.dbMessage.SimpleDBMessage;
import com.icee.myth.server.message.serverMessage.gmMessage.AddNormalActivityMessage;

/**
 *
 * @author yangyi
 */
public class AddNormalActivityDBMessage extends SimpleDBMessage{
    public final AddNormalActivityMessage addNormalActivityMessage;

    public AddNormalActivityDBMessage(AddNormalActivityMessage addNormalActivityMessage) {
        super(DBMessageType.ADD_NORMALACTIVITY);
        this.addNormalActivityMessage = addNormalActivityMessage;
    }

}
