/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.dbMessage;

import com.icee.myth.common.message.dbMessage.SimpleDBMessage;
import com.icee.myth.protobuf.InternalCommonProtocol.DBOccupyInfoProto;

/**
 *
 * @author liuxianke
 */
public class SaveCharOccupyInfoDBMessage extends SimpleDBMessage {
    public final int playerId;
    public final DBOccupyInfoProto occupyInfoProto;

    public SaveCharOccupyInfoDBMessage(int playerId, DBOccupyInfoProto occupyInfoProto) {
        super(DBMessageType.SAVE_CHAR_OCCUPY_INFO);
        this.playerId = playerId;
        this.occupyInfoProto = occupyInfoProto;
    }
}
