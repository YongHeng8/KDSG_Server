/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.dbMessage.builder;

import com.icee.myth.server.message.dbMessage.ChangePlayerNameDBMessage;
import com.icee.myth.server.message.dbMessage.GetCouponDBMessage;
import com.icee.myth.server.message.dbMessage.SaveBillDBMessage;
import com.icee.myth.server.message.dbMessage.SaveRelationDBMessage;
import com.icee.myth.common.charInfo.CharDetailInfo;
import com.icee.myth.common.message.dbMessage.DBMessage;
import com.icee.myth.common.message.dbMessage.DBMessage.DBMessageType;
import com.icee.myth.common.message.dbMessage.SimpleDBMessage;
import com.icee.myth.protobuf.ExternalCommonProtocol.CreateCharProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.RewardProto;
import com.icee.myth.protobuf.InternalCommonProtocol.DBOccupyInfoProto;
import com.icee.myth.protobuf.InternalCommonProtocol.DBRelationProto;
import com.icee.myth.server.bill.Order;
import com.icee.myth.server.message.dbMessage.AddNormalActivityDBMessage;
import com.icee.myth.server.message.dbMessage.ConsumerLogDBMessage;
import com.icee.myth.server.message.dbMessage.CreateCharDBMessage;
import com.icee.myth.server.message.dbMessage.DeleteNormalActivityDBMessage;
import com.icee.myth.server.message.dbMessage.GetBriefPlayerInfosDBMessage;
import com.icee.myth.server.message.dbMessage.GetCharDetailDBMessage;
import com.icee.myth.server.message.dbMessage.GetCharNumDBMessage;
import com.icee.myth.server.message.dbMessage.GetCharOccupyInfoDBMessage;
import com.icee.myth.server.message.dbMessage.GetNewMailDBMessage;
import com.icee.myth.server.message.dbMessage.GetRelationDBMessage;
import com.icee.myth.server.message.dbMessage.SaveCharInfoDBMessage;
import com.icee.myth.server.message.dbMessage.SaveCharOccupyInfoDBMessage;
import com.icee.myth.server.message.dbMessage.SaveMailDBMessage;
import com.icee.myth.server.message.serverMessage.gmMessage.AddNormalActivityMessage;
import com.icee.myth.server.message.serverMessage.gmMessage.DeleteNormalActivityMessage;
import java.util.ArrayList;

/**
 *
 * @author liuxianke
 */
public class MapDBMessageBuilder {
    public static GetCharNumDBMessage buildGetCharNumDBMessage(int playerId) {
        return new GetCharNumDBMessage(playerId);
    }

    public static GetCharDetailDBMessage buildGetCharDetailDBMessage(int playerId) {
        return new GetCharDetailDBMessage(playerId);
    }

    public static GetCharOccupyInfoDBMessage buildGetCharOccupyInfoDBMessage(int playerId) {
        return new GetCharOccupyInfoDBMessage(playerId);
    }

    public static CreateCharDBMessage buildCreateCharDBMessage(int playerId, CreateCharProto charInfo) {
        return new CreateCharDBMessage(playerId, charInfo);
    }

    public static SaveCharInfoDBMessage buildSaveCharInfoDBMessage(int playerId, CharDetailInfo info) {
        return new SaveCharInfoDBMessage(playerId, info);
    }

    public static SaveCharOccupyInfoDBMessage buildSaveCharOccupyInfoDBMessage(int playerId, DBOccupyInfoProto info) {
        return new SaveCharOccupyInfoDBMessage(playerId, info);
    }

    public static ChangePlayerNameDBMessage buildChangePlayerNameDBMessage(int playerId, String newName) {
        return new ChangePlayerNameDBMessage(playerId, newName);
    }

    public static AddNormalActivityDBMessage buildAddNormalActivityDBMessage(AddNormalActivityMessage addNormalActivityMessage) {
        return new AddNormalActivityDBMessage(addNormalActivityMessage);
    }

    public static DeleteNormalActivityDBMessage buildDeleteNormalActivityDBMessage(DeleteNormalActivityMessage deleteNormalActivityMessage) {
        return new DeleteNormalActivityDBMessage(deleteNormalActivityMessage);
    }

    public static GetRelationDBMessage buildGetRelationDBMessage(int playerId) {
        return new GetRelationDBMessage(playerId);
    }

    public static GetBriefPlayerInfosDBMessage buildGetBriefPlayerInfosDBMessage(int playerId, ArrayList<Integer> unknownFriends) {
        return new GetBriefPlayerInfosDBMessage(playerId, unknownFriends);
    }

//    public static GetBriefPlayerInfoByIDDBMessage buildGetBriefPlayerInfoByIDDBMessage(int playerId, int targetId) {
//        return new GetBriefPlayerInfoByIDDBMessage(playerId, targetId);
//    }

    public static SaveRelationDBMessage buildSaveRelationDBMessage(int playerId, DBRelationProto relationProto) {
        return new SaveRelationDBMessage(playerId, relationProto);
    }
    
    public static SimpleDBMessage buildShutdownDBMessage() {
        return new SimpleDBMessage(DBMessageType.SHUTDOWN);
    }

    public static DBMessage buildSaveBillDBMessage(int playerId, String passport, Order order,int status, int step) {
        return new SaveBillDBMessage(playerId, passport, order, status, step);
    }

    public static DBMessage buildConsumerLogDBMessage(int playerId, int consumerType, int goldNum, int goldType, String productId, int productType) {
        return new ConsumerLogDBMessage(playerId, consumerType, goldNum, goldType, productId, productType);
    }

    public static DBMessage buildGetCouponDBMessage(int playerId, String passport, String code) {
        return new GetCouponDBMessage(playerId, passport, code);
    }

    public static DBMessage buildSaveMailDBMessage(long mailId, int targetId, String title, String description, RewardProto rewardInfo) {
        return new SaveMailDBMessage(mailId, targetId, title, description, rewardInfo);
    }

    public static DBMessage buildGetNewMailDBMessage(int playerId, long lastGlobalMaxMailId) {
        return new GetNewMailDBMessage(playerId, lastGlobalMaxMailId);
    }
}
