/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.dbMessage;

import com.icee.myth.common.message.dbMessage.SimpleDBMessage;
import com.icee.myth.protobuf.ExternalCommonProtocol.CreateCharProto;

/**
 *
 * @author liuxianke
 */
public class CreateCharDBMessage extends SimpleDBMessage {
    public final int playerId;
    public final CreateCharProto createCharInfo;

    public CreateCharDBMessage(int playerId, CreateCharProto createCharInfo) {
        super(DBMessageType.CREATE_CHAR);
        this.playerId = playerId;
        this.createCharInfo = createCharInfo;
    }
}
