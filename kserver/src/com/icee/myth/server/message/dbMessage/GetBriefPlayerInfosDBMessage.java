/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.dbMessage;

import java.util.ArrayList;

import com.icee.myth.common.message.dbMessage.SimpleDBMessage;

/**
 *
 * @author liuxianke
 */
public class GetBriefPlayerInfosDBMessage extends SimpleDBMessage {
    public final int playerId;
    public final ArrayList<Integer> unknownFriends;

    public GetBriefPlayerInfosDBMessage(int playerId, ArrayList<Integer> unknownFriends) {
        super(DBMessageType.GET_BRIEF_PLAYER_INFOS);

        this.playerId = playerId;
        this.unknownFriends = unknownFriends;
    }

}
