/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.dbMessage;

import com.icee.myth.common.message.dbMessage.SimpleDBMessage;

/**
 *
 * @author liuxianke
 */
public class GetCharOccupyInfoDBMessage extends SimpleDBMessage {
    public final int playerId;

    public GetCharOccupyInfoDBMessage(int playerId) {
        super(DBMessageType.GET_CHAR_OCCUPY_INFO);
        this.playerId = playerId;
    }
}
