/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage.gmMessage;

import com.icee.myth.common.message.serverMessage.SimpleMessage;

/**
 *
 * @author liuxianke
 */
public class GMMessage extends SimpleMessage {
    public enum GMMessageType {
        GM_GET_ONLINE_PLAYER_COUNT,
        GM_GET_ONLINE_PLAYER_IDS,
        GM_KICK_PLAYER,
        GM_SHUTUP,
        GM_BROADCAST,
        GM_ADD_MAIL,
        GM_ADD_NORMAL_ACTIVITY,
        GM_DELETE_NORMAL_ACTIVITY,
        GM_SET_CARD_DRAW_ACTIVITY,
        GM_SET_STAGE_ACTIVITY,
        GM_SET_SILVER,
        GM_SET_GOLD1,
        GM_SET_GOLD2,
        GM_SET_ENERGY,
        GM_SET_VIP_EXP,
        GM_ADD_ITEM,
        GM_REMOVE_ITEM,
        GM_ADD_CARD,
        GM_REMOVE_CARD,
        GM_CHANGE_SANDBOX,
        GM_CHANGE_PLAYER_NAME,
        GM_SET_LEVEL,
        GM_EDIT_GOLD1,
        GM_EDIT_GOLD2,        
        GM_GET_CONSUMER_LOG,
    }
    
    public final GMMessageType subType;

	public GMMessage(GMMessageType subType) {
        super(MessageType.MAP_GM);

        this.subType = subType;
    }
}
