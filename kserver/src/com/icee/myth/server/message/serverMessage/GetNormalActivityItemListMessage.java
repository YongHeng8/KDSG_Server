/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;

/**
 *
 * @author liuxianke
 */
public class GetNormalActivityItemListMessage extends InternalPlayerMessage {
    public final int activityId;

    public GetNormalActivityItemListMessage(int playerId, int activityId) {
        super(MessageType.MAP_NORMAL_ACTIVITY_GET_ITEM_LIST, playerId);
        this.activityId = activityId;
    }
}
