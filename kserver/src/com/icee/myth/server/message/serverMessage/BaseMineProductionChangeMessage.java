/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;

/**
 *
 * @author liuxianke
 */
public class BaseMineProductionChangeMessage extends InternalPlayerMessage{

    public final int production;

    public BaseMineProductionChangeMessage(int playerId, int production) {
        super(MessageType.MAP_BASE_MINE_PRODUCTION_CHANGE, playerId);
        this.production = production;
    }
}
