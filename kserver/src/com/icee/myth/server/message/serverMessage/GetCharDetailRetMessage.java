/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.charInfo.CharDetailInfo;
import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;

/**
 *
 * @author liuxianke
 */
public class GetCharDetailRetMessage extends InternalPlayerMessage {
    public final CharDetailInfo charDetailInfo;

    public GetCharDetailRetMessage(int playerId, CharDetailInfo charDetailInfo) {
        super(MessageType.MAP_GET_CHAR_DETAIL_INFO_RET, playerId);

        this.charDetailInfo = charDetailInfo;
    }

}
