/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;

/**
 *
 * @author liuxianke
 */
public class HegemonyRefreshMessage extends InternalPlayerMessage {
    public final int count;

    public HegemonyRefreshMessage(int playerId, int count) {
        super(MessageType.MAP_HEGEMONY_REFRESH, playerId);

        this.count = count;
    }
}
