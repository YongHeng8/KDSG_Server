/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage.gmMessage;

/**
 *
 * @author yangyi
 */
public class DeleteNormalActivityMessage extends GMMessage {
    public final int activityId;
    public boolean response;
    public boolean hasWriteDB = false;

    public DeleteNormalActivityMessage (int activityId) {
        super(GMMessageType.GM_DELETE_NORMAL_ACTIVITY);
        this.activityId = activityId;
    }

}
