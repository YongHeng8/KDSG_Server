/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.SimpleMessage;
import java.util.LinkedList;

/**
 *
 * @author liuxianke
 */
public class UninitOccupyInfoMessage extends SimpleMessage {
    public final LinkedList<Integer> result;

    public UninitOccupyInfoMessage(LinkedList<Integer> result) {
        super(MessageType.ALL_UNINIT_OCCUPY_INFO_MESSAGE);
        this.result = result;
    }
}
