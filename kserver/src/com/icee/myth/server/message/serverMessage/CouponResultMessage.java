/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;
import com.icee.myth.server.reward.CertainRewardInfo;

/**
 *
 * @author yangyi
 */
public class CouponResultMessage extends InternalPlayerMessage{
    public final String code;
    public final CertainRewardInfo reward;

    public CouponResultMessage(int playerId, String code, CertainRewardInfo reward) {
        super(MessageType.MAP_COUPON_RESULT, playerId);
        this.code = code;
        this.reward = reward;
    }
}
