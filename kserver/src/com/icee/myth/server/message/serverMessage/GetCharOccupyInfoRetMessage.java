/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.charInfo.CharOccupyInfo;
import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;

/**
 *
 * @author liuxianke
 */
public class GetCharOccupyInfoRetMessage extends InternalPlayerMessage {
    public final CharOccupyInfo charOccupyInfo;

    public GetCharOccupyInfoRetMessage(int playerId, CharOccupyInfo charOccupyInfo) {
        super(MessageType.MAP_GET_CHAR_OCCUPY_INFO_RET, playerId);

        this.charOccupyInfo = charOccupyInfo;
    }
}
