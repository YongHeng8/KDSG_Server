/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;
import com.icee.myth.protobuf.ExternalCommonProtocol.BattleResultProto;

/**
 *
 * @author liuxianke
 */
public class BaseResistanceBattleResultMessage extends InternalPlayerMessage {
    public final BattleResultProto battleResult;
    public final int king;
    public final boolean stimulate;

    public BaseResistanceBattleResultMessage(int playerId, int king, boolean stimulate, BattleResultProto battleResult) {
        super(MessageType.MAP_BASE_RESISTANCE_BATTLERESULT, playerId);

        this.battleResult = battleResult;
        this.king = king;
        this.stimulate = stimulate;
    }
}
