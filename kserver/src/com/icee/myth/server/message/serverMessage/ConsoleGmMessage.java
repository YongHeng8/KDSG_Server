/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.SimpleMessage;
import com.icee.myth.protobuf.InternalCommonProtocol.M2SGmProto;

/**
 *
 * @author yangyi
 */
public class ConsoleGmMessage extends SimpleMessage{

    public final M2SGmProto gmProto;

    public ConsoleGmMessage(M2SGmProto gmProto) {
        super(MessageType.ALL_GM);
        this.gmProto = gmProto;
    }

}
