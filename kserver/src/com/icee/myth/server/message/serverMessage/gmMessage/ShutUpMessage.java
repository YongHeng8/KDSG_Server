/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage.gmMessage;

/**
 *
 * @author yangyi
 */
public class ShutUpMessage extends GMMessage{
    public final int playerId;
    public final int time;

    public ShutUpMessage(int playerId, int time) {
        super(GMMessageType.GM_SHUTUP);
        this.playerId = playerId;
        this.time = time;
    }
}
