/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage.gmMessage;

/**
 *
 * @author yangyi
 */
public class AddNormalActivityMessage extends GMMessage {
    public final String activityJsonString;
    public boolean response = false;
    public boolean hasWriteDB = false;
    
    public AddNormalActivityMessage(String activityJsonString) {
        super(GMMessageType.GM_ADD_NORMAL_ACTIVITY);
        this.activityJsonString = activityJsonString;
    }

}
