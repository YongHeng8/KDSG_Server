/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;
import com.icee.myth.protobuf.ExternalCommonProtocol.BattleResultProto;

/**
 *
 * @author liuxianke
 */
public class BaseAttackBattleResultMessage extends InternalPlayerMessage {
    public final BattleResultProto battleResult;
    public final int targetIndex;
    public final int targetId;

    public BaseAttackBattleResultMessage(int playerId, int targetIndex, int targetId, BattleResultProto battleResult) {
        super(MessageType.MAP_BASE_ATTACK_BATTLERESULT, playerId);

        this.battleResult = battleResult;
        this.targetIndex = targetIndex;
        this.targetId = targetId;
    }
}
