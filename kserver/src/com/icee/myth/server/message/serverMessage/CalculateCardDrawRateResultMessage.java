/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.SimpleMessage;
import java.util.TreeMap;

/**
 *
 * @author liuxianke
 */
public class CalculateCardDrawRateResultMessage extends SimpleMessage {
    public final TreeMap<Integer, Integer> result;

    public CalculateCardDrawRateResultMessage(TreeMap<Integer, Integer> result) {
        super(MessageType.ALL_CALCULATE_CARD_DRAW_RATE_RESULT);
        this.result = result;
    }
}
