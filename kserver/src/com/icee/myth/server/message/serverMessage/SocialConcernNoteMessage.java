/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;

/**
 *
 * @author liuxianke
 */
public class SocialConcernNoteMessage extends InternalPlayerMessage {

    public final int targetPlayerId;

    public SocialConcernNoteMessage(int playerId, int targetPlayerId) {
        super(MessageType.MAP_SOCIAL_CONCERN_NOTE, playerId);
        this.targetPlayerId = targetPlayerId;
    }
}
