/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;

/**
 *
 * @author liuxianke
 */
public class EnterActivityStageMessage extends InternalPlayerMessage {
    public final int activityId;
    public final int itemId;
    public final int helperId;

    public EnterActivityStageMessage(int playerId, int activityId, int itemId, int helperId) {
        super(MessageType.MAP_NORMAL_ACTIVITY_ENTER_STAGE, playerId);
        this.activityId = activityId;
        this.itemId = itemId;
        this.helperId = helperId;
    }
}
