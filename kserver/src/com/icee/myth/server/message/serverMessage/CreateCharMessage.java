/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;
import com.icee.myth.protobuf.ExternalCommonProtocol.CreateCharProto;

/**
 * CreateCharLGWMessage是收到玩家创建角色命令时产生的消息
 * @author liuxianke
 */
public class CreateCharMessage extends InternalPlayerMessage {

    public final CreateCharProto charInfo;

    public CreateCharMessage(int playerId, CreateCharProto charInfo) {
        super(MessageType.MAP_CREATE_CHAR, playerId);

        this.charInfo = charInfo;
    }

}
