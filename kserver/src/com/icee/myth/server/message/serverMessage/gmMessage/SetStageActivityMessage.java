/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage.gmMessage;

/**
 *
 * @author liuxianke
 */
public class SetStageActivityMessage extends GMMessage {
    public final String activityJsonString;
    public boolean response = false;

    public SetStageActivityMessage(String activityJsonString) {
        super(GMMessageType.GM_SET_STAGE_ACTIVITY);
        this.activityJsonString = activityJsonString;
    }
}
