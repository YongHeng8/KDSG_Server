/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;
import com.icee.myth.protobuf.ExternalCommonProtocol.BattleResultProto;

/**
 *
 * @author liuxianke
 */
public class HegemonyBattleResultMessage extends InternalPlayerMessage {
    public final int targetSlot;
    public final BattleResultProto battleResult;
    public final int targetPower;
    public final boolean stimulate;

    public HegemonyBattleResultMessage(int playerId, int targetSlot, int targetPower, boolean stimulate, BattleResultProto battleResult) {
        super(MessageType.MAP_HEGEMONY_BATTLERESULT, playerId);

        this.targetSlot = targetSlot;
        this.battleResult = battleResult;
        this.targetPower = targetPower;
        this.stimulate = stimulate;
    }
}
