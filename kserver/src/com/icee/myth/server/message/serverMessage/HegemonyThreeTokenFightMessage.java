/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;

/**
 *
 * @author liuxianke
 */
public class HegemonyThreeTokenFightMessage extends InternalPlayerMessage {
    public final int targetSlot;

    public HegemonyThreeTokenFightMessage(int playerId, int targetSlot) {
        super(MessageType.MAP_HEGEMONY_THREE_TOKEN_FIGHT, playerId);

        this.targetSlot = targetSlot;
    }
}
