/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage.gmMessage;

/**
 *
 * @author yangyi
 */
public class KickPlayerMessage extends GMMessage{
    public final int playerId;
    public KickPlayerMessage(int playerId) {
        super(GMMessageType.GM_KICK_PLAYER);
        this.playerId = playerId;
    }
}
