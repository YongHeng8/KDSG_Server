/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;

/**
 *
 * @author liuxianke
 */
public class BaseAttackMessage extends InternalPlayerMessage{

    public final int targetIndex;
    public final int targetId;

    public BaseAttackMessage(int playerId, int targetIndex, int targetId) {
        super(MessageType.MAP_BASE_ATTACK, playerId);
        this.targetIndex = targetIndex;
        this.targetId = targetId;
    }
}
