/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;

/**
 *
 * @author liuxianke
 */
public class BaseHarvestCapeMineMessage extends InternalPlayerMessage{

    public final int capeId;

    public BaseHarvestCapeMineMessage(int playerId, int capeId) {
        super(MessageType.MAP_BASE_HARVEST_CAPE_MINE, playerId);
        this.capeId = capeId;
    }
}
