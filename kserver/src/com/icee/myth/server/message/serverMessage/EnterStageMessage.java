/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;

/**
 *
 * @author liuxianke
 */
public class EnterStageMessage extends InternalPlayerMessage {
    public final int stageId;
    public final boolean isBigStage;
    public final int helper;

    public EnterStageMessage(int playerId, int stageId, boolean isBigStage, int helper) {
        super(MessageType.MAP_STAGE_ENTER, playerId);

        this.stageId = stageId;
        this.isBigStage = isBigStage;
        this.helper = helper;
    }
}
