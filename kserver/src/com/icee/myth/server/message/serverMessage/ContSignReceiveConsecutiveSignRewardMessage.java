/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;

/**
 *
 * @author yangyi
 */
public class ContSignReceiveConsecutiveSignRewardMessage extends InternalPlayerMessage{

    public final int day;

    public ContSignReceiveConsecutiveSignRewardMessage(int playerId, int day) {
        super(MessageType.MAP_CONTSIGN_RECEIVE_CONSECUTIVE_SIGN_REWARD, playerId);

        this.day = day;
    }

}
