/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;

/**
 *
 * @author liuxianke
 */
public class ItemCombineMessage extends InternalPlayerMessage {
    public final int itemId;

    public ItemCombineMessage(int playerId, int itemId) {
        super(MessageType.MAP_ITEM_COMBINE, playerId);

        this.itemId = itemId;
    }
}
