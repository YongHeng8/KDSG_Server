/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.serverMessage;

import com.icee.myth.common.message.serverMessage.InternalPlayerMessage;
import com.icee.myth.protobuf.InternalCommonProtocol.DBRelationProto;

/**
 *
 * @author liuxianke
 */
public class GetRelationRetMessage extends InternalPlayerMessage {
    public final DBRelationProto relationProto;
    public final boolean foundPlayer;

    public GetRelationRetMessage(int playerId, DBRelationProto relationProto, boolean foundPlayer) {
        super(MessageType.MAP_GET_RELATION_RET, playerId);

        this.relationProto = relationProto;
        this.foundPlayer = foundPlayer;
    }
}
