/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.message.networkSendThreadMessage;

import com.icee.myth.common.channelContext.ChannelContext;
import com.icee.myth.common.message.networkSendThreadMessage.SimpleNetworkSendThreadMessage;

/**
 *
 * @author liuxianke
 */
public class AddChannelContextNSTMessage extends SimpleNetworkSendThreadMessage {
    public final ChannelContext channelContext;

    public AddChannelContextNSTMessage(ChannelContext channelContext) {
        super(NetworkSendThreadMessageType.NSTMT_ADDCHANNELCONTEXT);

        this.channelContext = channelContext;
    }
}
