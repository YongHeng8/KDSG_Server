/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.activity.normalActivity;

import com.icee.myth.protobuf.InternalCommonProtocol.DBNormalActivityItemStatusProto;
import com.icee.myth.protobuf.InternalCommonProtocol.DBNormalActivityStatusProto;
import java.util.List;
import java.util.TreeMap;

/**
 *
 * @author liuxianke
 */
public class NormalActivityStatus {
    public final int id;    // 活动号
    public final TreeMap<Integer, NormalActivityItemStatus> itemStatuses = new TreeMap<Integer, NormalActivityItemStatus>();    // 活动项目状态，key为项目号

    public NormalActivityStatus(int id) {
        this.id = id;
    }

    public NormalActivityStatus(DBNormalActivityStatusProto dbNormalActivityStatusProto) {
        this.id = dbNormalActivityStatusProto.getActivityId();

        if (dbNormalActivityStatusProto.getItemStatusesCount() > 0) {
            List<DBNormalActivityItemStatusProto> itemStatusProtos = dbNormalActivityStatusProto.getItemStatusesList();
            for (DBNormalActivityItemStatusProto itemStatusProto : itemStatusProtos) {
                itemStatuses.put(itemStatusProto.getItemId(), new NormalActivityItemStatus(itemStatusProto));
            }
        }
    }

    public DBNormalActivityStatusProto buildDBNormalActivityStatusProto() {
        DBNormalActivityStatusProto.Builder builder = DBNormalActivityStatusProto.newBuilder();

        builder.setActivityId(id);

        for (NormalActivityItemStatus itemStatus : itemStatuses.values()) {
            builder.addItemStatuses(itemStatus.buildDBNormalActivityItemStatusProto());
        }

        return builder.build();
    }

    public NormalActivityItemStatus getItemStatus(int itemId) {
        NormalActivityItemStatus itemStatus = itemStatuses.get(itemId);

        if (itemStatus == null) {
            itemStatus = new NormalActivityItemStatus(itemId, 0, false);
            itemStatuses.put(itemId, itemStatus);
        }

        return itemStatus;
    }

    public void removeItemStatus(int itemId) {
        itemStatuses.remove(itemId);
    }
}
