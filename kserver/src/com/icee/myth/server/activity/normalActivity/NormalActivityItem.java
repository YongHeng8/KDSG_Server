/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.server.activity.normalActivity;

import com.icee.myth.config.MapConfig;
import com.icee.myth.log.GameLogger;
import com.icee.myth.log.message.FileDebugGameLogMessage;
import com.icee.myth.log.message.GameLogMessage.GameLogType;
import com.icee.myth.log.message.builder.GameLogMessageBuilder;
import com.icee.myth.protobuf.builder.ClientToMapBuilder;
import com.icee.myth.server.actor.Human;
import com.icee.myth.server.reward.CertainRewardInfo;
import com.icee.myth.utils.Consts;
import com.icee.myth.utils.IdValue;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author liuxianke
 */
public class NormalActivityItem {

    public int id;          // 活动项id（所属活动中的下标）
    public int type;        // 活动项类型（0关卡 1充值 2消费 3兑换）
    public String title;    // 活动项标题
    public String icon;     // 活动项图标
    public CertainRewardInfo reward;    // 奖励（对关卡活动无效）
    public int value1;      // 值1（关卡活动为次数；充值活动为充值数）
    public int value2;      // 值2（关卡活动为关卡号）
    public IdValue[] exchangeItems; // 兑换物品列表（兑换活动用）
    public boolean isDaily; // 是否每日任务

    private boolean canGetReward(Human human, NormalActivityItemStatus activityItemStatus) {
        switch (type) {
            case Consts.NORMAL_ACTIVITY_ITEM_TYPE_STAGE:
            case Consts.NORMAL_ACTIVITY_ITEM_TYPE_CHARGE:
            case Consts.NORMAL_ACTIVITY_ITEM_TYPE_CONSUME: {
                return activityItemStatus.value >= value1;
            }
            case Consts.NORMAL_ACTIVITY_ITEM_TYPE_EXCHANGE: {
                return human.items.containItems(exchangeItems);
            }
        }
        return false;
    }

    public void getReward(Human human, int activityId, NormalActivityItemStatus activityItemStatus) {
        if (reward != null) {
            if (!activityItemStatus.gotten) {
                if (canGetReward(human, activityItemStatus)) {
                    human.digestCertainReward(reward, Consts.SOUL_CHANGE_LOG_TYPE_GETREWARD_NORMAL_ACTIVITY, activityId);

                    // 消耗兑换物品
                    if (type == Consts.NORMAL_ACTIVITY_ITEM_TYPE_EXCHANGE) {
                        if ((exchangeItems != null) && (exchangeItems.length > 0)) {
                            human.items.consumeItems(exchangeItems, true);
                        }
                    }

                    // 设置已领取标志
                    activityItemStatus.gotten = true;

                    // 通知客户端奖励已领取
                    human.sendMessage(ClientToMapBuilder.buildNormalActivityRewardGotten(activityId, id));

                    // 行为日志
                    if (MapConfig.INSTANCE.useCYLog == true) {
                        List<String> cyLogList = new ArrayList<String>();
                        cyLogList.add("behaviorMC");
                        cyLogList.add(String.valueOf(MapConfig.INSTANCE.gameId));
                        cyLogList.add("KDTY");
                        cyLogList.add(String.valueOf(human.id));
                        cyLogList.add(human.name);
                        cyLogList.add("");
                        cyLogList.add("GetNormalActivityItemRewardDBBehavior");
                        cyLogList.add(activityId + " " + id);
                        cyLogList.add("");
                        cyLogList.add("");
                        cyLogList.add("");
                        cyLogList.add("");
                        cyLogList.add("");
                        GameLogger.getlogger().log(GameLogMessageBuilder.buildFileCYGameLogMessage(cyLogList, GameLogType.GAMELOGTYPE_CY_BEHAVIOR));
                    }
                    GameLogger.getlogger().log(GameLogMessageBuilder.buildGetNormalActivityItemRewardDBBehaviorGameLogMessage(human.id, activityId, id));
                } else {
                    GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                            FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                            "Player[" + human.id + "] can't get normal activity item[" + id + "] reward now."));
                }
            } else {
                GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                        FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                        "Player[" + human.id + "] can't get normal activity item[" + id + "] reward because already gotten."));
            }
        } else {
            GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                    FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                    "Player[" + human.id + "] can't get normal activity[" + activityId + "] item[" + id + "] reward because no reward."));
        }
    }

    public void enterActivityStage(Human human, int activityId, NormalActivityItemStatus activityItemStatus, int helperId) {
        if (type == Consts.NORMAL_ACTIVITY_ITEM_TYPE_STAGE) {
            if (activityItemStatus.value < value1) {
                human.enterActivityStage(value2, helperId, activityId, id);
            } else {
                GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                        FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                        "Player[" + human.id + "] can't enter activity[" + activityId + "] item[" + id + "] stage because times limit."));
            }
        } else {
            GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                    FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                    "Player[" + human.id + "] can't enter activity[" + activityId + "] item[" + id + "] stage because it is not a stage activity."));
        }
    }
}
