/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.activity.normalActivity;

import com.icee.myth.protobuf.InternalCommonProtocol.DBNormalActivityItemStatusProto;

/**
 *
 * @author liuxianke
 */
public class NormalActivityItemStatus {
    public int id;          // 活动项id
    public int value;       // 若关卡任务表示参与次数；若是充值任务表示充值金额
    public boolean gotten;  // 是否已领取（对关卡活动无效）

    public NormalActivityItemStatus(int id, int value, boolean gotten) {
        this.id = id;
        this.value = value;
        this.gotten = gotten;
    }

    public NormalActivityItemStatus(DBNormalActivityItemStatusProto itemStatusProto) {
        this(itemStatusProto.getItemId(), itemStatusProto.getValue(), itemStatusProto.getGotten());
    }

    public DBNormalActivityItemStatusProto buildDBNormalActivityItemStatusProto() {
        DBNormalActivityItemStatusProto.Builder builder = DBNormalActivityItemStatusProto.newBuilder();

        builder.setItemId(id);
        builder.setValue(value);
        builder.setGotten(gotten);

        return builder.build();
    }
}
