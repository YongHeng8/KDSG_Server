/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.server.activity.cardDrawActivity;

import com.icee.myth.config.MapConfig;
import com.icee.myth.protobuf.ExternalCommonProtocol.CardDrawActivityProto;
import com.icee.myth.server.GameServer;
import com.icee.myth.server.card.cardDraw.CardDrawItem;

/**
 * 
 * @author liuxianke
 */
public class CardDrawActivityTemplate {
    public int cardDrawId;          // 卡包号
    public int priority;            // 优先级
    public CardDrawDiscountInfo discountInfo;   // 折扣信息
    public CardDrawAddCardInfo addCardInfo;     // 添加卡片信息

    public CardDrawActivityProto buildCardDrawActivityProto() {
        CardDrawActivityProto.Builder builder = CardDrawActivityProto.newBuilder();
        builder.setCardDrawId(cardDrawId);
        builder.setPriority(priority);

        if (discountInfo != null) {
            builder.setDiscountInfo(discountInfo.buildCardDrawDiscountProto());
        }

        if (addCardInfo != null) {
            builder.setAddCardInfo(addCardInfo.buildCardDrawAddCardProto());
        }

        return builder.build();
    }

    public int getAddCardProbabilityUpperBound() {
        if ((addCardInfo != null) && (addCardInfo.cardItems != null) && (addCardInfo.cardItems.length > 0)) {
            long currentTime = GameServer.INSTANCE.getCurrentTime();
            if ((addCardInfo.startTime.getTime() <= currentTime) && (addCardInfo.endTime.getTime() > currentTime)) {
                return addCardInfo.cardItems[addCardInfo.cardItems.length - 1].cardInfo.probabilityUpperBound;
            }
        }

        return 0;
    }

    public CardDrawItem getCardDrawItem(int probability) {
        for (CardDrawAddCardItem addCardItem : addCardInfo.cardItems) {
            if (addCardItem.cardInfo.probabilityUpperBound > probability) {
                return addCardItem.cardInfo;
            }
        }
        // 不可能到此
        assert (false);
        return null;
    }

    public float getDiscount() {
        if (discountInfo != null) {
            long currentTime = GameServer.INSTANCE.getCurrentTime();
            if ((discountInfo.startTime.getTime() <= currentTime) && (discountInfo.endTime.getTime() > currentTime)) {
                return MapConfig.INSTANCE.discounts[discountInfo.discountId];
            } else {
                return 1.0f;
            }
        } else {
            return 1.0f;
        }
    }
}
