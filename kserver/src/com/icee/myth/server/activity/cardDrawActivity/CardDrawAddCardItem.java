/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.activity.cardDrawActivity;

import com.icee.myth.protobuf.ExternalCommonProtocol.CardDrawAddCardItemProto;
import com.icee.myth.server.card.cardDraw.CardDrawItem;

/**
 *
 * @author liuxianke
 */
public class CardDrawAddCardItem {
    public CardDrawItem cardInfo;   // 卡片信息
    public int type;      // 类型（用于客户端显示：如概率双倍、加入新卡）

    public CardDrawAddCardItemProto buildCardDrawAddCardItemProto() {
        CardDrawAddCardItemProto.Builder builder = CardDrawAddCardItemProto.newBuilder();
        builder.setCardId(cardInfo.cardId);
        builder.setType(type);

        return builder.build();
    }
}
