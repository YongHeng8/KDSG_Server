/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.activity.stageActivity;

import com.icee.myth.protobuf.ExternalCommonProtocol.StageActivitiesProto;
import com.icee.myth.server.GameServer;
import java.util.TreeMap;

/**
 *
 * @author liuxianke
 */
public class StageActivityTemplates {
    public StageCardDropMultipleInfo cardDropMultipleInfo;  // 全局掉卡倍数信息
    public StageHalfEnergyInfo halfEnergyInfo;              // 全局体力减半信息
    public final TreeMap<Integer, StageActivityTemplate> stageActivities = new TreeMap<Integer, StageActivityTemplate>();   // 某些关卡掉卡和体力减半信息

    public static final StageActivityTemplates INSTANCE = new StageActivityTemplates();

    public StageActivitiesProto stageActivitiesProto = null;

    private StageActivityTemplates() {
    }

    public void clear() {
        cardDropMultipleInfo = null;
        halfEnergyInfo = null;
        stageActivities.clear();
        stageActivitiesProto = null;
    }

    public void addStageActivityTemplate(StageActivityTemplate template) {
        stageActivities.put(template.stageId, template);
    }

    public int getStageCardDropMultiple(int stageId) {
        long currentTime = GameServer.INSTANCE.getCurrentTime();
        if ((cardDropMultipleInfo != null) && (cardDropMultipleInfo.startTime.getTime() <= currentTime) && (cardDropMultipleInfo.endTime.getTime() > currentTime)) {
            return cardDropMultipleInfo.multiple;
        }

        StageActivityTemplate stageActivityTemplate = stageActivities.get(stageId);
        if ((stageActivityTemplate != null) && (stageActivityTemplate.cardDropMultipleInfo != null) && (stageActivityTemplate.cardDropMultipleInfo.startTime.getTime() <= currentTime) && (stageActivityTemplate.cardDropMultipleInfo.endTime.getTime() > currentTime)) {
            return stageActivityTemplate.cardDropMultipleInfo.multiple;
        }

        return 1;
    }

    public boolean halfEnergy(int stageId) {
        long currentTime = GameServer.INSTANCE.getCurrentTime();
        if ((halfEnergyInfo != null) && (halfEnergyInfo.startTime.getTime() <= currentTime) && (halfEnergyInfo.endTime.getTime() > currentTime)) {
            return true;
        }

        StageActivityTemplate stageActivityTemplate = stageActivities.get(stageId);
        return ((stageActivityTemplate != null) && (stageActivityTemplate.halfEnergyInfo != null) && (stageActivityTemplate.halfEnergyInfo.startTime.getTime() <= currentTime) && (stageActivityTemplate.halfEnergyInfo.endTime.getTime() > currentTime));
    }

    public StageActivitiesProto buildStageActivitiesProto() {
        if (stageActivitiesProto == null) {
            StageActivitiesProto.Builder builder = StageActivitiesProto.newBuilder();

            for (StageActivityTemplate template : stageActivities.values()) {
                builder.addStageActivities(template.buildStageActivityProto());
            }

            if (cardDropMultipleInfo != null) {
                builder.setCardDropMultipleInfo(cardDropMultipleInfo.buildStageCardDropMultipleProto());
            }

            if (halfEnergyInfo != null) {
                builder.setHalfEnergyInfo(halfEnergyInfo.buildStageHalfEnergyProto());
            }

            stageActivitiesProto = builder.build();
        }

        return stageActivitiesProto;
    }
}
