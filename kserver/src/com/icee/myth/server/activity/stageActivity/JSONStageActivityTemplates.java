/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.activity.stageActivity;

/**
 *
 * @author liuxianke
 */
public class JSONStageActivityTemplates {
    public StageCardDropMultipleInfo cardDropMultipleInfo;  // 全局掉卡倍数信息
    public StageHalfEnergyInfo halfEnergyInfo;              // 全局体力减半信息
    public StageActivityTemplate[] templates;               // 某些关卡掉卡和体力减半信息

    public void buildStageActivityTemplates() {
        StageActivityTemplates.INSTANCE.clear();
        StageActivityTemplates.INSTANCE.cardDropMultipleInfo = cardDropMultipleInfo;
        StageActivityTemplates.INSTANCE.halfEnergyInfo = halfEnergyInfo;
        if ((templates != null) && (templates.length > 0)) {
            for (StageActivityTemplate template : templates) {
                StageActivityTemplates.INSTANCE.addStageActivityTemplate(template);
            }
        }
    }
}
