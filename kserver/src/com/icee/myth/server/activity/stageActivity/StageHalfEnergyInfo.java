/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.activity.stageActivity;

import com.icee.myth.protobuf.ExternalCommonProtocol.StageHalfEnergyProto;
import java.util.Date;

/**
 *
 * @author liuxianke
 */
public class StageHalfEnergyInfo {
    public Date startTime;  // 开始时间
    public Date endTime;    // 结束时间

    public StageHalfEnergyProto buildStageHalfEnergyProto() {
        StageHalfEnergyProto.Builder builder = StageHalfEnergyProto.newBuilder();

        builder.setStartTime(startTime.getTime());
        builder.setEndTime(endTime.getTime());

        return builder.build();
    }
}
