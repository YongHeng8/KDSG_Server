/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.contsign;

import com.icee.myth.server.GameServer;
import com.icee.myth.server.reward.CertainRewardInfo;
import com.icee.myth.utils.Consts;
import com.icee.myth.utils.JSONHelper;

/**
 *
 * @author yangyi
 */
public class ContSignTemplates {
    public CertainRewardInfo[] cumulativeRewards;   // 累计登陆奖励
    public CertainRewardInfo[] consecutiveRewards;  // 连续登陆奖励
    public LivenessRewardTemplate[] livenessRewards;    // 活跃度奖励
    public LivenessTargetTemplate livenessTarget;       // 活跃度目标

    public static ContSignTemplates INSTANCE = JSONHelper.parseFileNoException(GameServer.INSTANCE.serverId + "/" + Consts.CONTSIGN_TEMPLATES_FILEPATH, ContSignTemplates.class);

    private ContSignTemplates(){
        
    }
}
