/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.battle;

import com.icee.myth.protobuf.ExternalCommonProtocol.ChangeCardProto;

/**
 * 乱入信息
 * @author liuxianke
 */
public class ChangeCardInfo {
    public final int slotId;
    public final int fromCardId;
    public final int toCardId;

    public ChangeCardInfo(int slotId, int fromCardId, int toCardId) {
        this.slotId = slotId;
        this.fromCardId = fromCardId;
        this.toCardId = toCardId;
    }

    public ChangeCardProto buildChangeCardProto() {
        ChangeCardProto.Builder builder = ChangeCardProto.newBuilder();
        builder.setSlotId(slotId);
        builder.setFromCardId(fromCardId);
        builder.setToCardId(toCardId);

        return builder.build();
    }
}
