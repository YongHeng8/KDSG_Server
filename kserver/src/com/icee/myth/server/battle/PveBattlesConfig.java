/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.battle;

import com.icee.myth.log.GameLogger;
import com.icee.myth.log.message.FileDebugGameLogMessage;
import com.icee.myth.log.message.builder.GameLogMessageBuilder;
import java.util.HashMap;

/**
 *
 * @author liuxianke
 */
public class PveBattlesConfig {
    public final static PveBattlesConfig INSTANCE = new PveBattlesConfig();

    public HashMap<Integer, PveBattleStaticInfo>  battleStaticInfos = new HashMap<Integer, PveBattleStaticInfo>();

    private PveBattlesConfig() {
    }

    public void addPveBattleStaticInfo(PveBattleStaticInfo staticInfo) {
        if (battleStaticInfos.put(staticInfo.id, staticInfo) != null) {
            GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                    FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                    "Duplicated pve battle[" + staticInfo.id +"]"));
        }
    }

    public PveBattleStaticInfo getPveBattleStaticInfo(int battleId) {
        return battleStaticInfos.get(battleId);
    }
}
