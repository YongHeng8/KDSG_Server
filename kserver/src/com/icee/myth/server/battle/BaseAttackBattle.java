/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.battle;

import com.icee.myth.common.messageQueue.ServerMessageQueue;
import com.icee.myth.log.GameLogger;
import com.icee.myth.log.message.FileDebugGameLogMessage;
import com.icee.myth.log.message.builder.GameLogMessageBuilder;
import com.icee.myth.server.message.serverMessage.builder.MapMessageBuilder;
import com.icee.myth.utils.StackTraceUtil;

/**
 * 据点征讨战
 * @author liuxianke
 */
public class BaseAttackBattle extends Battle {
    private final int playerId;     // 发起战斗的玩家id（用于在结果计算完成后返回给playerId对应的玩家）
    private final int targetIndex;  // 战斗所属目标下标（用于返回玩家状态机处理时进行校验）
    private final int targetId;     // 战斗所属目标id（用于返回玩家状态机处理时进行校验）

    public BaseAttackBattle(int playerId, int targetIndex, int targetId) {
        super(true);
        this.playerId = playerId;
        this.targetIndex = targetIndex;
        this.targetId = targetId;
    }

    @Override
    public void run() {
        try {
            calculateBattleResult();

            // 发送战斗结果消息给主逻辑线程
            ServerMessageQueue.queue().offer(MapMessageBuilder.buildBaseAttackBattleResultMessage(playerId, targetIndex, targetId, battleResultBuilder.build()));
        } catch (Exception e) {
            GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                    FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                    StackTraceUtil.getStackTrace(e)));
        }
    }
}
