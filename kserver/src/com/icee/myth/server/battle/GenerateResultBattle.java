/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.battle;

import com.icee.myth.log.GameLogger;
import com.icee.myth.log.message.FileDebugGameLogMessage;
import com.icee.myth.log.message.builder.GameLogMessageBuilder;
import com.icee.myth.utils.StackTraceUtil;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;

/**
 * 
 * @author liuxianke
 */
public class GenerateResultBattle extends Battle {
    public final String outputFileName;
    
    public GenerateResultBattle(String outputFileName) {
        super(true);

        this.outputFileName = outputFileName;
    }

    @Override
    public void run() {
        try {
            calculateBattleResult();

            // 将战斗结果写入指定文件中
            File file = new File(outputFileName);
            DataOutputStream out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(file)));
            out.write(battleResultBuilder.build().toByteArray());
            out.close();
        } catch (Exception e) {
            GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                    FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                    StackTraceUtil.getStackTrace(e)));
        }
    }
}
