/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.battle;

/**
 * 战位中的怪物选择配置
 * 对应着battleConfig.json里的"battleEnemySlots"
 * @author liuxianke
 */
public class PveBattleSlotEnemyConfig {
    public int slotId;  // 位置号
    public PveBattleSlotEnemyPropability[] enemyPropabilitys;   // 怪物出现概率
    public boolean isBoss;  // 是否boss
}
