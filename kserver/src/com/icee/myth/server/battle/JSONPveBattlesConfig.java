/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.battle;

/**
 *
 * @author liuxianke
 */
public class JSONPveBattlesConfig {
    public PveBattleStaticInfo[] battleStaticInfos;   // 场景战斗模板数组

    public void buildPveBattlesConfig() {
        if ((battleStaticInfos != null) && (battleStaticInfos.length > 0)) {
            for (PveBattleStaticInfo staticInfo : battleStaticInfos) {
                PveBattlesConfig.INSTANCE.addPveBattleStaticInfo(staticInfo);
            }
        }
    }
}
