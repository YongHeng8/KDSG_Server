/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.battle;

import com.icee.myth.common.messageQueue.ServerMessageQueue;
import com.icee.myth.log.GameLogger;
import com.icee.myth.log.message.FileDebugGameLogMessage;
import com.icee.myth.log.message.builder.GameLogMessageBuilder;
import com.icee.myth.server.message.serverMessage.builder.MapMessageBuilder;
import com.icee.myth.utils.StackTraceUtil;

/**
 *
 * @author liuxianke
 */
public class HegemonyBattle extends Battle {
    private final int playerId;     // 发起战斗的玩家id（用于在结果计算完成后返回给playerId对应的玩家）
    private final int targetSlot;   // 战斗所属争霸目标槽位（用于返回玩家状态机处理时进行校验）
    private final int targetPower;  // 对手总战力（用于在战斗结束后计算奖励）
    private final boolean stimulate;// 破釜沉舟（用于返回客户端做表现）

    public HegemonyBattle(int playerId, int targetSlot, int targetPower, boolean stimulate) {
        super(true);
        this.playerId = playerId;
        this.targetSlot = targetSlot;
        this.targetPower = targetPower;
        this.stimulate = stimulate;
    }

    @Override
    public void run() {
        try {
            calculateBattleResult();

            // 发送战斗结果消息给主逻辑线程
            ServerMessageQueue.queue().offer(MapMessageBuilder.buildHegemonyBattleResultMessage(playerId, targetSlot, targetPower, stimulate, battleResultBuilder.build()));
        } catch (Exception e) {
            GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                    FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                    StackTraceUtil.getStackTrace(e)));
        }
    }
}
