/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.mail;

import com.icee.myth.protobuf.ExternalCommonProtocol.MailBriefProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.MailContentProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.MailProto;
import com.icee.myth.server.reward.CertainRewardInfo;

/**
 *
 * @author liuxianke
 */
public class Mail {
    public final long id;
    public final String title;
    public final String description;
    public final CertainRewardInfo reward;
    public int status;  // 状态（0：未读 1：已读 2：已领取）

    public Mail(long id, String title, String description, CertainRewardInfo reward, int status) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.reward = reward;
        this.status = status;
    }

    public MailProto buildMailProto() {
        MailProto.Builder builder1 = MailProto.newBuilder();
        builder1.setMailId(id);

        MailContentProto.Builder builder2 = MailContentProto.newBuilder();
        builder2.setTitle(title);
        builder2.setDescription(description);

        if (reward != null) {
            builder2.setReward(reward.buildRewardProto());
        }

        builder1.setContent(builder2);

        builder1.setStatus(status);

        return builder1.build();
    }

    public MailBriefProto buildMailBriefProto() {
        MailBriefProto.Builder builder = MailBriefProto.newBuilder();
        builder.setMailId(id);
        builder.setTitle(title);
        builder.setStatus(status);
        builder.setHasReward(reward!=null);
        return builder.build();
    }
}
