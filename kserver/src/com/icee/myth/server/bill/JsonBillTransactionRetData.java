/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.bill;

/**
 *
 * @author yangyi
 */
public class JsonBillTransactionRetData {
    public int status;
    public String error;

    JsonBillTransactionRetData(int status, String error) {
        this.status = status;
        this.error = error;
    }
}
