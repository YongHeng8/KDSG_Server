/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.bill;

/**
 *
 * @author yangyi
 */
public class Order {
    public String no;
    public int currencyId;
    public String goodsId;
    public int amount;
    public int price;
    public String memo;

    public Order(String orderno, int currencyId, String goodsId, int amount, int price, String memo) {
        this.no = orderno;
        this.amount = amount;
        this.memo = memo;
        this.currencyId = currencyId;
        this.goodsId = goodsId;
        this.price = price;
    }
}
