/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.bill;

import java.util.ArrayList;

/**
 *
 * @author yangyi
 */
public class JsonBillGetAssetRetData {
    public int status;
    public ArrayList<AssetDetailData> assetList;
    //public int rmb;
    //public int yellowsoul;
    public String error;

    public JsonBillGetAssetRetData(int status, ArrayList<AssetDetailData> assetList, String error){
        this.status = status;
        //this.rmb = rmb;
        //this.yellowsoul = yellowsoul;
        this.error = error;
        this.assetList = new ArrayList<AssetDetailData>();

        for (AssetDetailData assetDetailData : assetList){
            this.assetList.add(new AssetDetailData(assetDetailData.goodsId, assetDetailData.num));
        }
    }
}
