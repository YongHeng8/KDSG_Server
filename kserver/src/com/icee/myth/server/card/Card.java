/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.card;

import com.icee.myth.common.messageQueue.ServerMessageQueue;
import com.icee.myth.config.MapConfig;
import com.icee.myth.log.GameLogger;
import com.icee.myth.log.message.GameLogMessage.GameLogType;
import com.icee.myth.log.message.builder.GameLogMessageBuilder;
import com.icee.myth.protobuf.ExternalCommonProtocol.CardProto;
import com.icee.myth.protobuf.builder.ClientToMapBuilder;
import com.icee.myth.server.GameServer;
import com.icee.myth.server.actor.Human;
import com.icee.myth.server.base.occupy.OccupyInfo;
import com.icee.myth.server.base.occupy.OccupyInfos;
import com.icee.myth.server.message.serverMessage.builder.MapMessageBuilder;
import com.icee.myth.utils.Consts;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author liuxianke
 */
public class Card implements Comparable<Card> {
    public final Human human;
    public final int id;      // 卡片实例id
    public CardStaticInfo staticInfo;
    public int level;   // 卡片等级
    public int experience;     // 卡片当前等级经验

    public int place;   // 卡片所在位置（0无 1阵型中 ...）

    public Card(Human human, CardStaticInfo staticInfo, int id, int level, int experience) {
        this.human = human;
        this.staticInfo = staticInfo;
        this.id = id;
        this.level = level;
        this.experience = experience;
    }

    public CardProto buildCardProto() {
        CardProto.Builder builder = CardProto.newBuilder();
        builder.setId(staticInfo.id);
        builder.setInstId(id);
        builder.setLevel(level);
        builder.setExperience(experience);

        return builder.build();
    }

    public int getPower(float atkUp, float hpUp, int hitAndDodUp, int criAndTenUp) {
        return staticInfo.getPower(level, atkUp, hpUp, hitAndDodUp, criAndTenUp);
    }

    public void increaseExperience(int incValue, boolean needSend) {
        if (incValue > 0) {
            int maxLevel = staticInfo.maxLv;
            if (level < maxLevel) {
                int upLevel = 0;

                if (MapConfig.INSTANCE.useCYLog == true) {
                    List<String> cyLogList = new ArrayList<String>();
                    cyLogList.add("behaviorMC");
                    cyLogList.add(String.valueOf(MapConfig.INSTANCE.gameId));
                    cyLogList.add("KDTY");
                    cyLogList.add(String.valueOf(human.id));
                    cyLogList.add(human.name);
                    cyLogList.add("");
                    cyLogList.add("GetCardExperienceDBBehavior");
                    cyLogList.add(id + " " + level + " " + experience + " " + incValue);
                    cyLogList.add("");
                    cyLogList.add("");
                    cyLogList.add("");
                    cyLogList.add("");
                    cyLogList.add("");
                    GameLogger.getlogger().log(GameLogMessageBuilder.buildFileCYGameLogMessage(cyLogList, GameLogType.GAMELOGTYPE_CY_BEHAVIOR));
                }

                GameLogger.getlogger().log(GameLogMessageBuilder.buildGetCardExperienceDBBehaviorGameLogMessage(human.id, id, level, experience, incValue));

                experience += incValue;

                int levelupExperience = GameServer.INSTANCE.getCardLevelupExperience(level);
                while ((levelupExperience != -1) && (experience >= levelupExperience) && (level + upLevel < maxLevel)) {
                    upLevel++;
                    experience -= levelupExperience;
                    levelupExperience = GameServer.INSTANCE.getCardLevelupExperience(level + upLevel);
                }

                if (level + upLevel >= maxLevel) {
                    experience = 0;
                }

                if (upLevel > 0) {
                    levelup(upLevel);
                }

                if (needSend) {
                    human.sendMessage(ClientToMapBuilder.buildCardLevelExperienceChange(id, level, experience));
                }
            }
        }
    }

    // 升级
    public void levelup(int upLevel) {
        int oldLevel = level;
        level += upLevel;

        switch (place) {
            case Consts.PLACE_TYPE_SANDBOX: {
                // 若是队长卡片修改全局简略信息和据点信息
                if (human.sandbox.slots[human.sandbox.leader].id == id) {
                    GameServer.INSTANCE.briefPlayerInfos.setPlayerLeaderCard(human.id, staticInfo.id, level);

                    OccupyInfo occupyInfo = OccupyInfos.INSTANCE.getOccupyInfo(human.id);
                    if ((occupyInfo != null) && occupyInfo.inited) {
                        occupyInfo.leaderCardId = staticInfo.id;
                        occupyInfo.leaderCardLevel = level;
                    }

                    // 通知朋友队长信息改变
                    human.broadcastToFriends(ClientToMapBuilder.buildFriendLeaderCardChange(human.id, staticInfo.id, level));
                }

                human.checkMaxPowerChange();
                break;
            }
            case Consts.PLACE_TYPE_MINE: {
                // 如果是银矿卡片，刷新银矿产量值
                ServerMessageQueue.queue().offer(MapMessageBuilder.buildBaseMineProductionChangeMessage(human.id, human.mine.getProduction()));
                break;
            }
        }

        if (MapConfig.INSTANCE.useCYLog == true) {
            List<String> cyLogList = new ArrayList<String>();
            cyLogList.add("behaviorMC");
            cyLogList.add(String.valueOf(MapConfig.INSTANCE.gameId));
            cyLogList.add("KDTY");
            cyLogList.add(String.valueOf(human.id));
            cyLogList.add(human.name);
            cyLogList.add("");
            cyLogList.add("CardLevelupDBBehavior");
            cyLogList.add(id + " " + oldLevel + " " + level);
            cyLogList.add("");
            cyLogList.add("");
            cyLogList.add("");
            cyLogList.add("");
            cyLogList.add("");
            GameLogger.getlogger().log(GameLogMessageBuilder.buildFileCYGameLogMessage(cyLogList, GameLogType.GAMELOGTYPE_CY_BEHAVIOR));
        }

        GameLogger.getlogger().log(GameLogMessageBuilder.buildCardLevelupDBBehaviorGameLogMessage(human.id, id, oldLevel, level));
    }

    public int compareTo(Card o) {
        return 0;
    }
}
