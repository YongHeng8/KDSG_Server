/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.card;

/**
 *
 * @author liuxianke
 */
public class CardsConfig {
    public CardStaticInfo[] cardStaticInfos;

    public void buildCardFactories() {
        CardFactories cardFactories = CardFactories.INSTANCE;
        for (CardStaticInfo cardStaticInfo : cardStaticInfos) {
            CardFactory factory = new CardFactory(cardStaticInfo);

            cardFactories.addCardFactory(factory);
        }
    }
}
