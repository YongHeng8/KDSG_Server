/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.card.cardDraw;

import com.icee.myth.server.activity.cardDrawActivity.CardDrawActivityTemplate;
import com.icee.myth.server.activity.cardDrawActivity.CardDrawActivityTemplates;
import com.icee.myth.utils.RandomGenerator;

/**
 *
 * @author liuxianke
 */
public class CardDrawStaticInfo {
    public int id;  // 卡包id（数组下标）
    public int silverOneDraw;    // 一抽所需银币
    public int goldOneDraw;      // 一抽所需金币
    public int silverSixDraw;    // 六抽所需银币
    public int goldSixDraw;      // 六抽所需金币
    public int vip;                 // 所需vip等级
    public CardDrawItem[] contents; // 卡包内容
    public CardDrawItem[] addictions;   // 附加内容（用于新手首抽必出紫卡，和六连抽必出紫卡功能）

    /**
     * 随机draw卡
     * @return
     */
    public CardDrawItem draw() {
        CardDrawActivityTemplate cardDrawActivityTemplate = CardDrawActivityTemplates.INSTANCE.getCardDrawActivityTemplate(id);
        int probabilityUpperBound = contents[contents.length - 1].probabilityUpperBound;
        int addictionalUpperBound = (cardDrawActivityTemplate!=null)?cardDrawActivityTemplate.getAddCardProbabilityUpperBound():0;
        int ran = RandomGenerator.INSTANCE.generator.nextInt(probabilityUpperBound+addictionalUpperBound);
        
        if (ran < probabilityUpperBound) {
            for (CardDrawItem cardDrawItem : contents) {
                if (cardDrawItem.probabilityUpperBound > ran) {
                    return cardDrawItem;
                }
            }
        } else {
            return cardDrawActivityTemplate.getCardDrawItem(ran - probabilityUpperBound);
        }
        
        // 不可能到此
        assert (false);
        return null;
    }

    /**
     * 抽一张卡
     * @return
     */
    public CardDrawItem drawAddiction() {
        int probabilityUpperBound = addictions[addictions.length - 1].probabilityUpperBound;
        int ran = RandomGenerator.INSTANCE.generator.nextInt(probabilityUpperBound);

        for (CardDrawItem cardDrawItem : addictions) {
            if (cardDrawItem.probabilityUpperBound > ran) {
                return cardDrawItem;
            }
        }

        // 不可能到此
        assert (false);
        return null;
    }
}
