/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.skill;

/**
 *
 * @author liuxianke
 */
public class SkillStaticInfo {
    public int id;  // 技能号（注意：该id就是在skillTemplates数组中的下标）
    public int effectId;    // 给客户端的技能特效id
    public int targetType;  // 目标类型（0自身、1角色）
    public int targetChooseType;    // 目标选择类型(用于AI选择技能目标，0：前排单体（无前排时选后排）、1：后排单体（无后排时选前排）、2：最小生命百分比敌人、3：最小生命百分比友军、4：随机敌人、5：随机友军)
    public SkillHitPoint[] hitPoints;   // 打击点列表
}
