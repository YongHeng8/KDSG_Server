/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.skill;

/**
 *
 * @author liuxianke
 */
public class SkillsConfig {
    public SkillStaticInfo[] skillStaticInfos;

    public void buildSkills() {
        Skills skills = Skills.INSTANCE;
        for (SkillStaticInfo skillStaticInfo : skillStaticInfos) {
            Skill skill = new Skill(skillStaticInfo);

            skills.addSkill(skill);
        }
    }
}
