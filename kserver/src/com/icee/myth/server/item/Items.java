/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.item;

import com.icee.myth.config.MapConfig;
import com.icee.myth.log.GameLogger;
import com.icee.myth.log.message.FileDebugGameLogMessage;
import com.icee.myth.log.message.GameLogMessage.GameLogType;
import com.icee.myth.log.message.builder.GameLogMessageBuilder;
import com.icee.myth.protobuf.ExternalCommonProtocol.ItemProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.ItemsProto;
import com.icee.myth.protobuf.builder.ClientToMapBuilder;
import com.icee.myth.server.actor.Human;
import com.icee.myth.utils.IdValue;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;

/**
 *
 * @author liuxianke
 */
public class Items {
    public Human human;
    public final TreeMap<Integer, Integer> items = new TreeMap<Integer, Integer>();

    public Items(Human human, ItemsProto itemsProto) {
        this.human = human;

        if (itemsProto != null) {
            List<ItemProto> itemProtos = itemsProto.getItemsList();
            for (ItemProto itemProto : itemProtos) {
                items.put(itemProto.getId(), itemProto.getNum());
            }
        }
    }

    public ItemsProto buildItemsProto() {
        ItemsProto.Builder builder1 = ItemsProto.newBuilder();

        for (Iterator<Entry<Integer, Integer>> iter = items.entrySet().iterator(); iter.hasNext(); ) {
            Entry<Integer, Integer> entry = iter.next();
            ItemProto.Builder builder2 = ItemProto.newBuilder();
            builder2.setId(entry.getKey());
            builder2.setNum(entry.getValue());

            builder1.addItems(builder2);
        }

        return builder1.build();
    }

    public void addItem(int itemId, int addnum, boolean needSend) {
        Integer numI = items.get(itemId);
        int num = ((numI != null)?numI:0) + addnum;
        items.put(itemId, num);

        if (needSend) {
            human.sendMessage(ClientToMapBuilder.buildItemNumChange(itemId, num));
        }
    }

    public int getItemNum(int itemId) {
        Integer numI = items.get(itemId);

        return (numI != null)?numI:0;
    }

    public void consumeItem(IdValue item, boolean needSend) {
        Integer numI = items.get(item.id);
        if (numI != null) {
            int left = numI - item.value;
            if (left > 0) {
                items.put(item.id, left);
            } else {
                if (left < 0) {
                    GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                            FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                            "Player[" + human.id + "] consume " + (-left) + " more item[" + item.id + "] than own."));
                }

                items.remove(item.id);
            }

            human.sendMessage(ClientToMapBuilder.buildItemNumChange(item.id, left));
        }
    }

    public void consumeItems(IdValue[] needItems, boolean needSend) {
        for (IdValue idValue : needItems) {
            consumeItem(idValue, needSend);
        }
    }

    public boolean containItems(IdValue[] checkItems) {
        for (IdValue checkItem : checkItems) {
            Integer numI = items.get(checkItem.id);

            if ((numI == null) || (numI < checkItem.value)) {
                return false;
            }
        }
        return true;
    }

    /**
     * 合成itemId对应的物品
     * @param itemId 要合成的物品id
     */
    public void combine(int itemId) {
        ItemStaticInfo itemStaticInfo = ItemsConfig.INSTANCE.getItemStaticInfo(itemId);
        if (itemStaticInfo != null) {
            if ((itemStaticInfo.composeItems != null) && (itemStaticInfo.composeItems.length>0)) {
                if (containItems(itemStaticInfo.composeItems)) {
                    consumeItems(itemStaticInfo.composeItems, true);

                    addItem(itemId, 1, true);

                    human.sendMessage(ClientToMapBuilder.buildItemCombined(itemId));

                    // 记录行为日志
                    if (MapConfig.INSTANCE.useCYLog == true) {
                        List<String> cyLogList = new ArrayList<String>();
                        cyLogList.add("behaviorMC");
                        cyLogList.add(String.valueOf(MapConfig.INSTANCE.gameId));
                        cyLogList.add("KDTY");
                        cyLogList.add(String.valueOf(human.id));
                        cyLogList.add(human.name);
                        cyLogList.add("");
                        cyLogList.add("CombineItemDBBehavior");
                        cyLogList.add(itemId + " ");
                        cyLogList.add("");
                        cyLogList.add("");
                        cyLogList.add("");
                        cyLogList.add("");
                        cyLogList.add("");
                        GameLogger.getlogger().log(GameLogMessageBuilder.buildFileCYGameLogMessage(cyLogList, GameLogType.GAMELOGTYPE_CY_BEHAVIOR));
                    }
                    GameLogger.getlogger().log(GameLogMessageBuilder.buildCombineItemDBBehaviorGameLogMessage(human.id, itemId));
                } else {
                    GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                            FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                            "Player[" + human.id + "] can't combine item[" + itemId + "] because not enough materials."));
                }
            } else {
                GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                        FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                        "Player[" + human.id + "] can't combine item[" + itemId + "] because item is not combinable."));
            }
        } else {
            GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                    FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                    "Player[" + human.id + "] can't combine item[" + itemId + "] because no such item."));
        }
    }
}
