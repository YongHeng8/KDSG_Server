/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.server.rpc.handler;

import com.google.protobuf.GeneratedMessage;
import com.icee.myth.protobuf.RpcServiceProtocol.GMItemProto;
import com.icee.myth.server.actor.Human;

/**
 *
 * @author yangyi
 */
public class AddItemRpcHandler implements RpcModifyPlayerHandler {

    public static final AddItemRpcHandler INSTANCE = new AddItemRpcHandler();

    private AddItemRpcHandler() {
    }

    public boolean handle(Human human, GeneratedMessage proto) {
        GMItemProto gMItemProto = (GMItemProto) proto;
        human.items.addItem(gMItemProto.getItemId(), gMItemProto.getItemNum(), human.inGame);
        return true;
    }
}
