/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.server.rpc.handler;

import com.google.protobuf.GeneratedMessage;
import com.icee.myth.protobuf.RpcServiceProtocol.GMSandboxProto;
import com.icee.myth.server.actor.Human;

/**
 *
 * @author liuxianke
 */
public class ChangeSandboxRpcHandler implements RpcModifyPlayerHandler {

    public static final ChangeSandboxRpcHandler INSTANCE = new ChangeSandboxRpcHandler();

    private ChangeSandboxRpcHandler() {
    }

    public boolean handle(Human human, GeneratedMessage proto) {
        GMSandboxProto gmSandboxProto = (GMSandboxProto) proto;
        return human.sandbox.change(gmSandboxProto.getSandbox());
    }
}
