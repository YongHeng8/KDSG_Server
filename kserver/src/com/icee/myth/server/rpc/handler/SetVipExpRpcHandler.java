/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.server.rpc.handler;

import com.google.protobuf.GeneratedMessage;
import com.icee.myth.protobuf.ExternalCommonProtocol.VariableValueProto;
import com.icee.myth.server.actor.Human;

/**
 *
 * @author yangyi
 */
public class SetVipExpRpcHandler implements RpcModifyPlayerHandler {

    public static final SetVipExpRpcHandler INSTANCE = new SetVipExpRpcHandler();

    private SetVipExpRpcHandler() {
    }

    public boolean handle(Human human, GeneratedMessage proto) {
        int num = (int) ((VariableValueProto) proto).getValue();
        int expNum = human.vip.exp;
        int increaseNum = num - expNum;
        if (increaseNum > 0) {
            human.vip.increaseVipExp(increaseNum, human.inGame);
        }
        return true;
    }
}
