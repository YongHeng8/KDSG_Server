/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.server.rpc.handler;

import com.google.protobuf.GeneratedMessage;
import com.icee.myth.protobuf.RpcServiceProtocol.GMItemProto;
import com.icee.myth.server.actor.Human;
import com.icee.myth.utils.IdValue;

/**
 *
 * @author yangyi
 */
public class RemoveItemRpcHandler implements RpcModifyPlayerHandler {

    public static final RemoveItemRpcHandler INSTANCE = new RemoveItemRpcHandler();

    private RemoveItemRpcHandler() {
    }

    public boolean handle(Human human, GeneratedMessage proto) {
        GMItemProto gMItemProto = (GMItemProto) proto;
        human.items.consumeItem(new IdValue(gMItemProto.getItemId(), gMItemProto.getItemNum()), human.inGame);
        return true;
    }
}
