/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.server.rpc.handler;

import com.google.protobuf.GeneratedMessage;
import com.icee.myth.protobuf.ExternalCommonProtocol.VariableValueProto;
import com.icee.myth.server.actor.Human;
import com.icee.myth.utils.Consts;

/**
 *
 * @author yangyi
 */
public class SetSilverRpcHandler implements RpcModifyPlayerHandler {

    public static final SetSilverRpcHandler INSTANCE = new SetSilverRpcHandler();

    private SetSilverRpcHandler() {
    }

    public boolean handle(Human human, GeneratedMessage proto) {
        long num = ((VariableValueProto) proto).getValue();
        long curNum = human.getSilver();
        int increaseNum = (int) (num - curNum);
        if (increaseNum > 0) {
            human.increaseSilver(increaseNum, Consts.SOUL_CHANGE_LOG_TYPE_GM_INCREASE, Consts.SOUL_CHANGE_LOG_SUBTYPE_NONE, human.inGame);
        } else {
            human.decreaseSilver(-increaseNum, Consts.SOUL_CHANGE_LOG_TYPE_GM_DECREASE, Consts.SOUL_CHANGE_LOG_SUBTYPE_NONE, human.inGame);
        }
        return true;
    }
}
