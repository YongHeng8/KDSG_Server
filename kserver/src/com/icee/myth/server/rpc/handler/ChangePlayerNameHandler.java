/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.server.rpc.handler;

import com.google.protobuf.GeneratedMessage;
import com.icee.myth.protobuf.ExternalCommonProtocol.IntStringProto;
import com.icee.myth.server.actor.Human;

/**
 *
 * @author yangyi
 */
public class ChangePlayerNameHandler implements RpcModifyPlayerHandler {

    public static final ChangePlayerNameHandler INSTANCE = new ChangePlayerNameHandler();

    private ChangePlayerNameHandler() {
    }

    public boolean handle(Human human, GeneratedMessage proto) {
        IntStringProto intString = (IntStringProto) proto;
        human.name = intString.getStringValue();
        return true;
    }
}
