/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.base.council;

import com.icee.myth.log.GameLogger;
import com.icee.myth.log.message.FileDebugGameLogMessage;
import com.icee.myth.log.message.builder.GameLogMessageBuilder;
import com.icee.myth.protobuf.ExternalCommonProtocol.IntValuesProto;
import com.icee.myth.protobuf.builder.ClientToMapBuilder;
import com.icee.myth.server.actor.Human;
import com.icee.myth.server.card.Card;
import com.icee.myth.server.levelup.HumanLevelsConfig;
import com.icee.myth.server.slot.LocationInsensitiveCardSlots;
import com.icee.myth.utils.Consts;

/**
 *
 * @author liuxianke
 */
public class Council extends LocationInsensitiveCardSlots {

    public int criAndTenIncrease;    // 队伍暴击能力和韧性增长

    public Council(Human human, IntValuesProto intValuesProto) {
        super(human, Consts.PLACE_TYPE_COUNCIL, intValuesProto);

        calculateCriAndTenIncrease();
    }

    @Override
    public boolean change(IntValuesProto intValuesProto) {
        if (intValuesProto.getValuesCount() <= HumanLevelsConfig.INSTANCE.levelConfigs[human.lv - 1].maxCouncilCardNum) {
            if (super.change(intValuesProto)) {
                calculateCriAndTenIncrease();

                human.sendMessage(ClientToMapBuilder.buildCouncilChange(buildSlotsProto()));
                return true;
            }
        } else {
            GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                    FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                    "Player[" + human.id + "] change place[" + type + "] error because too many cards."));
        }

        return false;
    }

    // 计算暴击能力和韧性增长(每张卡片能提供的增长数值为：0.003*Lv*Wis* leaderpoint)
    private void calculateCriAndTenIncrease() {
        criAndTenIncrease = 0;
        for (Card card : cards) {
            criAndTenIncrease += 0.003 * card.level * card.staticInfo.WIS * card.staticInfo.leadPoint;
        }
    }
}
