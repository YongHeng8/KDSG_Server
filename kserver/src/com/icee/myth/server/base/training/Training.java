/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.base.training;

import com.icee.myth.log.GameLogger;
import com.icee.myth.log.message.FileDebugGameLogMessage;
import com.icee.myth.log.message.builder.GameLogMessageBuilder;
import com.icee.myth.protobuf.ExternalCommonProtocol.IntValuesProto;
import com.icee.myth.protobuf.builder.ClientToMapBuilder;
import com.icee.myth.server.actor.Human;
import com.icee.myth.server.card.Card;
import com.icee.myth.server.levelup.HumanLevelsConfig;
import com.icee.myth.server.slot.LocationInsensitiveCardSlots;
import com.icee.myth.utils.Consts;

/**
 *
 * @author liuxianke
 */
public class Training extends LocationInsensitiveCardSlots {

    public int hitAndDodIncrease;    // 队伍命中能力和闪避增长

    public Training(Human human, IntValuesProto intValuesProto) {
        super(human, Consts.PLACE_TYPE_TRAINING, intValuesProto);

        calculateHitAndDodIncrease();
    }

    @Override
    public boolean change(IntValuesProto intValuesProto) {
        if (intValuesProto.getValuesCount() <= HumanLevelsConfig.INSTANCE.levelConfigs[human.lv - 1].maxTraningCardNum) {
            if (super.change(intValuesProto)) {
                calculateHitAndDodIncrease();

                human.sendMessage(ClientToMapBuilder.buildTrainingChange(buildSlotsProto()));
                return true;
            }
        } else {
            GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                    FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                    "Player[" + human.id + "] change place[" + type + "] error because too many cards."));
        }

        return false;
    }

    // 计算攻击增长(每张卡片能提供的增长数值为：0.003*Lv*Sta*leaderpoint)
    private void calculateHitAndDodIncrease() {
        hitAndDodIncrease = 0;
        for (Card card : cards) {
            hitAndDodIncrease += 0.003 * card.level * card.staticInfo.STA * card.staticInfo.leadPoint;
        }
    }
}
