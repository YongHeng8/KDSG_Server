/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.server.base.mine;

import com.icee.myth.config.MapConfig;
import com.icee.myth.log.GameLogger;
import com.icee.myth.log.message.FileDebugGameLogMessage;
import com.icee.myth.log.message.builder.GameLogMessageBuilder;
import com.icee.myth.protobuf.ExternalCommonProtocol.IntValuesProto;
import com.icee.myth.protobuf.builder.ClientToMapBuilder;
import com.icee.myth.server.actor.Human;
import com.icee.myth.server.card.Card;
import com.icee.myth.server.levelup.HumanLevelsConfig;
import com.icee.myth.server.slot.LocationInsensitiveCardSlots;
import com.icee.myth.utils.Consts;

/**
 *
 * @author liuxianke
 */
public class Mine extends LocationInsensitiveCardSlots {

    public Mine(Human human, IntValuesProto intValuesProto) {
        super(human, Consts.PLACE_TYPE_MINE, intValuesProto);
    }

    public int getProduction() {
        int production = human.lv * MapConfig.INSTANCE.mineBaseRatio;

        for (Card card : cards) {
            production += card.level * card.staticInfo.leadPoint;
        }

        return production;
    }

    @Override
    public boolean change(IntValuesProto intValuesProto) {
        if (intValuesProto.getValuesCount() <= HumanLevelsConfig.INSTANCE.levelConfigs[human.lv - 1].maxMineCardNum) {
            if (super.change(intValuesProto)) {
                human.sendMessage(ClientToMapBuilder.buildMineChange(buildSlotsProto()));
                return true;
            }
        } else {
            GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                    FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                    "Player[" + human.id + "] change place[" + type + "] error because too many cards."));
        }

        return false;
    }

}
