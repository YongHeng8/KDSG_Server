/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.base.barrack;

import com.icee.myth.log.GameLogger;
import com.icee.myth.log.message.FileDebugGameLogMessage;
import com.icee.myth.log.message.builder.GameLogMessageBuilder;
import com.icee.myth.protobuf.ExternalCommonProtocol.IntValuesProto;
import com.icee.myth.protobuf.builder.ClientToMapBuilder;
import com.icee.myth.server.actor.Human;
import com.icee.myth.server.card.Card;
import com.icee.myth.server.levelup.HumanLevelsConfig;
import com.icee.myth.server.slot.LocationInsensitiveCardSlots;
import com.icee.myth.utils.Consts;

/**
 *
 * @author liuxianke
 */
public class Barrack extends LocationInsensitiveCardSlots {

    public float hpIncrease;    // 队伍生命增长

    public Barrack(Human human, IntValuesProto intValuesProto) {
        super(human, Consts.PLACE_TYPE_BARRACK, intValuesProto);

        calculateHpIncrease();
    }

    @Override
    public boolean change(IntValuesProto intValuesProto) {
        if (intValuesProto.getValuesCount() <= HumanLevelsConfig.INSTANCE.levelConfigs[human.lv - 1].maxBarrackCardNum) {
            if (super.change(intValuesProto)) {
                calculateHpIncrease();

                human.sendMessage(ClientToMapBuilder.buildBarrackChange(buildSlotsProto()));
                return true;
            }
        } else {
            GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                    FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                    "Player[" + human.id + "] change place[" + type + "] error because too many cards."));
        }

        return false;
    }

    // 计算生命增长(每张卡片能提供的增长数值为：0.001%*Lv*Sta*leaderpoint)
    private void calculateHpIncrease() {
        hpIncrease = 0;
        for (Card card : cards) {
            hpIncrease += 0.00001 * card.level * card.staticInfo.STA * card.staticInfo.leadPoint;
        }
    }
}
