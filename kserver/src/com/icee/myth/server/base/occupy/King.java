/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.base.occupy;

import com.icee.myth.protobuf.InternalCommonProtocol.DBKingProto;
import com.icee.myth.server.GameServer;

/**
 *
 * @author liuxianke
 */
public class King {
    public final int id;            // 君主玩家id
    public final int production;    // 税收贡献
    public long startTime;          // 臣服时间

    public King(int id, int production) {
        this.id = id;
        this.production = production;
        startTime = GameServer.INSTANCE.getCurrentTime();
    }

    public King(DBKingProto kingProto) {
        id = kingProto.getId();
        production = kingProto.getProduction();
        startTime = kingProto.getStartTime();
    }

    public DBKingProto buildDBKingProto() {
        DBKingProto.Builder builder = DBKingProto.newBuilder();

        builder.setId(id);
        builder.setProduction(production);
        builder.setStartTime(startTime);

        return builder.build();
    }
}
