/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.reward;

import com.icee.myth.protobuf.ExternalCommonProtocol.RewardItemProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.RewardProto;
import com.icee.myth.utils.Consts;
import java.util.List;

/**
 *
 * @author liuxianke
 */
public class CertainRewardInfo {
    public RewardItemInfo[] items;  // 掉落物品
    public int experience;  // 经验获得
    public int energy;  // 体力
    public int silver;  // 蓝魂（金钱）获得
    public int gold;  // 黄魂（元宝）获得

    public CertainRewardInfo() {
        
    }

    public CertainRewardInfo(RewardProto rewardProto) {
        experience = rewardProto.getExperience();
        energy = rewardProto.getEnergy();
        silver = rewardProto.getSilver();
        gold = rewardProto.getGold();

        int itemNum = rewardProto.getItemsCount();
        if (itemNum > 0) {
            items = new RewardItemInfo[itemNum];
            List<RewardItemProto> itemProtos = rewardProto.getItemsList();
            int i=0;
            for (RewardItemProto itemProto : itemProtos) {
                items[i] = new RewardItemInfo(itemProto);
                i++;
            }
        }
    }

    public RewardProto buildRewardProto() {
        RewardProto.Builder builder1 = RewardProto.newBuilder();

        builder1.setExperience(experience);
        builder1.setEnergy(energy);
        builder1.setSilver(silver);
        builder1.setGold(gold);

        if (items != null) {
            for (RewardItemInfo itemInfo : items) {
                RewardItemProto.Builder builder2 = RewardItemProto.newBuilder();
                builder2.setType(itemInfo.itemType);

                switch (itemInfo.itemType) {
                    case Consts.REWARD_ITEM_TYPE_CARD: {
                        builder2.setId(itemInfo.itemId);
                        builder2.setLevel(itemInfo.level);
                        break;
                    }
                    case Consts.REWARD_ITEM_TYPE_ITEM: {
                        builder2.setId(itemInfo.itemId);
                        builder2.setNum(itemInfo.num);
                        break;
                    }
                    case Consts.REWARD_ITEM_TYPE_GOLD:
                    case Consts.REWARD_ITEM_TYPE_SILVER: {
                        builder2.setNum(itemInfo.num);
                        break;
                    }
                }

                builder1.addItems(builder2);
            }
        }

        return builder1.build();
    }
}
