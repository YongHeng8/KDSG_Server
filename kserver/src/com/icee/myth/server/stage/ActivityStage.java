/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.stage;

import com.icee.myth.server.actor.Human;
import com.icee.myth.server.social.BriefPlayerInfo;

/**
 *
 * @author liuxianke
 */
public class ActivityStage extends Stage {
    public final int activityId;
    public final int activityItemId;

    public ActivityStage(Human human, StageStaticInfo staticInfo, BriefPlayerInfo helperInfo, int activityId, int activityItemId) {
        super(human, staticInfo, helperInfo);

        this.activityId = activityId;
        this.activityItemId = activityItemId;
    }
}
