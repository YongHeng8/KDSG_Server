/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.stage;

import com.icee.myth.protobuf.ExternalCommonProtocol.RewardItemProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.RewardProto;
import com.icee.myth.server.reward.RewardItemInfo;
import com.icee.myth.utils.Consts;
import java.util.LinkedList;

/**
 * 宝箱（关卡掉落）
 * @author liuxianke
 */
public class StageBox {
    public LinkedList<RewardItemInfo> items;      // 掉落列表

    public int experience;  // 经验获得（注意：该处经验在产生宝箱的时候自动加给玩家）
    public int silver;      // 白银获得
    public int gold;        // 黄金获得

    public RewardProto buildRewardProto() {
        RewardProto.Builder builder1 = RewardProto.newBuilder();

        if (items != null) {
            for (RewardItemInfo itemInfo : items) {
                RewardItemProto.Builder builder2 = RewardItemProto.newBuilder();
                builder2.setType(itemInfo.itemType);

                switch (itemInfo.itemType) {
                    case Consts.REWARD_ITEM_TYPE_CARD: {
                        builder2.setId(itemInfo.itemId);
                        builder2.setLevel(itemInfo.level);
                        break;
                    }
                    case Consts.REWARD_ITEM_TYPE_ITEM: {
                        builder2.setId(itemInfo.itemId);
                        builder2.setNum(itemInfo.num);
                        break;
                    }
                    case Consts.REWARD_ITEM_TYPE_GOLD:
                    case Consts.REWARD_ITEM_TYPE_SILVER: {
                        builder2.setNum(itemInfo.num);
                        break;
                    }
                }

                builder1.addItems(builder2);
            }
        }

        if (experience > 0) {
            builder1.setExperience(experience);
        }

        if (silver > 0) {
            builder1.setSilver(silver);
        }

        if (gold > 0) {
            builder1.setGold(gold);
        }

        return builder1.build();
    }

    public void addItems(LinkedList<RewardItemInfo> items) {
        if (items != null) {
            if (this.items == null) {
                this.items = new LinkedList<RewardItemInfo>();
            }
            this.items.addAll(items);
        }
    }
}
