/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.slot;

import com.icee.myth.log.GameLogger;
import com.icee.myth.log.message.FileDebugGameLogMessage;
import com.icee.myth.log.message.builder.GameLogMessageBuilder;
import com.icee.myth.protobuf.ExternalCommonProtocol.IntValuesProto;
import com.icee.myth.server.actor.Human;
import com.icee.myth.server.card.Card;
import com.icee.myth.utils.Consts;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author liuxianke
 */
public abstract class LocationInsensitiveCardSlots {
    public final Human human;
    public final int type;  // 位置类型
    public ArrayList<Card> cards;

    public LocationInsensitiveCardSlots(Human human, int type, IntValuesProto intValuesProto) {
        this.human = human;
        this.type = type;

        cards = new ArrayList<Card>();

        if (intValuesProto != null) {
            List<Integer> cardIdList = intValuesProto.getValuesList();

            for (Integer cardId : cardIdList) {
                Card card = human.cards.getCard(cardId);

                if (card != null) {
                    cards.add(card);
                    card.place = type;
                } else {
                    GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                            FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                            "Player[" + human.id + "] place[" + type + "] card[" + cardId + "] not found when init place."));
                }
            }
        }
    }

    public IntValuesProto buildSlotsProto() {
        IntValuesProto.Builder builder1 = IntValuesProto.newBuilder();

        for (Card card : cards) {
            builder1.addValues(card.id);
        }
        
        return builder1.build();
    }

    public boolean change(IntValuesProto intValuesProto) {
        if (intValuesProto.getValuesCount() > 0) {
            ArrayList<Card> newCards = new ArrayList<Card>();
            List<Integer> cardIds = intValuesProto.getValuesList();
            for (Integer cardId : cardIds) {
                Card card = human.cards.getCard(cardId);
                if (card != null)  {
                    if ((card.place == type) || (card.place == Consts.PLACE_TYPE_NONE)) {
                        newCards.add(card);
                    } else {
                        GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                                FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                                "Player[" + human.id + "] change place[" + type + "] error because card[" + cardId + "] in other place."));
                        return false;
                    }
                } else {
                    GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                            FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                            "Player[" + human.id + "] change place[" + type + "] error because card[" + cardId + "] not exist."));
                    return false;
                }
            }

            if (!cards.equals(newCards)) {
                for (Card card : cards) {
                    card.place = Consts.PLACE_TYPE_NONE;
                }
                cards = newCards;
                for (Card card : cards) {
                    card.place = type;
                }
                
                return true;
            } else {
                GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                        FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                        "Player[" + human.id + "] change place[" + type + "] error because no change."));
            }
        } else {
            if (!cards.isEmpty()) {
                for (Card card : cards) {
                    card.place = Consts.PLACE_TYPE_NONE;
                }
                
                cards.clear();
                return true;
            } else {
                GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                        FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                        "Player[" + human.id + "] change place[" + type + "] error because no change."));
            }
        }

        return false;
    }

}
