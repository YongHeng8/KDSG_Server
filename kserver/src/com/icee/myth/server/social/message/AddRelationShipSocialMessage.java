/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.social.message;

/**
 *
 * @author liuxianke
 */
public class AddRelationShipSocialMessage extends SocialMessage {
    public final int followerId;
    public final int concernId;

    public AddRelationShipSocialMessage(int followerId, int concernId) {
        super(SocialMessageType.SMT_ADD_RELATIONSHIP);
        this.followerId = followerId;
        this.concernId = concernId;
    }
}
