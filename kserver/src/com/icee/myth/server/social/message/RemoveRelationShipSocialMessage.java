/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.social.message;

/**
 *
 * @author liuxianke
 */
public class RemoveRelationShipSocialMessage extends SocialMessage {
    public final int followerId;
    public final int concernId;

    public RemoveRelationShipSocialMessage(int followerId, int concernId) {
        super(SocialMessageType.SMT_REMOVE_RELATIONSHIP);
        this.followerId = followerId;
        this.concernId = concernId;
    }
}
