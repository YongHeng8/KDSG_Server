/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.social.message;

/**
 *
 * @author liuxianke
 */
public class GetInfoSocialMessage extends SocialMessage {
    public final int id;

    public GetInfoSocialMessage(int id) {
        super(SocialMessageType.SMT_GET_INFO);
        this.id = id;
    }
}
