/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.social;

import com.icee.myth.protobuf.ExternalCommonProtocol.BriefPlayerProto;

/**
 * 玩家简略信息，在玩家社交关系中用到
 * @author liuxianke
 */
public class BriefPlayerInfo {
    public int id;              // 玩家id
    public String name;         // 玩家名
    public int level;           // 玩家等级
    public int leaderCardId;    // 队长卡片id
    public int leaderCardLevel; // 队长卡片等级

    public boolean inGame;  // 在线标志（也用于判断是否可清理出内存）（玩家上线时设为true，离线时设为false）

    public long lastVisitTime;  // 最近访问时间

    public BriefPlayerInfo(int id, String name, int level, int leaderCardId, int leaderCardLevel){
        this.id = id;
        this.name = name;
        this.level = level;
        this.leaderCardId = leaderCardId;
        this.leaderCardLevel = leaderCardLevel;
    }

    public BriefPlayerProto buildBriefPlayerProto(boolean isFriend, long cooldown) {
        BriefPlayerProto.Builder builder = BriefPlayerProto.newBuilder();

        builder.setId(id);
        builder.setName(name);
        builder.setLevel(level);
        builder.setLeaderCardId(leaderCardId);
        builder.setLeaderCardLevel(leaderCardLevel);
        
        if (isFriend) {
            builder.setIsFriend(isFriend);
            builder.setCooldown(cooldown);
            
            if (inGame) {
                builder.setInGame(inGame);
            }
        }

        return builder.build();
    }
}
