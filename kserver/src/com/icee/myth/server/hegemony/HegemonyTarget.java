/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.hegemony;

import com.icee.myth.server.sandbox.TargetSandbox;
import com.icee.myth.protobuf.ExternalCommonProtocol.HegemonyTargetProto;
import com.icee.myth.protobuf.InternalCommonProtocol.DBHegemonyTargetProto;
import com.icee.myth.server.actor.Human;
import com.icee.myth.server.card.CardFactories;
import com.icee.myth.server.card.CardStaticInfo;
import com.icee.myth.server.sandbox.TargetSandboxCard;
import com.icee.myth.utils.Consts;

/**
 * 争霸目标
 * @author liuxianke
 */
public class HegemonyTarget {
    public final String name; // 目标玩家姓名
    public final int id;      // 目标玩家id（为0时为机器人，否则为玩家id）
    public final int level;   // 目标玩家等级
    public final int rank;    // 目标玩家军衔
    public final float hpIncrease;  // 目标玩家血量增长
    public final float atkIncrease; // 目标玩家攻击增长
    public final int criAndTenIncrease; // 目标玩家暴击韧性增长
    public final int hitAndDodIncrease; // 目标玩家命中闪避增长
    public final TargetSandbox sandbox;// 目标玩家阵型
    public int power;   // 战斗力
    public int status;  // 状态（0.未战 1.胜利 2.失败）

    public HegemonyTarget(String name, int id, int level, int rank, float hpIncrease, float atkIncrease, int criAndTenIncrease, int hitAndDodIncrease, TargetSandbox sandbox) {
        this.name = name;
        this.id = id;
        this.level = level;
        this.rank = rank;
        this.sandbox = sandbox;

        this.hpIncrease = hpIncrease;
        this.atkIncrease = atkIncrease;
        this.criAndTenIncrease = criAndTenIncrease;
        this.hitAndDodIncrease = hitAndDodIncrease;

        for (int i=0; i<Consts.MAX_SANDBOX_SLOT_NUM; i++) {
            TargetSandboxCard targetCard = sandbox.slots[i];
            if (targetCard != null) {
                CardStaticInfo cardStaticInfo = CardFactories.INSTANCE.getCardFactory(targetCard.cardId).staticInfo;
                power += cardStaticInfo.getPower(targetCard.cardLevel, atkIncrease, hpIncrease, hitAndDodIncrease, criAndTenIncrease);
            }
        }

        this.status = Consts.HEGEMONY_TARGET_STATUS_NOTFIGHT;
    }
    
    public HegemonyTarget(Human human) {
        this(human.name, human.id, human.lv, human.rankLv,
             human.getHpUp(),
             human.getAtkUp(),
             human.getCriAndTenUp(),
             human.getHitAndDodUp(),
             new TargetSandbox(human.sandbox));
    }
    
    public HegemonyTarget(DBHegemonyTargetProto hegemonyTargetProto) {
        name = hegemonyTargetProto.getName();
        id = hegemonyTargetProto.getId();
        level = hegemonyTargetProto.getLevel();
        rank = hegemonyTargetProto.getRank();
        hpIncrease = hegemonyTargetProto.getHpIncrease();
        atkIncrease = hegemonyTargetProto.getAtkIncrease();
        criAndTenIncrease = hegemonyTargetProto.getCriAndTenIncrease();
        hitAndDodIncrease = hegemonyTargetProto.getHitAndDodIncrease();
        sandbox = new TargetSandbox(hegemonyTargetProto.getSandbox());
        status = hegemonyTargetProto.getStatus();
        power = hegemonyTargetProto.getPower();
    }

    public HegemonyTargetProto buildHegemonyTargetProto() {
        HegemonyTargetProto.Builder builder = HegemonyTargetProto.newBuilder();
        builder.setName(name);
        builder.setId(id);
        builder.setLevel(level);
        builder.setRank(rank);
        builder.setStatus(status);

        builder.setSandbox(sandbox.buildTargetSandboxProto());

        builder.setPower(power);

        return builder.build();
    }

    public DBHegemonyTargetProto buildDBHegemonyTargetProto() {
        DBHegemonyTargetProto.Builder builder = DBHegemonyTargetProto.newBuilder();
        builder.setName(name);
        builder.setId(id);
        builder.setLevel(level);
        builder.setRank(rank);
        builder.setHpIncrease(hpIncrease);
        builder.setAtkIncrease(atkIncrease);
        builder.setCriAndTenIncrease(criAndTenIncrease);
        builder.setHitAndDodIncrease(hitAndDodIncrease);
        builder.setStatus(status);

        builder.setSandbox(sandbox.buildTargetSandboxProto());

        builder.setPower(power);
        
        return builder.build();
    }
}
