/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.hegemony;

import com.icee.myth.server.sandbox.TargetSandboxCard;

/**
 *
 * @author liuxianke
 */
public class RobotCardsOfRank {
    public TargetSandboxCard[] frontLeaderCards;    // 前排队长卡
    public TargetSandboxCard[] backLeaderCards;     // 后排队长卡
    public TargetSandboxCard[] frontCards;          // 前排卡片
    public TargetSandboxCard[] backCards;           // 后排卡片
}
