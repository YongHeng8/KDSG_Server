/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.quest;

import com.icee.myth.log.GameLogger;
import com.icee.myth.log.message.FileDebugGameLogMessage;
import com.icee.myth.log.message.builder.GameLogMessageBuilder;
import java.util.HashMap;

/**
 *
 * @author liuxianke
 */
public class QuestsConfig {
    public final HashMap<Integer, QuestStaticInfo> questStaticInfos = new HashMap<Integer, QuestStaticInfo>();

    public static final QuestsConfig INSTANCE = new QuestsConfig();

    private QuestsConfig() {
    }

    public void addQuestStaticInfo(QuestStaticInfo questStaticInfo) {
        assert (!questStaticInfos.containsKey(questStaticInfo.id));
        questStaticInfos.put(questStaticInfo.id, questStaticInfo);
    }

    public QuestStaticInfo getQuestStaticInfo(int id) {
        QuestStaticInfo questStaticInfo = questStaticInfos.get(id);
        if (questStaticInfo == null) {
            GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                    FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                    "Quest["+id+"] static info not found."));
        }
        return questStaticInfo;
    }
}
