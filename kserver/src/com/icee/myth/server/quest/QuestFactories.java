///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//
//package com.icee.myth.server.quest;
//
//import com.icee.myth.log.GameLogger;
//import com.icee.myth.log.message.FileDebugGameLogMessage;
//import com.icee.myth.log.message.builder.GameLogMessageBuilder;
//import java.util.HashMap;
//
///**
// *
// * @author liuxianke
// */
//public class QuestFactories {
//    public final HashMap<Integer, QuestFactory> factories = new HashMap<Integer, QuestFactory>();
//
//    public static final QuestFactories INSTANCE = new QuestFactories();
//
//    private QuestFactories() {
//    }
//
//    public void addQuestFactory(QuestFactory questFactory) {
//        assert (!factories.containsKey(questFactory.staticInfo.id));
//        factories.put(questFactory.staticInfo.id, questFactory);
//    }
//
//    public QuestFactory getQuestFactory(int id) {
//        QuestFactory questFactory = factories.get(id);
//        if (questFactory == null) {
//            GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
//                    FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
//                    "Quest["+id+"] info not found."));
//        }
//        return questFactory;
//    }
//}
