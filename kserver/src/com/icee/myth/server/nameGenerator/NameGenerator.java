/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.nameGenerator;

import com.icee.myth.server.GameServer;
import com.icee.myth.utils.Consts;
import com.icee.myth.utils.JSONHelper;
import com.icee.myth.utils.RandomGenerator;

/**
 *
 * @author liuxianke
 */
public class NameGenerator {
    public String[] surname;        // 姓表
    public String[] personalName;   // 名表

    public static final NameGenerator INSTANCE = JSONHelper.parseFileNoException(GameServer.INSTANCE.serverId + "/" + Consts.NAMESCONFIG_FILEPATH, NameGenerator.class);

    private NameGenerator() {
    }

    public String generateName() {
        return surname[RandomGenerator.INSTANCE.generator.nextInt(surname.length)] + personalName[RandomGenerator.INSTANCE.generator.nextInt(personalName.length)];
    }
}
