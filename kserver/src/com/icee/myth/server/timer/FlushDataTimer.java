/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.timer;

import com.icee.myth.server.player.MapPlayer;
import com.icee.myth.utils.Consts;

/**
 *
 * @author liuxianke
 */
public class FlushDataTimer {
    public int actionTime = 0;
    private final MapPlayer player;

    public FlushDataTimer(MapPlayer player) {
        this.player = player;
    }

    public void update(int difftime) {
        actionTime += difftime;
        if (actionTime < Consts.FLUSH_PLAYER_DATA_PERIOD) {
            return;
        }

        player.human.flushData();

        actionTime -= Consts.FLUSH_PLAYER_DATA_PERIOD;
    }

}
