/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.notification.message;

import com.icee.myth.protobuf.ExternalCommonProtocol.BeAddFriendNoteProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.NoteProto;
import com.icee.myth.server.notification.NoteTypeDefine;

/**
 *
 * @author liuxianke
 */
public class BeAddFriendNotificationMessage extends NotificationMessage {
    public final int followerId;
    public final String followerName;
    public final int followerLeaderCardLv;
    public final int followerLeaderCardId;;

    public BeAddFriendNotificationMessage(int followerId, String followerName, int followerLeaderCardId, int followerLeaderCardLv) {
        super(NoteTypeDefine.NOTETYPE_BEADD_FRIEND);
        this.followerId = followerId;
        this.followerName = followerName;
        this.followerLeaderCardLv = followerLeaderCardLv;
        this.followerLeaderCardId = followerLeaderCardId;
    }
    
    @Override
    public NoteProto buildNoteProto() {
        NoteProto.Builder builder1 = NoteProto.newBuilder();
        builder1.setType(type);

        BeAddFriendNoteProto.Builder builder2 = BeAddFriendNoteProto.newBuilder();
        builder2.setId(followerId);
        builder2.setName(followerName);
        builder2.setLeaderCardID(followerLeaderCardId);
        builder2.setLeaderCardLv(followerLeaderCardLv);

        builder1.setData(builder2.build().toByteString());

        return builder1.build();
    }

}
