/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.server.notification.message;

import com.icee.myth.protobuf.ExternalCommonProtocol.IntValueProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.NoteProto;
import com.icee.myth.server.notification.NoteTypeDefine;

/**
 *
 * @author yangyi
 */
public class BillNotificationMessage extends NotificationMessage {

    public final int yellowsoul;

    public BillNotificationMessage(int yellowsoul) {
        super(NoteTypeDefine.NOTETYPE_BILL);
        this.yellowsoul = yellowsoul;
    }

    @Override
    public NoteProto buildNoteProto() {
        NoteProto.Builder builder1 = NoteProto.newBuilder();
        builder1.setType(type);

        IntValueProto.Builder builder2 = IntValueProto.newBuilder();
        builder2.setValue(yellowsoul);

        builder1.setData(builder2.build().toByteString());
        return builder1.build();
    }
}
