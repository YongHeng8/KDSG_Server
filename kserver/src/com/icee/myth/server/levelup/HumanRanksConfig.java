/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.levelup;

import com.icee.myth.server.GameServer;
import com.icee.myth.utils.Consts;
import com.icee.myth.utils.JSONHelper;

/**
 *
 * @author liuxianke
 */
public class HumanRanksConfig {
    public int maxRank;    // 最大等级数
    public HumanRankConfig[] rankConfigs;  // 等级配置

    public static final HumanRanksConfig INSTANCE = JSONHelper.parseFileNoException(GameServer.INSTANCE.serverId + "/" + Consts.HUMANRANKSCONFIG_FILEPATH, HumanRanksConfig.class);

    private HumanRanksConfig() {
    }
}
