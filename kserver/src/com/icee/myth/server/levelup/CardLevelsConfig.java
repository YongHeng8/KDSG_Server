/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.levelup;

import com.icee.myth.server.GameServer;
import com.icee.myth.utils.Consts;
import com.icee.myth.utils.JSONHelper;

/**
 *
 * @author liuxianke
 */
public class CardLevelsConfig {
    public int maxLevel;    // 最大等级数
    public CardLevelConfig[] levelConfigs;  // 等级配置

    public static final CardLevelsConfig INSTANCE = JSONHelper.parseFileNoException(GameServer.INSTANCE.serverId + "/" + Consts.CARDLEVELSCONFIG_FILEPATH, CardLevelsConfig.class);

    private CardLevelsConfig() {
    }
}
