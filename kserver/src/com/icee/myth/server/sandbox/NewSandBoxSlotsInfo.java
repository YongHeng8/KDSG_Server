/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.sandbox;

import com.icee.myth.server.card.Card;
import com.icee.myth.utils.Consts;

/**
 *
 * @author liuxianke
 */
public class NewSandBoxSlotsInfo {
    public final Card[] slots = new Card[Consts.MAX_SANDBOX_SLOT_NUM];
    public int leaderPoint;
}
