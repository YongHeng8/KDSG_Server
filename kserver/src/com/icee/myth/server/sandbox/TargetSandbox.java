/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.server.sandbox;

import com.icee.myth.protobuf.ExternalCommonProtocol.TargetSandboxProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.TargetSandboxSlotProto;
import com.icee.myth.server.card.Card;
import com.icee.myth.utils.Consts;
import java.util.List;

/**
 *
 * @author liuxianke
 */
public class TargetSandbox {
    public final TargetSandboxCard[] slots = new TargetSandboxCard[Consts.MAX_SANDBOX_SLOT_NUM];
    public int leader;  // 队长槽位号

    public TargetSandbox() {
        
    }

    public TargetSandbox(SandBox sandBox) {
        for (int i=0; i<Consts.MAX_SANDBOX_SLOT_NUM; i++) {
            Card card = sandBox.slots[i];
            if (card != null) {
                slots[i] = new TargetSandboxCard(card.staticInfo.id, card.level);
            }
        }

        leader = sandBox.leader;
    }

    public TargetSandbox(TargetSandboxProto targetSandboxProto) {
        List<TargetSandboxSlotProto> targetSandboxSlotProtos = targetSandboxProto.getSlotsList();

        for (TargetSandboxSlotProto targetSandboxSlotProto : targetSandboxSlotProtos) {
            int slotId = targetSandboxSlotProto.getSlotId();
            assert (slots[slotId] == null);
            slots[slotId] = new TargetSandboxCard(targetSandboxSlotProto.getCardId(), targetSandboxSlotProto.getCardlevel());
        }

        leader = targetSandboxProto.getLeader();
    }

    public TargetSandboxProto buildTargetSandboxProto() {
        TargetSandboxProto.Builder builder = TargetSandboxProto.newBuilder();

        for (int i=0; i<Consts.MAX_SANDBOX_SLOT_NUM; i++) {
            if (slots[i] != null) {
                builder.addSlots(slots[i].buildTargetSandboxSlotProto(i));
            }
        }
        builder.setLeader(leader);

        return builder.build();
    }
}
