package com.icee.myth.server;

import com.icee.myth.common.channelContext.ChannelContext;
import com.icee.myth.common.message.networkSendThreadMessage.NetworkSendThreadMessage;
import com.icee.myth.common.message.networkSendThreadMessage.NetworkSendThreadMessage.NetworkSendThreadMessageType;
import com.icee.myth.common.messageQueue.NetworkSendThreadMessageQueue;
import com.icee.myth.log.GameLogger;
import com.icee.myth.log.message.FileDebugGameLogMessage;
import com.icee.myth.log.message.builder.GameLogMessageBuilder;
import com.icee.myth.server.message.networkSendThreadMessage.AddChannelContextNSTMessage;
import com.icee.myth.utils.Consts;
import com.icee.myth.utils.LinkedTransferQueue;
import com.icee.myth.utils.StackTraceUtil;

import java.util.Iterator;
import java.util.LinkedList;

/**
 *
 * @author liuxianke
 */
public class NetworkSendThreadHandle implements Runnable {

    protected final LinkedTransferQueue<NetworkSendThreadMessage> messageQueue = NetworkSendThreadMessageQueue.queue();
    public final LinkedList<ChannelContext> channelContexts = new LinkedList<ChannelContext>();

    private long realPrevTime;
    private long realCurrTime;
    private int prevSleepTime = 0;

    public NetworkSendThreadHandle() {
        
    }

    @Override
    public void run() {
        realPrevTime = System.currentTimeMillis();
        while (true) {
            realCurrTime = System.currentTimeMillis();
            int difftime = (int) (realCurrTime - realPrevTime);

            handleMessages();

            // 统一发送消息
            flushNetData();

            realPrevTime = realCurrTime;
            // diff (D0) include time of previous sleep (d0) + tick time (t0)
            // we want that next d1 + t1 == WORLD_SLEEP_CONST
            // we can't know next t1 and then can use (t0 + d1) == WORLD_SLEEP_CONST requirement
            // d1 = WORLD_SLEEP_CONST - t0 = WORLD_SLEEP_CONST - (D0 - d0) = WORLD_SLEEP_CONST + d0 - D0
            if (difftime <= Consts.GAME_SLEEP_CONST + prevSleepTime) {
                prevSleepTime = Consts.GAME_SLEEP_CONST + prevSleepTime - difftime;
            } else {
                prevSleepTime = 10;
                GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                        FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_DEBUG,
                        "NetworkSendThread: difftime=" + difftime));
            }

            try {
                Thread.sleep(prevSleepTime);
            } catch (InterruptedException ex) {
                GameLogger.getlogger().log(GameLogMessageBuilder.buildFileDebugGameLogMessage(
                        FileDebugGameLogMessage.DebugLogType.DEBUGLOGTYPE_ERROR,
                        StackTraceUtil.getStackTrace(ex)));
            }
        }
    }

    private void handleMessages() {
        // handle message from message queue
        // 不处理shuttingDown过程中产生的消息
        NetworkSendThreadMessage msg = messageQueue.poll();
        while (msg != null) {
            NetworkSendThreadMessageType type = msg.getType();
            switch (msg.getType()) {
                case NSTMT_ADDCHANNELCONTEXT: {
                    channelContexts.add(((AddChannelContextNSTMessage)msg).channelContext);
                    break;
                }
            }
            msg = messageQueue.poll();
        }
    }

    private void flushNetData() {
        for (Iterator<ChannelContext> iter=channelContexts.iterator(); iter.hasNext();) {
            ChannelContext channelContext = iter.next();
            if (channelContext.isActive()) {
                channelContext.flush();
            } else {
                iter.remove();
            }
        }
    }

}
