/*
 * grant permission to some user
 *
 * grant all on myth.* to `root`@`localhost` identified by '';
 */


/*
 * create database myth
 *
 */
create database koudaisanguo default charset utf8 collate utf8_general_ci;

GRANT ALL ON koudaisanguo.* TO 'mythtest'@'127.0.0.%' IDENTIFIED BY 'jkY5qmGKVcRs4nST';
GRANT ALL ON mysql.* TO 'mythtest'@'127.0.0.%' IDENTIFIED BY 'jkY5qmGKVcRs4nST';