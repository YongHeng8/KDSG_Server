/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.admin;

/**
 *
 * @author yangyi
 */
public class JsonUserData {
    public String userName;   // 用户名
    public int privilege;     // 权限

    public JsonUserData(String userName, int privilege) {
        this.userName = userName;
        this.privilege = privilege;
    }
}
