/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.player;

import com.icee.myth.protobuf.ExternalCommonProtocol.CardProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.CardsProto;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author liuxianke
 */
public class Cards {
    public final ArrayList<Card> cards = new ArrayList<Card>();

    public Cards(CardsProto cardsProto) {
        List<CardProto> cardProtos = cardsProto.getCardsList();
        for (CardProto cardProto : cardProtos) {
            cards.add(new Card(cardProto));
        }
    }
}
