/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.player;

import com.icee.myth.protobuf.ExternalCommonProtocol.CardProto;

/**
 *
 * @author liuxianke
 */
public class Card {
    public int instId;
    public int typeId;
    public int level;
    public int experience;

    public Card(CardProto cardProto) {
        instId = cardProto.getInstId();
        typeId = cardProto.getId();
        level = cardProto.getLevel();
        experience = cardProto.getExperience();
    }
}
