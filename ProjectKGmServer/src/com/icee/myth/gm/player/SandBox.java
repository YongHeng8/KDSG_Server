/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.player;

import com.icee.myth.protobuf.ExternalCommonProtocol.SandboxProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.VariableValueProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.VariableValuesProto;
import java.util.List;

/**
 *
 * @author liuxianke
 */
public class SandBox {
    public static final int MAX_SLOT_NUM = 6;
    
    public int[] slots;  // 数组下标对应阵型位置。当对应槽位上有卡片时，槽位值为卡片实例id，否则槽位值为0
    public int leader;  // 队长卡片槽位号
    public int helper;  // 友军卡片槽位号

    public SandBox(int[] slots, int leader, int helper) {
        this.slots = slots;
        this.leader = leader;
        this.helper = helper;
    }

    public SandBox(SandboxProto sandboxProto) {
        VariableValuesProto variableValuesProto = sandboxProto.getSlots();
        List<VariableValueProto> slotList = variableValuesProto.getValuesList();

        slots = new int[MAX_SLOT_NUM];
        for (VariableValueProto slot : slotList) {
            slots[slot.getId()] = (int)slot.getValue();
        }

        leader = sandboxProto.getLeader();
        helper = sandboxProto.getHelper();
    }

    public SandboxProto buildSandboxProto() {
        SandboxProto.Builder builder1 = SandboxProto.newBuilder();

        builder1.setHelper(helper);
        builder1.setLeader(leader);

        VariableValuesProto.Builder builder2 = VariableValuesProto.newBuilder();

        for (int i=0; i<MAX_SLOT_NUM; i++) {
            if (slots[i] != 0) {
                VariableValueProto.Builder builder3 = VariableValueProto.newBuilder();

                builder3.setId(i);
                builder3.setValue(slots[i]);

                builder2.addValues(builder3);
            }
        }

        builder1.setSlots(builder2);

        return builder1.build();
    }
}
