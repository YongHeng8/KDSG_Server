/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.kf;

import com.icee.myth.gm.activity.JSONBroadcastInfo;

/**
 *
 * @author yangyi
 */
public class JsonKFTemplate {
    public int id;
    public int serverId;    // 活动所在服务器
    public JSONBroadcastInfo broadcastInfo;  // 广播信息

    public JsonKFTemplate(int id, int serverId, JSONBroadcastInfo broadcastInfo){
        this.id = id;
        this.serverId = serverId;
        this.broadcastInfo = broadcastInfo;
    }

    public JsonKFTemplate(){
    }

}
