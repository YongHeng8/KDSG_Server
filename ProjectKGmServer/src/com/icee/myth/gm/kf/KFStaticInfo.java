/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.kf;

import com.icee.myth.gm.activity.BroadcastInfo;

/**
 *
 * @author yangyi
 */
public class KFStaticInfo {
    public final int serverId;    // 活动所在服务器
    public final BroadcastInfo broadcastInfo;  // 广播信息

    public KFStaticInfo(JsonKFTemplate template) {
        serverId = template.serverId;

        if (template.broadcastInfo != null) {
            broadcastInfo = new BroadcastInfo(template.broadcastInfo);
            
        } else {
            broadcastInfo = null;
        }
    }
}
