/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.controller;

import java.util.List;

import com.icee.myth.gm.admin.JsonUserData;
import com.icee.myth.gm.channelhandler.HttpServerHandler;
import com.icee.myth.utils.JSONHelper;

/**
 *
 * @author yangyi
 */
public class AdminViewController extends AbstractController{

     public AdminViewController(int privilege){
        super(privilege);
    }

    @Override
    public void process() {
        List<JsonUserData> users =  dbHandler.getAllUser();
        if(users != null){
            String content = "{\"result\":0,\"users\":" + JSONHelper.toJSON(users) + "}";
            HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", content, null);
        } else {
            HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"get add users error\"}", null);
        }
    }
}
