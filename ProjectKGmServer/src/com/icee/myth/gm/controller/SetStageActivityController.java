/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.controller;

import com.icee.myth.gm.GmServer;
import com.icee.myth.gm.channelhandler.HttpServerHandler;
import com.icee.myth.gm.client.GameServerClient;
import com.icee.myth.utils.Consts;
import com.icee.myth.utils.JSONHelper;
import com.icee.myth.utils.LogConsts;
import com.icee.myth.utils.MLogger;
import java.util.ArrayList;

/**
 *
 * @author liuxianke
 */
public class SetStageActivityController extends AbstractController {

    public SetStageActivityController(int privilege) {
        super(privilege);
    }

    @Override
    public void process() {
        Long serverId = (Long) jobj.get("server");
        String data = (String)jobj.get("data");

        if (serverId != null && data != null) {
            if (serverId >= 0) {
                GameServerClient gameServerClient = GmServer.INSTANCE.getServer(serverId.intValue());
                if (gameServerClient != null) {
                    if (gameServerClient.setStageActivity(data)) {
                        dbHandler.insertGmOperateLog(userData.userName, Consts.GMOPERATELOG_SET_STAGE_ACTIVITY, jobj.toJSONString());
                        HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":0}", null);
                    } else {
                        HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"set stage activity fail\"}", null);
                    }
                } else {
                    HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"server not found\"}", null);
                }
            } else {
                ArrayList<Integer> errServerList = new ArrayList<Integer>();

                for(GameServerClient gameServerClient : GmServer.INSTANCE.getGameServerClients().values()){
                    if (gameServerClient.setStageActivity(data)) {
                        MLogger.getlogger().debuglog(LogConsts.LOGLEVEL_DEBUG, "Set stage activity to server["+gameServerClient.serverConfig.id+"]");
                    } else {
                        errServerList.add(gameServerClient.getId());
                        MLogger.getlogger().debuglog(LogConsts.LOGLEVEL_ERROR, "Fail to set stage activity to server["+gameServerClient.serverConfig.id+"]");
                    }
                }
                dbHandler.insertGmOperateLog(userData.userName, Consts.GMOPERATELOG_SET_STAGE_ACTIVITY, jobj.toJSONString());
                HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":0,\"servers\":" + JSONHelper.toJSON(errServerList) + "}", null);
            }
        } else {
            HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"param error\"}", null);
        }
    }
}
