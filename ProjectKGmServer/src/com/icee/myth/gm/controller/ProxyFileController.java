///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.icee.myth.gm.controller;
//
//import com.icee.myth.gm.channelhandler.HttpServerHandler;
//import com.icee.myth.utils.LogConsts;
//import com.icee.myth.utils.MLogger;
//import com.icee.myth.utils.StackTraceUtil;
//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.InputStreamReader;
//import java.net.HttpURLConnection;
//import java.net.URL;
//
///**
// *
// * @author yangyi
// */
//public class ProxyFileController extends AbstractController {
//
//    public ProxyFileController(int privilege) {
//        super(privilege);
//    }
//
//    @Override
//    public void process() {
//        String url = (String) jobj.get("url");
//        String data = (String) jobj.get("data");
//        if (url != null) {
//            HttpURLConnection con = null;
//            BufferedReader reader = null;
//            try {
//                con = (HttpURLConnection) new URL(url).openConnection();
//                con.setRequestMethod("POST");
//                con.setDoOutput(true);
//                if(data != null){
//                    con.getOutputStream().write(data.getBytes("UTF-8"));
//                    con.getOutputStream().flush();
//                }
//                con.getOutputStream().close();
//                //获取结果
//                reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
//                StringBuffer contents = new StringBuffer();
//                String text = null;
//                while ((text = reader.readLine()) != null) {
//                    contents.append(text);
//                    contents.append(System.getProperty("line.separator"));
//                }
//                HttpServerHandler.writeResponse(request, ctx.getChannel(), "text/html; charset=UTF-8", contents.toString(), null);
//
//            } catch (IOException ex) {
//                MLogger.getlogger().debuglog(LogConsts.LOGLEVEL_ERROR, "request crossdomain file failed url:" + url);
//            } finally {
//                if (reader != null) {
//                    try {
//                        reader.close();
//                    } catch (IOException ex) {
//                        MLogger.getlogger().debuglog(LogConsts.LOGLEVEL_ERROR, StackTraceUtil.getStackTrace(ex));
//                    }
//                }
//                if (con != null) {
//                    con.disconnect();
//                }
//            }
//        }
//    }
//}
