/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.gm.controller;

import com.icee.myth.gm.client.GameServerClient;
import com.icee.myth.utils.Consts;

/**
 *
 * @author liuxianke
 */
public class SetSilverController extends RpcSetController {

    public SetSilverController(int privilege) {
        super(privilege);
    }

    @Override
    public boolean rpcSet(GameServerClient gameServerClient, int cid, int num) {
        if (gameServerClient.rpcSet(Consts.RPCSET_SILVER, cid, num)) {
            dbHandler.insertGmOperateLog(userData.userName, Consts.GMOPERATELOG_SETSILVER, jobj.toJSONString());
            return true;
        }
        return false;
    }
}
