/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.controller;

import com.icee.myth.gm.admin.UserData;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.handler.codec.http.HttpRequest;
import org.json.simple.JSONObject;
import com.icee.myth.gm.channelhandler.HttpServerHandler;
import com.icee.myth.gm.db.DbHandler;
import com.icee.myth.utils.Consts;
/**
 *
 * @author yangyi
 */
public abstract class AbstractController {
    public ChannelHandlerContext ctx;
    public HttpRequest request;
    public MessageEvent e;
    public JSONObject jobj;
    public UserData userData;
    public int privilege;
    public DbHandler dbHandler;
    
    public void init(ChannelHandlerContext ctx, HttpRequest request, MessageEvent e, JSONObject jobj, UserData userData, DbHandler dbHandler){
        this.ctx = ctx;
        this.request = request;
        this.e = e;
        this.jobj = jobj;
        this.userData = userData;
        this.dbHandler = dbHandler;
    }

    public AbstractController(int privilege){
        this.privilege = privilege;
    }
    
    public abstract void process();

    public boolean checkPriviledge(){
         if (privilege == Consts.PRIVILEGE_NONE || (userData.privilege & privilege) > 0) {
             return true;
         }
         return false;
    }

    public void index(){
        if(!checkPriviledge()){
            HttpServerHandler.sendErrorNoPrivilege(request, e);
            return;
        }
        process();
    }
}
