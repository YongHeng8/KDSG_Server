/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.controller;

import com.icee.myth.gm.GmServer;
import com.icee.myth.gm.channelhandler.HttpServerHandler;
import com.icee.myth.gm.client.GameServerClient;
import com.icee.myth.utils.Consts;

/**
 *
 * @author liuxianke
 */
public class AddCardController extends AbstractController{

     public AddCardController(int privilege){
        super(privilege);
    }

    @Override
    public void process() {
        Long serverId = (Long) jobj.get("server");
        Long cid = (Long)jobj.get("cid");
        Long cardTypeId = (Long)jobj.get("cardtypeid");
        Long cardLevel = (Long)jobj.get("cardlevel");
        GameServerClient gameServerClient = null;
        if (serverId != null) {
            gameServerClient = GmServer.INSTANCE.getServer(serverId.intValue());
        }
        if(gameServerClient != null && cid != null && cardTypeId != null && cardLevel != null){
            if(gameServerClient.addCard((int)(long)cid,(int)(long)cardTypeId,(int)(long)cardLevel)){
                dbHandler.insertGmOperateLog(userData.userName,Consts.GMOPERATELOG_ADDCARD, jobj.toJSONString());
                HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":0}", null);
            } else{
                HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"add card error\"}", null);
            }
        } else{
            HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"param error\"}", null);
        }
    }

}
