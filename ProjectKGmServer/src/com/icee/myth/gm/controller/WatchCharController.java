/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.controller;

import com.icee.myth.gm.GmServer;
import com.icee.myth.gm.channelhandler.HttpServerHandler;
import com.icee.myth.gm.client.GameServerClient;
import com.icee.myth.gm.db.DbHandler;
import com.icee.myth.gm.player.CharDetailInfo;
import com.icee.myth.gm.player.Human;
import com.icee.myth.utils.JSONHelper;

/**
 *
 * @author yangyi
 */
public class WatchCharController extends AbstractController{

    public WatchCharController(int privilege) {
        super(privilege);
    }

    @Override
    public void process() {
        Long serverId = (Long) jobj.get("serverid");
        GameServerClient gameServerClient = null;
        if (serverId != null) {
            gameServerClient = GmServer.INSTANCE.getServer(serverId.intValue());
        }
        String passport = (String)jobj.get("passport");
        if(passport != null && gameServerClient != null){
            DbHandler managerDbHandler = new DbHandler(GmServer.INSTANCE.managerDbHost, GmServer.INSTANCE.managerDbName);
            int cid = managerDbHandler.getCidByPassport(passport);
            managerDbHandler.close();
            if(cid >= 0){
                //success
                DbHandler gameDbHandler = new DbHandler(gameServerClient.serverConfig.dbHost, gameServerClient.serverConfig.dbName);
                CharDetailInfo charDetailInfo = gameDbHandler.getUserDetailInfo(cid);
                gameDbHandler.close();
                if (charDetailInfo != null) {
                    Human charInfo = new Human(charDetailInfo);
                    HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":0,\"data\":" + JSONHelper.toJSON(charInfo) + "}", null);
                } else {
                    HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"get detail char info from db error\"}", null);
                }
            } else {
                HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"get cid error\"}", null);
            }
        } else {
            HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"param error\"}", null);
        }
    }

}
