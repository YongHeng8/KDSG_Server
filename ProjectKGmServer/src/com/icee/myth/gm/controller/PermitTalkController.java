/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.controller;

import com.icee.myth.gm.GmServer;
import com.icee.myth.gm.channelhandler.HttpServerHandler;
import com.icee.myth.gm.client.GameServerClient;
import com.icee.myth.gm.db.DbHandler;
import com.icee.myth.utils.Consts;

/**
 *
 * @author yangyi
 */
public class PermitTalkController extends AbstractController{

    public PermitTalkController(int privilege) {
        super(privilege);
    }

    public boolean  permitTalk(String passport) {
        DbHandler managerDbHandler = new DbHandler(GmServer.INSTANCE.managerDbHost, GmServer.INSTANCE.managerDbName);
        if(managerDbHandler.updateForbinTalkTime(passport, 0)) {
            int cid = managerDbHandler.getCidByPassport(passport);
            managerDbHandler.close();
            for (GameServerClient server : GmServer.INSTANCE.getGameServerClients().values()) {
                server.forbidTalk(cid, 0);
            }
            return true;
        }
        managerDbHandler.close();
        return false;
    }

    @Override
    public void process() {
        String passport = (String)jobj.get("passport");
        if(passport != null){
            if(permitTalk(passport)){
                dbHandler.insertGmOperateLog(userData.userName, Consts.GMOPERATELOG_PERMITTALK, jobj.toJSONString());
                HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":0}", null);
            } else{
                HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"permit talk error\"}", null);
            }
        } else{
            HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"param error\"}", null);
        }
    }

}
