/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.controller;

import com.icee.myth.gm.GmServer;
import com.icee.myth.gm.channelhandler.HttpServerHandler;


/**
 *
 * @author yangyi
 */
public class LogoutController extends AbstractController{

    public LogoutController(int privilege){
        super(privilege);
    }
    
    @Override
    public void process() {
        GmServer.INSTANCE.removeUserDate(userData.userName);
        HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":0}", null);
    }

}
