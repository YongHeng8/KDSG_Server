/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.controller;

import com.icee.myth.gm.GmServer;
import com.icee.myth.gm.channelhandler.HttpServerHandler;
import com.icee.myth.gm.client.GameServerClient;

/**
 *
 * @author yangyi
 */
public abstract class RpcSetController extends AbstractController{

    public RpcSetController(int privilege) {
        super(privilege);
    }
    
    public abstract  boolean rpcSet(GameServerClient gameServerClient, int cid, int num);
    
    @Override
    public void process() {
        Long serverId = (Long) jobj.get("server");
        Long cid = (Long)jobj.get("cid");
        Long num = (Long)jobj.get("num");        
        GameServerClient gameServerClient = null;
        if (serverId != null) {
            gameServerClient = GmServer.INSTANCE.getServer(serverId.intValue());
        }
        if(gameServerClient != null && cid != null && num != null){
            if(rpcSet(gameServerClient, (int)(long)cid,(int)(long)num)){
                HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":0}", null);
            } else{
                HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"rpc set error\"}", null);
            }
        } else{
            HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"param error\"}", null);
        }
    }

}
