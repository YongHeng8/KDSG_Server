/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.gm.controller;

import com.icee.myth.gm.GmServer;
import com.icee.myth.gm.activity.JSONNormalActivityTemplates;
import com.icee.myth.gm.activity.NormalActivityTemplate;
import com.icee.myth.gm.channelhandler.HttpServerHandler;
import com.icee.myth.gm.client.GameServerClient;
import com.icee.myth.utils.Consts;
import com.icee.myth.utils.Escape;
import com.icee.myth.utils.JSONHelper;
import java.util.ArrayList;
import org.json.simple.JSONObject;

/**
 *
 * @author yangyi
 */
public class AddNormalActivityController extends AbstractController {

    public AddNormalActivityController(int privilege) {
        super(privilege);
    }

    @Override
    public void process() {
        Long serverId = (Long) jobj.get("server");
        JSONObject data = (JSONObject)jobj.get("data");
        
        if (serverId != null && data != null) {
            JSONNormalActivityTemplates templates = JSONHelper.parseString(Escape.unescape(data.toString()), JSONNormalActivityTemplates.class) ;

            if (serverId >= 0) {
                GameServerClient gameServerClient = GmServer.INSTANCE.getServer(serverId.intValue());
                if (gameServerClient != null) {
                    String content = "{\"result\":0,\"data\":[";
                    ArrayList<Integer> failedActivities = new ArrayList<Integer>();
                    content += "{\"server\":" + serverId + ",\"failedactivities\":";
                    for (NormalActivityTemplate template : templates.normalActivityTemplates) {
                        boolean ret = gameServerClient.addNormalActivity(JSONHelper.toJSON(template));
                        if (!ret) {
                            failedActivities.add(template.staticInfo.id);
                        }
                    }
                    content += JSONHelper.toJSON(failedActivities) + "}]}";
                    
                    dbHandler.insertGmOperateLog(userData.userName, Consts.GMOPERATELOG_ADDNORMALACTIVITY, jobj.toJSONString());
                    HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", content, null);
                } else {
                    HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"server not found\"}", null);
                }
            } else {
                String content = "{\"result\":0,\"data\":[";
                for (GameServerClient client : GmServer.INSTANCE.getGameServerClients().values()) {
                    content += "{\"server\":" + client.serverConfig.id + ",\"failedactivities\":";
                    ArrayList<Integer> failedActivities = new ArrayList<Integer>();
                    for (NormalActivityTemplate template : templates.normalActivityTemplates) {
                        boolean ret = client.addNormalActivity(JSONHelper.toJSON(template));
                        if (!ret) {
                            failedActivities.add(template.staticInfo.id);
                        }
                    }
                    content += JSONHelper.toJSON(failedActivities) + "},";
                }
                content = content.substring(0,content.length()-1);
                content += "]}";
                dbHandler.insertGmOperateLog(userData.userName, Consts.GMOPERATELOG_ADDNORMALACTIVITY, jobj.toJSONString());
                HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", content, null);
            }
        } else {
            HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"param error\"}", null);
        }
        
    }
}
