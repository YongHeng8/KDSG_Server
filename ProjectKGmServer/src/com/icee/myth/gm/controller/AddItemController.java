/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.controller;

import com.icee.myth.gm.GmServer;
import com.icee.myth.gm.channelhandler.HttpServerHandler;
import com.icee.myth.gm.client.GameServerClient;
import com.icee.myth.utils.Consts;

/**
 *
 * @author liuxianke
 */
public class AddItemController extends AbstractController{

     public AddItemController(int privilege){
        super(privilege);
    }

    @Override
    public void process() {
        Long serverId = (Long) jobj.get("server");
        Long cid = (Long)jobj.get("cid");
        Long itemId = (Long)jobj.get("itemid");
        Long itemNum = (Long)jobj.get("itemnum");
        GameServerClient gameServerClient = null;
        if (serverId != null) {
            gameServerClient = GmServer.INSTANCE.getServer(serverId.intValue());
        }
        if(gameServerClient != null && cid != null && itemId != null && itemNum != null){
            if(gameServerClient.addItem((int)(long)cid,(int)(long)itemId,(int)(long)itemNum)){
                dbHandler.insertGmOperateLog(userData.userName,Consts.GMOPERATELOG_ADDITEM, jobj.toJSONString());
                HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":0}", null);
            } else{
                HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"add item error\"}", null);
            }
        } else{
            HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"param error\"}", null);
        }
    }
}
