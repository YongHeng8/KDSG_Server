/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.controller;

import com.icee.myth.gm.GmServer;
import com.icee.myth.gm.api.PlatformUidTransform;
import com.icee.myth.gm.channelhandler.HttpServerHandler;
import com.icee.myth.gm.client.GameServerClient;
import com.icee.myth.gm.db.DbHandler;
import com.icee.myth.gm.player.PlayerInfo;
import com.icee.myth.utils.Escape;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author yangyi
 */
public class GetHasLoginServerByGameNameController extends AbstractController{

    public GetHasLoginServerByGameNameController(int privilege){
        super(privilege);
    }

    @Override
    public void process() {
        Long serverId = (Long) jobj.get("server");
        GameServerClient gameServerClient = null;
        if (serverId != null) {
            gameServerClient = GmServer.INSTANCE.getServer(serverId.intValue());
        }
        String playerName = (String)jobj.get("gameName");
        playerName = Escape.unescape(playerName);
        if (gameServerClient != null && playerName != null) {
            DbHandler gameDbHandler = new DbHandler(gameServerClient.serverConfig.dbHost, gameServerClient.serverConfig.dbName);
            int cid = gameDbHandler.getCidByPlayerName(playerName);
            gameDbHandler.close();
            if(cid >= 0){ //success
                DbHandler managerDbHandler = new DbHandler(GmServer.INSTANCE.managerDbHost, GmServer.INSTANCE.managerDbName);
                String passport = managerDbHandler.getPassportByCid(cid);
                if (passport != null) {
                    List<Integer> servers = managerDbHandler.getServersHasLogin(passport);
                    PlayerInfo playerInfo = managerDbHandler.getPlayerInfoFromDB(passport);
                    if(servers != null && playerInfo != null){
                        String puid = PlatformUidTransform.passportToPuid(playerInfo.thirdPartyUserId);
                        String content = "{\"result\":0,\"puid\":\"" + puid + "\",\"passport\":\"" + passport + "\",\"cid\":" + cid + ",\"servers\":[";
                        boolean firstserver = true;
                        for (Iterator<Integer> it = servers.iterator(); it.hasNext();) {
                            Integer id = it.next();
                            GameServerClient tmpGameServerClient = GmServer.INSTANCE.getServer(id);
                            if(tmpGameServerClient != null){
                                if(!firstserver){
                                    content += "," ;
                                }
                                firstserver = false;
                                content += "{\"id\":" + tmpGameServerClient.getId() + ",\"name\":\"" + tmpGameServerClient.getName() + "\"}";
                            }
                        }
                        content += "],\"forbidtalkendtime\":" + playerInfo.forbiddenEndTalkTime + ",\"forbidloginendtime\":" + playerInfo.forbiddenEndLoginTime + "}";
                        HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", content, null);
                    } else if (servers == null) {
                        HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"get servers list from db error\"}", null);
                    } else {
                        HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"can not find player info\"}", null);
                    }
                } else {
                    HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"can not get passport by cid\"}", null);
                }
                managerDbHandler.close();
            } else {
                HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"can not get cid by player name\"}", null);
            }
        } else{
            HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"param error\"}", null);
        }
    }

}
