/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.gm.controller;

import com.icee.myth.gm.admin.OperateLogs;
import com.icee.myth.gm.channelhandler.HttpServerHandler;
import com.icee.myth.utils.Consts;
import com.icee.myth.utils.JSONHelper;

/**
 *
 * @author yangyi
 */
public class OperateLogViewController extends AbstractController {

    OperateLogViewController(int privilege) {
        super(privilege);
    }

    @Override
    public void process() {
        Long start = (Long) jobj.get("start");
        Long num = (Long) jobj.get("num");
        String userName = (String) jobj.get("user");
        if (userName != null) {
            if ((userData.privilege & Consts.PRIVILEGE_VIREOTHEROPERATELOG) == 0) {
                HttpServerHandler.sendErrorNoPrivilege(request, e);
                return;
            }
        } else {
            userName = userData.userName;
        }
        if (start != null && num != null) {
            OperateLogs logs = dbHandler.getOperateLog(userName, (int) (long) start, (int) (long) num);
            if (logs != null) {
                HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":0,\"logs\":" + JSONHelper.toJSON(logs.logs) + "}", null);
            } else {
                HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"get log from db error\"}}", null);
            }
        } else {
            HttpServerHandler.writeResponse(request, e.getChannel(), "text/html; charset=UTF-8", "{\"result\":-1,\"error\":\"param error\"}", null);
        }
    }
}
