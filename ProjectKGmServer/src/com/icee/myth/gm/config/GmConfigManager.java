/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.config;

import com.google.gson.JsonSyntaxException;
import com.icee.myth.gm.GmServer;
import com.icee.myth.gm.activity.JSONActivityTemplates;
import com.icee.myth.gm.kf.JsonKFTemplates;
import com.icee.myth.utils.JSONHelper;
import com.icee.myth.utils.LogConsts;
import com.icee.myth.utils.MLogger;
import com.icee.myth.utils.StackTraceUtil;
import java.io.IOException;

/**
 *
 * @author yangyi
 */
public class GmConfigManager {

    public static void loadActivityConfig(String filename, GmServer gmServer) {
        try {
            JSONActivityTemplates activityTemplates = JSONHelper.parseFile(filename, JSONActivityTemplates.class);

            gmServer.initActivities(activityTemplates);
        } catch (JsonSyntaxException jse) {
            MLogger.getlogger().log(LogConsts.LOGTYPE_GAMEERR, StackTraceUtil.getStackTrace(jse));
        } catch (IOException ioe) {
            MLogger.getlogger().log(LogConsts.LOGTYPE_GAMEERR, StackTraceUtil.getStackTrace(ioe));
        }
    }

    public static void loadKFConfig(String filename, GmServer gmServer) {
        try {
            JsonKFTemplates kfTemplates = JSONHelper.parseFile(filename, JsonKFTemplates.class);

            gmServer.initKFConfig(kfTemplates);
        } catch (JsonSyntaxException jse) {
            MLogger.getlogger().log(LogConsts.LOGTYPE_GAMEERR, StackTraceUtil.getStackTrace(jse));
        } catch (IOException ioe) {
            MLogger.getlogger().log(LogConsts.LOGTYPE_GAMEERR, StackTraceUtil.getStackTrace(ioe));
        }
    }
    
}
