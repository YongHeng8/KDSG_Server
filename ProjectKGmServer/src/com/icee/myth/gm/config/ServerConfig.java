/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.config;

/**
 *
 * @author yangyi
 */
public class ServerConfig {
    public int id;  // 服id
    public String regionid;    // 服域名
    public String name;  // 服名称
    public String dbHost;   // 数据库地址
    public String dbName;   // 数据库名
    public String logdbhost;        // 日志数据库地址
    public String logDBNamePrefix;  // 日志数据库名前缀
    public String host;     // 部署服务器的机器内网IP
    public String externalHost; // 对外提供服务的IP
    public int externalPort;    // 对外提供服务的端口
    public int managerPort; // 让manager连接的端口
    public int rpcPort; // rpc连接端口
}
