/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.activity;

/**
 *
 * @author yangyi
 */
public class CertainRewardInfo {
    public RewardItemInfo[] items;  // 掉落物品
    public int experience;  // 经验获得
    public int energy;  // 体力
    public int silver;  // 蓝魂（金钱）获得
    public int gold;  // 黄魂（元宝）获得

}
