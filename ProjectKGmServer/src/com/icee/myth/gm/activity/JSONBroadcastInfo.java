/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.gm.activity;

/**
 *
 * @author liuxianke
 */
public class JSONBroadcastInfo {
    public String startTime;      // 开始时间，格式（yyyy-MM-dd HH:mm:ss）
    public String endTime;        // 结束时间，格式（yyyy-MM-dd HH:mm:ss）
    public int period;            // 周期（单位秒）
    public String message;        // 通告消息

    public JSONBroadcastInfo(String startTime,String endTime,int period,String message){
        this.startTime = startTime;
        this.endTime = endTime;
        this.period = period;
        this.message = message;
    }

    public JSONBroadcastInfo(){
    }
}
