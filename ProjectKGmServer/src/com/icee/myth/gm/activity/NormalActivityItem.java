/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.gm.activity;

import com.icee.myth.utils.IdValue;

/**
 *
 * @author liuxianke
 */
public class NormalActivityItem {

    public int id;          // 活动项id（所属活动中的下标）
    public int type;        // 活动项类型（0关卡 1充值 2消费 3兑换）
    public String title;    // 活动项标题
    public String icon;     // 活动项图标
    public CertainRewardInfo reward;    // 奖励（对关卡活动无效）
    public int value1;      // 值1（关卡活动为次数；充值活动为充值数）
    public int value2;      // 值2（关卡活动为关卡号）
    public IdValue[] exchangeItems; // 兑换物品列表（兑换活动用）
    public boolean isDaily; // 是否每日任务
}
