/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.utils;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/**
 *
 * @author liuxianke
 */
public class HttpHelper {

     public static String sanitizeUri(String uri) {
         // Decode the path.
         try {
             uri = URLDecoder.decode(uri, "UTF-8");
         } catch (UnsupportedEncodingException e) {
             try {
                 uri = URLDecoder.decode(uri, "ISO-8859-1");
             } catch (UnsupportedEncodingException e1) {
                 throw new Error();
             }
         }

         // Convert file separators.
         uri = uri.replace('/', File.separatorChar);

         // Simplistic dumb security check.
         // You will have to do something serious in the production environment.
         if (uri.contains(File.separator + ".") ||
             uri.contains("." + File.separator) ||
             uri.startsWith(".") || uri.endsWith(".")) {
             return null;
         }

         // Convert to absolute path.
         return System.getProperty("user.dir") + File.separator + uri;
     }
}
