CREATE DATABASE  `gmserver` default charset utf8 collate utf8_general_ci;

GRANT ALL ON gmserver.* TO 'mythtest'@'127.0.0.%' IDENTIFIED BY 'jkY5qmGKVcRs4nST';

USE `gmserver`;

/*Table structure for table `log` */
DROP TABLE IF EXISTS `log`;
CREATE TABLE `log` (
  `time` datetime DEFAULT NULL,
  `user` varchar(32) DEFAULT NULL,
  `operate` int(11) DEFAULT NULL,
  `content` mediumtext DEFAULT NULL,
  KEY `userIdx` (`user`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `trustedbrowserips` */
DROP TABLE IF EXISTS `trustedbrowserips`;
CREATE TABLE `trustedbrowserips` (
  `ip` varchar(20) NOT NULL,
  PRIMARY KEY (`ip`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `user` */
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `name` varchar(10) NOT NULL,
  `passwd` varchar(20) NOT NULL,
  `privilege` int(11) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* Procedure structure for procedure `get_log` */

DROP PROCEDURE IF EXISTS  `get_log` ;
DELIMITER $$
CREATE  PROCEDURE `get_log`(
	pusername varchar(32),
	pstart	INT(11),
	pnum INT(11)
    )
BEGIN
	SELECT *
	FROM `log` where `user` = `pusername` order by `time` desc ;
	set @START = pstart;
	set @LIMIT = pnum;
    END $$
DELIMITER ;

/* Procedure structure for procedure `get_user` */

DROP PROCEDURE IF EXISTS  `get_user` ;
DELIMITER $$
CREATE  PROCEDURE `get_user`(
	pname	varchar(10)
    )
BEGIN
	SELECT passwd, privilege
	FROM `user`
	WHERE `name`=pname;
    END $$
DELIMITER ;

/* Procedure structure for procedure `insert_operatelog` */

DROP PROCEDURE IF EXISTS  `insert_operatelog` ;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_operatelog`(
	puser varchar(32),
	poperate int,
	pcontent mediumtext
    )
BEGIN
	INSERT INTO `log` VALUES(now(),`puser`,`poperate`,`pcontent`);
    END $$
DELIMITER ;