/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.protobuf.builder;

import com.icee.myth.deamon.protobufMessage.ProtobufMessage;
import com.icee.myth.deamon.protobufMessage.ProtobufMessageType;

/**
 *
 * @author liuxianke
 */
public class DeamonToManagerBuilder {

    public static ProtobufMessage buildDeamonHeartbeat() {
        return new ProtobufMessage(ProtobufMessageType.DEAMON2MANAGER_HEARTBEAT,null);
    }

}
