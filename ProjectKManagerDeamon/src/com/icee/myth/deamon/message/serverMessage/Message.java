/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.deamon.message.serverMessage;

/**
 *
 * @author liuxianke
 */
public interface Message {
    /**
     * 消息类型                         -- 消息说明
     * MAPSCENE_XXX                    -- XXX
     *
     */
    public enum MessageType {
        ALL_HEARTBEAT,
        DEAMON_MANAGERHEARTBEAT,
        DEAMON_MANAGERCONNECT,
        DEAMON_MANAGERCLOSE,
        DEAMON_MANAGERDOWN,
        DEAMON_STARTSERVER,
        DEAMON_SERVERCONFIGDATA
    }

    public MessageType getType();
}
