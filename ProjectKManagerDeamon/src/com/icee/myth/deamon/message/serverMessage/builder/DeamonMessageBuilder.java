/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.deamon.message.serverMessage.builder;

import com.google.protobuf.ByteString;
import com.icee.myth.deamon.message.serverMessage.InternalChannelMessage;
import com.icee.myth.deamon.message.serverMessage.Message;
import com.icee.myth.deamon.message.serverMessage.Message.MessageType;
import com.icee.myth.deamon.message.serverMessage.ServerConfigDataDeamonMessage;
import com.icee.myth.deamon.message.serverMessage.SimpleMessage;
import com.icee.myth.deamon.message.serverMessage.StartServerDeamonMessage;
import org.jboss.netty.channel.Channel;

/**
 *
 * @author liuxianke
 */
public class DeamonMessageBuilder {

    public static Message buildHeartbeatMessage() {
        return new SimpleMessage(MessageType.ALL_HEARTBEAT);
    }

    public static Message buildManagerHeartbeatMessage() {
        return new SimpleMessage(MessageType.DEAMON_MANAGERHEARTBEAT);
    }

    public static Message buildManagerConnectMessage(Channel channel) {
        return new InternalChannelMessage(MessageType.DEAMON_MANAGERCONNECT, channel);
    }

    public static Message buildManagerCloseMessage(Channel channel) {
        return new InternalChannelMessage(MessageType.DEAMON_MANAGERCLOSE, channel);
    }

    public static Message buildManagerDownMessage() {
        return new SimpleMessage(MessageType.DEAMON_MANAGERDOWN);
    }

    public static Message buildStartServerMessage(String vmOptionStr, String paramStr) {
        return new StartServerDeamonMessage(vmOptionStr, paramStr);
    }

    public static Message buildServerConfigDataMessage(int serverId, ByteString data) {
        return new ServerConfigDataDeamonMessage(serverId, data);
    }
}
