/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.deamon.message.serverMessage;

import com.google.protobuf.ByteString;

/**
 *
 * @author liuxianke
 */
public class ServerConfigDataDeamonMessage extends SimpleMessage {
    public final int serverId;
    public final ByteString data;

    public ServerConfigDataDeamonMessage(int serverId, ByteString data) {
        super(MessageType.DEAMON_SERVERCONFIGDATA);

        this.serverId = serverId;
        this.data = data;
    }
}
