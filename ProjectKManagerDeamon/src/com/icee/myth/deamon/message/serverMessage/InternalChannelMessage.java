/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.deamon.message.serverMessage;

import org.jboss.netty.channel.Channel;

/**
 *
 * @author liuxianke
 */
public class InternalChannelMessage extends SimpleMessage {
    public final Channel channel;

    public InternalChannelMessage(MessageType type, Channel channel) {
        super(type);
        this.channel = channel;
    }

}
