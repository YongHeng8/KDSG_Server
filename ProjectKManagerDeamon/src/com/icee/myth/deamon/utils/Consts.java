/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.deamon.utils;

/**
 *
 * @author liuxianke
 */
public class Consts {
//    public static final int BOOTSTRAP_TO_MANAGER_PORT = 15687;

    public static final int FIRST_HEARTBEAT_NUM = 99999;    //服务启动时，channel心跳初始值
    public static final int NORMAL_HEARTBEAT_NUM = 10;  //正常情况下，channel心跳最大值
    public static final int RECONNECT_PERIOD = 2000;    //断线重连间隔时间，2秒
    public static final int HEARTBEAT_PERIOD = 1000;    //心跳间隔时间，1秒

}
