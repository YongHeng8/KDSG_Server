/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.deamon;

import com.icee.myth.deamon.channelContext.HeartbeatChannelContext;

/**
 * 注意：这里的Client接口不是游戏客户端，是Server相互之间通信的Client
 * @author liuxianke
 */
public interface Client {
    public void connectToServer();
    public HeartbeatChannelContext getChannelContext();
}
