/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.deamon.encoder;

import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.MessageLite;
import com.icee.myth.deamon.channelBuffer.NewChannelBuffers;
import com.icee.myth.deamon.protobufMessage.ProtobufMessage;
import com.icee.myth.deamon.utils.LogConsts;
import com.icee.myth.deamon.utils.MLogger;
import com.icee.myth.deamon.utils.StackTraceUtil;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
//import org.jboss.netty.channel.ChannelPipelineCoverage;
import org.jboss.netty.handler.codec.oneone.OneToOneEncoder;

import java.io.IOException;
import java.nio.ByteOrder;
import java.util.ArrayList;

import static org.jboss.netty.buffer.ChannelBuffers.wrappedBuffer;

/**
 * TypeLengthFieldProtobufEncoder用于编码除GW与Map间的服务器间传递消息
 * @author chencheng
 */
//@ChannelPipelineCoverage("all")
//ChannelPipelineCoverage注解了一种处理器类型，这个注解标示了一个处理器是否可被
//多个Channel通道共享（同时关 联着ChannelPipeline）。DiscardServerHandler没有处理任何有状态的信息，因此这里的注解是“all”
public class DeamonToManagerEncoder extends OneToOneEncoder {

    public DeamonToManagerEncoder() {
        super();
    }

    @Override
    protected Object encode(
            ChannelHandlerContext ctx, Channel channel, Object msg) throws Exception {
        if (msg instanceof ProtobufMessage) {
            return encodeProtobufMessage(channel, (ProtobufMessage)msg);
        } else if (msg instanceof ArrayList) {
            ArrayList<Object> msgs = (ArrayList<Object>)msg;
            int size = msgs.size();
            for(int i=0; i<size; i++) {
                Object obj = msgs.get(i);
                if (!(obj instanceof ChannelBuffer)) {
                    msgs.set(i, encodeProtobufMessage(channel, (ProtobufMessage)msgs.get(i)));
                }
            }
            return NewChannelBuffers.wrappedBuffer(msgs);
        } else {
            return msg;
        }
    }

    private Object encodeProtobufMessage(Channel channel, ProtobufMessage pm) {
        if (pm.payload instanceof MessageLite) {
            MessageLite ml = (MessageLite) pm.payload;
            int mlsize = ml.getSerializedSize();
            byte[] res = new byte[4 + mlsize];
            res[0] = (byte) ((pm.type >> 8) & 0xFF);
            res[1] = (byte) (pm.type & 0xFF);
            res[2] = (byte) ((mlsize >> 8) & 0xFF);
            res[3] = (byte) (mlsize & 0xFF);
            CodedOutputStream cos = CodedOutputStream.newInstance(res, 4, mlsize);
            try {
                ml.writeTo(cos);
            } catch (IOException ex) {
                MLogger.getlogger().log(LogConsts.LOGTYPE_NETERR, StackTraceUtil.getStackTrace(ex));
            }
            cos = null;
            return wrappedBuffer(res);
        } else if(pm.payload == null) {
            ChannelBuffer header = channel.getConfig().getBufferFactory().getBuffer(ByteOrder.BIG_ENDIAN, 4);
            header.writeShort((short)pm.type);
            header.writeShort((short)0);
            return header;
        }

        return pm;
    }
}
