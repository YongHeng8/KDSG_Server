/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.deamon.channelHandler;

import com.icee.myth.deamon.messageQueue.ServerMessageQueue;
import com.icee.myth.deamon.protobufMessage.ProtobufMessageType;
import com.icee.myth.deamon.message.serverMessage.builder.DeamonMessageBuilder;
import com.icee.myth.protobuf.DeamonToManagerProtocol;
import com.icee.myth.deamon.utils.LogConsts;
import com.icee.myth.deamon.utils.MLogger;
import com.icee.myth.deamon.utils.StackTraceUtil;
import com.icee.myth.protobuf.DeamonToManagerProtocol.StartServer;
import java.io.IOException;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBufferInputStream;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelStateEvent;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelUpstreamHandler;

/**
 *
 * @author liuxianke
 */
public class DeamonToManagerHandler extends SimpleChannelUpstreamHandler {

    @Override
    public void channelConnected(
            ChannelHandlerContext ctx, ChannelStateEvent e) {
        // Add cluster channel connected message to message queue
        ServerMessageQueue.queue().offer(DeamonMessageBuilder.buildManagerConnectMessage(e.getChannel()));
    }

    @Override
    public void channelClosed(
            ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
        // Add cluster channel close message to message queue
        ServerMessageQueue.queue().offer(DeamonMessageBuilder.buildManagerCloseMessage(e.getChannel()));
    }

    @Override
    public void messageReceived(
            ChannelHandlerContext ctx, MessageEvent e) {
        try {
            // add cluster channel received message to message queue
            ChannelBuffer cb = (ChannelBuffer) e.getMessage();
            short type = cb.readShort();
            int length = cb.readInt();

            switch (type) {
                case ProtobufMessageType.MANAGER2DEAMON_HEARTBEAT: {
                    ServerMessageQueue.queue().offer(DeamonMessageBuilder.buildManagerHeartbeatMessage());
                    break;
                }
                case ProtobufMessageType.MANAGER2DEAMON_STARTSERVER: {
                    StartServer startService = DeamonToManagerProtocol.StartServer.getDefaultInstance().newBuilderForType().mergeFrom(new ChannelBufferInputStream(cb)).build();
                    ServerMessageQueue.queue().offer(DeamonMessageBuilder.buildStartServerMessage(startService.getVmOptionStr(), startService.getParamStr()));
                    break;
                }
                case ProtobufMessageType.MANAGER2DEAMON_SERVERCONFIGDATA: {
                    DeamonToManagerProtocol.ServerConfigData serverConfigData = DeamonToManagerProtocol.ServerConfigData.getDefaultInstance().newBuilderForType().mergeFrom(new ChannelBufferInputStream(cb)).build();
                    ServerMessageQueue.queue().offer(DeamonMessageBuilder.buildServerConfigDataMessage(serverConfigData.getServerId(), serverConfigData.getConfigData()));
                    break;
                }
                // TODO: other message
            }
        } catch (IOException ex) {
            MLogger.getlogger().log(LogConsts.LOGTYPE_NETERR, StackTraceUtil.getStackTrace(ex));
        }
    }

    @Override
    public void exceptionCaught(
            ChannelHandlerContext ctx, ExceptionEvent e) {

        MLogger.getlogger().log(LogConsts.LOGTYPE_NETERR, StackTraceUtil.getStackTrace(e.getCause()));
        // TODO: 是否需要关闭与Cluster的连接
//        e.getChannel().close();
    }
}
