/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.deamon.processOutputThread;

import com.icee.myth.deamon.utils.LogConsts;
import com.icee.myth.deamon.utils.MLogger;
import com.icee.myth.deamon.utils.StackTraceUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 进程输出线程
 *
 * @author liuxianke
 */
public class ProcessOutputThread implements Runnable {

    private final Process process;

    public ProcessOutputThread(Process process) {
        this.process = process;
    }

    @Override
    public void run() {
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String s;
            while ((s = bufferedReader.readLine()) != null) {
                System.out.println(s);
            }
        } catch (IOException ex) {
            MLogger.getlogger().debuglog(LogConsts.LOGLEVEL_ERROR, StackTraceUtil.getStackTrace(ex));
        } finally {
            if (null != bufferedReader) {
                try {
                    bufferedReader.close();
                } catch (Exception ex) {
                    MLogger.getlogger().debuglog(LogConsts.LOGLEVEL_ERROR, StackTraceUtil.getStackTrace(ex));
                }
            }
        }
    }

}
