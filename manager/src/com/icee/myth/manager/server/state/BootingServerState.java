/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.manager.server.state;

import com.icee.myth.common.message.serverMessage.Message;
import com.icee.myth.common.message.serverMessage.Message.MessageType;
import com.icee.myth.manager.Manager;
import com.icee.myth.manager.server.Server;
import com.icee.myth.protobuf.builder.ConsoleToManagerBuilder;
import com.icee.myth.utils.LogConsts;
import com.icee.myth.utils.MLogger;

/**
 * 启动中状态
 * @author liuxianke
 */
public class BootingServerState implements ServerState {
    public final static BootingServerState INSTANCE = new BootingServerState();

    private BootingServerState() {
    }

    @Override
    public void handleMessage(Server server, Message message) {
        MessageType msgType = message.getType();
        switch (msgType) {
            case MANAGER_CLUSTER_CLOSE: {
                server.managerToServerClient.handleMessage(message);

                // 状态不变
                break;
            }
            case MANAGER_CLUSTER_CONNECT: {
                server.managerToServerClient.handleMessage(message);

                // 将结果发回Console
                Manager.INSTANCE.broadcastToConsole(ConsoleToManagerBuilder.buildServerStartOK(server.getId()));

                // 恢复正常
                server.state = RunningServerState.INSTANCE;
                break;
            }
            default: {
                // 消息异常（不作处理）,记录日志
                MLogger.getlogger().debuglog(LogConsts.LOGLEVEL_ERROR, "BootingServerState handle error message type[" + msgType + "].");
                break;
            }
        }
    }
}
