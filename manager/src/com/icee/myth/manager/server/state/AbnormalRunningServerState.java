/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.manager.server.state;

import com.icee.myth.common.message.serverMessage.Message;
import com.icee.myth.common.message.serverMessage.Message.MessageType;
import com.icee.myth.manager.server.Server;
import com.icee.myth.utils.LogConsts;
import com.icee.myth.utils.MLogger;

/**
 * 服务异常运行状态
 * @author liuxianke
 */
public class AbnormalRunningServerState implements ServerState {
    public final static AbnormalRunningServerState INSTANCE = new AbnormalRunningServerState();

    private AbnormalRunningServerState() {
    }

    @Override
    public void handleMessage(Server server, Message message) {
        MessageType msgType = message.getType();
        switch (msgType) {
            case MANAGER_CLUSTER_CLOSE: {
                server.managerToServerClient.handleMessage(message);

                // 状态不变
                break;
            }
            case MANAGER_CLUSTER_CONNECT: {
                server.managerToServerClient.handleMessage(message);

                // 恢复正常
                server.state = RunningServerState.INSTANCE;
                break;
            }
            default: {
                // 消息异常（不作处理）,记录日志
                MLogger.getlogger().debuglog(LogConsts.LOGLEVEL_ERROR, "AbnormalRunningServerState handle error message type[" + msgType + "].");
                break;
            }
        }
    }
}
