/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.manager.server.state;

import com.icee.myth.common.message.serverMessage.Message;
import com.icee.myth.common.message.serverMessage.Message.MessageType;
import com.icee.myth.manager.Manager;
import com.icee.myth.manager.server.Server;
import com.icee.myth.protobuf.builder.ConsoleToManagerBuilder;
import com.icee.myth.utils.LogConsts;
import com.icee.myth.utils.MLogger;

/**
 *
 * @author liuxianke
 */
public class ClosingServerState implements ServerState {
    public final static ClosingServerState INSTANCE = new ClosingServerState();

    private ClosingServerState() {
    }

    @Override
    public void handleMessage(Server server, Message message) {
        MessageType msgType = message.getType();
        switch (msgType) {
            case MANAGER_CLUSTER_CLOSE: {
                server.managerToServerClient.handleMessage(message);

                // 将结果发回Console
                Manager.INSTANCE.broadcastToConsole(ConsoleToManagerBuilder.buildServerShutdownOK(server.getId()));

                // 将Server状态改为“已关闭”
                server.state = ShutdownServerState.INSTANCE;
                break;
            }
            default: {
                // 消息异常（不作处理）,记录日志
                MLogger.getlogger().debuglog(LogConsts.LOGLEVEL_ERROR, "ClosingServerState handle error message type[" + msgType + "].");
                break;
            }
        }
    }
}
