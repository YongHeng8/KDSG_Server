/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.manager.server.state;

import com.icee.myth.common.message.serverMessage.Message;
import com.icee.myth.manager.server.Server;

/**
 *
 * @author liuxianke
 */
public interface ServerState {
    public void handleMessage(Server server, Message message);
}
