/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.manager.server.state;

import com.icee.myth.common.message.serverMessage.Message;
import com.icee.myth.common.message.serverMessage.Message.MessageType;
import com.icee.myth.manager.message.serverMessage.GmMessage;
import com.icee.myth.manager.server.Server;
import com.icee.myth.utils.LogConsts;
import com.icee.myth.utils.MLogger;

/**
 *
 * @author liuxianke
 */
public class RunningServerState implements ServerState {
    public final static RunningServerState INSTANCE = new RunningServerState();

    private RunningServerState() {
    }

    @Override
    public void handleMessage(Server server, Message message) {
        MessageType msgType = message.getType();
        switch (msgType) {
            case MANAGER_SHUTDOWN_SERVER: {
                server.shutdown();
                break;
            }
            case MANAGER_GM: {
                server.gm((GmMessage)message);
                break;
            }
            case MANAGER_CLEAR_TEST_FLAG:{
                server.setTestFlag(false);
                break;
            }
            case MANAGER_SET_TEST_FLAG:{
                server.setTestFlag(true);
                break;
            }
            case MANAGER_CLUSTER_CLOSE: {
                server.managerToServerClient.handleMessage(message);
                // 将Server状态改为“异常”
                server.state = AbnormalRunningServerState.INSTANCE;
                break;
            }
            case MANAGER_BILL_NOTIFY: {
                server.managerToServerClient.handleMessage(message);
                break;
            }
            default: {
                // 消息异常（不作处理）,记录日志
                MLogger.getlogger().debuglog(LogConsts.LOGLEVEL_ERROR, "RunningServerState handle error message type[" + msgType + "].");
                break;
            }
        }
    }
}
