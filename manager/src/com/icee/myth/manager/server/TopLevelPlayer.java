/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.manager.server;

/**
 *
 * @author yangyi
 */
public class TopLevelPlayer {
    public int id;
    public String name;
    public int sex;
    public int job;
    public int level;

    public TopLevelPlayer(int id, String name, int sex, int job, int level){
        this.id = id;
        this.name = name;
        this.sex = sex;
        this.job = job;
        this.level = level;
    }
}
