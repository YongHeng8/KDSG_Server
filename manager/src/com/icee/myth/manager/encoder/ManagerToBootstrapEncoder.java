/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.manager.encoder;

import static org.jboss.netty.buffer.ChannelBuffers.*;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.oneone.OneToOneEncoder;

/**
 * 因为Jar文件数据格式可以被JarInputStream解析，这里不需要增加任何头部信息
 * @author liuxianke
 */
public class ManagerToBootstrapEncoder extends OneToOneEncoder {

    public ManagerToBootstrapEncoder() {
        super();
    }

    @Override
    protected Object encode(
            ChannelHandlerContext ctx, Channel channel, Object msg) throws Exception {
        if (msg instanceof byte[]) {
            return wrappedBuffer((byte[])msg);
        }
        return msg;
    }
}
