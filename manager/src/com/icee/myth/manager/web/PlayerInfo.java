package com.icee.myth.manager.web;

/**
 *
 * @author yangyi
 */
public class PlayerInfo {
    public String thirdPartyUserId;
    public String thirdPartyUserName;
    public int cid;
    public boolean auth ;                     //是否实名验证
    public int forbiddenEndTalkTime;
    public int forbiddenEndLoginTime;
    public String clientIp;
    
    public String sessionId;
    public long  time;

    public PlayerInfo(String passport) {
        this.thirdPartyUserId = passport;
    }

    public PlayerInfo(String thirdPartyUserId, int cid, int forbiddenEndTalkTime, int forbiddenEndLoginTime) {
        this.thirdPartyUserId = thirdPartyUserId;
        this.cid = cid;
        this.forbiddenEndTalkTime = forbiddenEndTalkTime;
        this.forbiddenEndLoginTime = forbiddenEndLoginTime;
    }

//    @Override
//    public boolean equals(Object obj) {
//	if (obj instanceof PlayerInfo) {
//            PlayerInfo other = (PlayerInfo)obj;
//	    if((thirdPartyUserId == null ? other.thirdPartyUserId == null : thirdPartyUserId.equals(other.thirdPartyUserId))){
//                return true;
//            }
//	}
//	return false;
//    }
//
//    @Override
//    public int hashCode() {
//        int hash = 3;
//        hash = 19 * hash + (this.thirdPartyUserId != null ? this.thirdPartyUserId.hashCode() : 0);
//        return hash;
//    }
    
}
