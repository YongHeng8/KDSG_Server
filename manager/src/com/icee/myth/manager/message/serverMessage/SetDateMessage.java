/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.manager.message.serverMessage;

import com.icee.myth.common.message.serverMessage.SimpleMessage;

/**
 *
 * @author liuxianke
 */
public class SetDateMessage extends SimpleMessage {
    public final long date;

    public SetDateMessage(long date) {
        super(MessageType.MANAGER_SET_DATE);

        this.date = date;
    }
}
