/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.manager.message.serverMessage;

import com.icee.myth.common.message.serverMessage.SimpleMessage;

/**
 *
 * @author liuxianke
 */
public class DeamonDownManagerMessage extends SimpleMessage {
    public final String deamonIP;

    public DeamonDownManagerMessage(String deamonIP) {
        super(MessageType.MANAGER_DEAMON_DOWN);

        this.deamonIP = deamonIP;
    }
}
