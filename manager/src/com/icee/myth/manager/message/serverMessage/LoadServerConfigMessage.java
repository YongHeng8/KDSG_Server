/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.manager.message.serverMessage;

import com.icee.myth.manager.config.ServerConfig;

/**
 *
 * @author liuxianke
 */
public class LoadServerConfigMessage extends ServerManagerMessage {
    public final ServerConfig serverConfig;

    public LoadServerConfigMessage(int serverId, ServerConfig serverConfig) {
        super(MessageType.MANAGER_LOAD_SERVER_CONFIG, serverId);

        this.serverConfig = serverConfig;
    }
}
