/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.manager.message.serverMessage;

import org.jboss.netty.channel.Channel;

/**
 *
 * @author liuxianke
 */
public class ClusterConnectManagerMessage extends ServerManagerMessage {
    public final Channel channel;

    public ClusterConnectManagerMessage(Channel channel, int serverId) {
        super(MessageType.MANAGER_CLUSTER_CONNECT, serverId);
        this.channel = channel;
    }
}
