/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.manager.message.serverMessage;

import com.icee.myth.common.message.serverMessage.SimpleMessage;
import com.icee.myth.protobuf.ExternalCommonProtocol.IntValuesProto;

/**
 *
 * @author liuxianke
 */
public class BattleWinRateResultMessage extends SimpleMessage {
    public final IntValuesProto winRateResult;

    public BattleWinRateResultMessage(IntValuesProto winRateResult) {
        super(MessageType.MANAGER_BATTLE_WIN_RATE_RESULT);

        this.winRateResult = winRateResult;
    }
}
