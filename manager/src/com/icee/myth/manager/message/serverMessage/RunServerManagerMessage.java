/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.manager.message.serverMessage;

/**
 *
 * @author liuxianke
 */
public class RunServerManagerMessage extends ServerManagerMessage {
    public final boolean needSendConfig;

    public RunServerManagerMessage(int serverId, boolean needSendConfig) {
        super(MessageType.MANAGER_RUN_SERVER, serverId);

        this.needSendConfig = needSendConfig;
    }
}
