package com.icee.myth.manager.message.serverMessage.builder;

import com.icee.myth.manager.message.serverMessage.BillNotifyServerMessage;
import com.icee.myth.manager.message.serverMessage.GmMessage;
import com.icee.myth.common.message.serverMessage.InternalChannelMessage;
import com.icee.myth.common.message.serverMessage.Message;
import com.icee.myth.common.message.serverMessage.Message.MessageType;
import com.icee.myth.common.message.serverMessage.SimpleMessage;
import com.icee.myth.manager.config.ServerConfig;
import com.icee.myth.manager.message.serverMessage.BattleWinRateResultMessage;
import com.icee.myth.manager.message.serverMessage.CardDrawRateResultMessage;
import com.icee.myth.manager.message.serverMessage.ClusterConnectManagerMessage;
import com.icee.myth.manager.message.serverMessage.RunServerManagerMessage;
import com.icee.myth.manager.message.serverMessage.DeamonDownManagerMessage;
import com.icee.myth.manager.message.serverMessage.FightingOccupyInfoMessage;
import com.icee.myth.manager.message.serverMessage.LoadServerConfigMessage;
import com.icee.myth.manager.message.serverMessage.SetTimeMessage;
import com.icee.myth.manager.message.serverMessage.ServerManagerMessage;
import com.icee.myth.manager.message.serverMessage.SetDateMessage;
import com.icee.myth.manager.message.serverMessage.UninitOccupyInfoMessage;
import com.icee.myth.protobuf.ConsoleToManagerProtocol.C2MGmProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.IntValuesProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.VariableValuesProto;
import org.jboss.netty.channel.Channel;

/**
 * Manager消息生成器类
 * @author liuxianke
 */
public class ManagerMessageBuilder {

    public static Message buildDeamonConnectMessage(Channel channel) {
        return new InternalChannelMessage(MessageType.MANAGER_DEAMON_CONNECT, channel);
    }

    public static Message buildDeamonCloseMessage(Channel channel) {
        return new InternalChannelMessage(MessageType.MANAGER_DEAMON_CLOSE, channel);
    }

    public static Message buildDeamonDownMessage(String deamonIP) {
        return new DeamonDownManagerMessage(deamonIP);
    }

    public static Message buildConsoleConnectMessage(Channel channel) {
        return new InternalChannelMessage(MessageType.MANAGER_CONSOLE_CONNECT, channel);
    }

    public static Message buildConsoleCloseMessage(Channel channel) {
        return new InternalChannelMessage(MessageType.MANAGER_CONSOLE_CLOSE, channel);
    }

    public static Message buildClusterConnectMessage(Channel channel, int serverId) {
        return new ClusterConnectManagerMessage(channel, serverId);
    }

    public static Message buildClusterHeartbeatMessage(int serverId) {
        return new ServerManagerMessage(MessageType.MANAGER_CLUSTER_HEARTBEAT, serverId);
    }

    public static Message buildClusterCloseMessage(int serverId) {
        return new ServerManagerMessage(MessageType.MANAGER_CLUSTER_CLOSE, serverId);
    }

    public static Message buildClusterDownMessage(int serverId) {
        return new ServerManagerMessage(MessageType.MANAGER_CLUSTER_DOWN, serverId);
    }

    public static Message buildRunServerMessage(int serverId, boolean needSendConfig) {
        return new RunServerManagerMessage(serverId, needSendConfig);
    }

    public static Message buildShutdownServerMessage(int serverId) {
        return new ServerManagerMessage(MessageType.MANAGER_SHUTDOWN_SERVER, serverId);
    }

    public static Message buildSaveNotifyServerMessage(int serverId, int playerId) {
        return new BillNotifyServerMessage(serverId, playerId);
    }

    public static Message buildSyncConfigMessage() {
        return new SimpleMessage(MessageType.MANAGER_SYNCCONFIG);
    }

    public static Message buildGetServerStatusMessage() {
        return new SimpleMessage(MessageType.MANAGER_GET_SERVER_STATUS);
    }

    public static Message buildGmMessage(C2MGmProto gmProto) {
        return new GmMessage(gmProto);
    }

    public static Message buildClearTestFlagMessage(int serverId) {
        return new ServerManagerMessage(MessageType.MANAGER_CLEAR_TEST_FLAG, serverId);
    }

    public static Message buildSetTestFlagMessage(int serverId) {
        return new ServerManagerMessage(MessageType.MANAGER_SET_TEST_FLAG, serverId);
    }

    public static Message buildForceSetServerShutdownStatusMessage(int serverId) {
        return new ServerManagerMessage(MessageType.MANAGER_FORCE_SET_SERVER_SHUTDOWN_STATUS, serverId);
    }

    public static Message buildLoadServerConfigMessage(int serverId, ServerConfig serverConfig) {
        return new LoadServerConfigMessage(serverId, serverConfig);
    }

    public static Message buildBattleWinRateResultMessage(IntValuesProto winRateResult) {
        return new BattleWinRateResultMessage(winRateResult);
    }

    public static Message buildCardDrawRateResultMessage(VariableValuesProto cardDrawRateResult) {
        return new CardDrawRateResultMessage(cardDrawRateResult);
    }

    public static Message buildUninitOccupyInfoMessage(IntValuesProto intValuesProto) {
        return new UninitOccupyInfoMessage(intValuesProto);
    }

    public static Message buildFightingOccupyInfoMessage(IntValuesProto intValuesProto) {
        return new FightingOccupyInfoMessage(intValuesProto);
    }

    public static Message buildSetDateMessage(long date) {
        return new SetDateMessage(date);
    }

    public static Message buildSetTimeMessage(long time) {
        return new SetTimeMessage(time);
    }

    public static Message buildGetDateTimeMessage() {
        return new SimpleMessage(MessageType.MANAGER_GET_DATE_TIME);
    }
}
