/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.manager.message.serverMessage;

import com.icee.myth.common.message.serverMessage.SimpleMessage;
import com.icee.myth.protobuf.ExternalCommonProtocol.VariableValuesProto;

/**
 *
 * @author liuxianke
 */
public class CardDrawRateResultMessage extends SimpleMessage {
    public final VariableValuesProto cardDrawRateResult;

    public CardDrawRateResultMessage(VariableValuesProto cardDrawRateResult) {
        super(MessageType.MANAGER_CARD_DRAW_RATE_RESULT);

        this.cardDrawRateResult = cardDrawRateResult;
    }
}
