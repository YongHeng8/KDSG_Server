/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.common.message.serverMessage;

/**
 *
 * @author liuxianke
 */
public interface Message {
    /**
     * 消息类型                         -- 消息说明
     * MAPSCENE_XXX                    -- XXX
     *
     */
    public enum MessageType {
        ALL_HEARTBEAT,
        MANAGER_DEAMON_HEARTBEAT,
        MANAGER_DEAMON_CONNECT,
        MANAGER_DEAMON_CLOSE,
        MANAGER_DEAMON_DOWN,
        MANAGER_CONSOLE_CONNECT,
        MANAGER_CONSOLE_CLOSE,
        MANAGER_CLUSTER_HEARTBEAT,
        MANAGER_CLUSTER_CONNECT,
        MANAGER_CLUSTER_CLOSE,
        MANAGER_CLUSTER_DOWN,
        MANAGER_RUN_SERVER,
        MANAGER_SHUTDOWN_SERVER,
        MANAGER_SYNCCONFIG,
        MANAGER_GET_SERVER_STATUS,
        MANAGER_GM,
        MANAGER_CLEAR_TEST_FLAG,
        MANAGER_SET_TEST_FLAG,
        MANAGER_BILL_NOTIFY,
        MANAGER_FORCE_SET_SERVER_SHUTDOWN_STATUS,
        MANAGER_LOAD_SERVER_CONFIG,
        MANAGER_BATTLE_WIN_RATE_RESULT,
        MANAGER_CARD_DRAW_RATE_RESULT,
        MANAGER_UNINIT_OCCUPY_INFO,
        MANAGER_FIGHTING_OCCUPY_INFO,
        MANAGER_SET_DATE,
        MANAGER_SET_TIME,
        MANAGER_GET_DATE_TIME
    }

    public MessageType getType();
}
