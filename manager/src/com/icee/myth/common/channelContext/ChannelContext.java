/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.common.channelContext;

import com.icee.myth.utils.LinkedTransferQueue;
import java.util.ArrayList;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFuture;


/**
 *
 * @author liuxianke
 */
public class ChannelContext {

    private static final int MAX_FLUSH_MESSAGE_NUM_PER_WRITING = 1024;

    protected final LinkedTransferQueue<Object> messageQueue = new LinkedTransferQueue<Object>();

    protected Channel channel = null;  // channel to server
    protected boolean isActive = false;

    public ChannelContext() {
    }

    public void setChannel(Channel channel) {
        if (channel!=null) {
            this.channel = channel;
            isActive = true;
        }
    }

    public boolean isActive() {
        return isActive;
    }

    public final void write(Object message) {
        if(isActive) {
            messageQueue.add(message);
        }
    }

    public void flush() {
        if(isActive) {
            Object msg = messageQueue.poll();
            if (msg != null) {
                boolean finish = false;
                while(!finish) {
                    ArrayList<Object> messages = new ArrayList<Object>(MAX_FLUSH_MESSAGE_NUM_PER_WRITING);
                    while (msg != null && messages.size()<MAX_FLUSH_MESSAGE_NUM_PER_WRITING) {
                        messages.add(msg);
                        msg = messageQueue.poll();
                    }
                    finish = messages.size()<MAX_FLUSH_MESSAGE_NUM_PER_WRITING;
                    channel.write(messages);
                }
            }
        }
    }

    public ChannelFuture fastWrite(Object message) {
        if(isActive) {
            return channel.write(message);
        } else {
            return null;
        }
    }

    public ChannelFuture close() {
        if(isActive) {
            isActive = false;
            messageQueue.clear();
            return channel.close();
        } else
            return null;
    }

    public boolean isChannel(Channel channel) {
        return this.channel == channel;
    }

    public int getId() {
        if(isActive)
            return this.channel.getId();
        else
            return -1;
    }

}
