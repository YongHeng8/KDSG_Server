/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.protobuf.builder;

import com.icee.myth.common.protobufMessage.ProtobufMessage;
import com.icee.myth.common.protobufMessage.ProtobufMessageType;
import com.icee.myth.manager.message.serverMessage.GmMessage;
import com.icee.myth.protobuf.ConsoleToManagerProtocol.M2SGmProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.IntValueProto;

/**
 *
 * @author liuxianke
 */
public class ManagerToClusterBuilder {

    public static ProtobufMessage buildManagerHeartbeat() {
        return new ProtobufMessage(ProtobufMessageType.MANAGER2CLUSTER_HEARTBEAT,null);
    }

    public static ProtobufMessage buildShutdown() {
        return new ProtobufMessage(ProtobufMessageType.MANAGER2CLUSTER_SHUTDOWN, null);
    }

    public static ProtobufMessage buildGmMessage(GmMessage gmMessage) {
        M2SGmProto.Builder gmProto = M2SGmProto.newBuilder();
        gmProto.setPlayerId(gmMessage.playerId);
        gmProto.setGmCmd(gmMessage.gmCmd);
        for(String args : gmMessage.gmArgs){
            gmProto.addArgs(args);
        }
        return new ProtobufMessage(ProtobufMessageType.MANAGER2CLUSTER_GM, gmProto.build());
    }

    public static ProtobufMessage buildBillNotifyMessage(int playerId) {
        IntValueProto.Builder intvalue = IntValueProto.newBuilder();
        intvalue.setValue(playerId);
        return new ProtobufMessage(ProtobufMessageType.MANAGER2CLUSTER_BILLNOTIFY, intvalue.build());
    }

}
