/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.protobuf.builder;

import com.icee.myth.common.protobufMessage.ProtobufMessage;
import com.icee.myth.common.protobufMessage.ProtobufMessageType;
import com.icee.myth.protobuf.ConsoleToManagerProtocol.ServerStatus;
import com.icee.myth.protobuf.ConsoleToManagerProtocol.ServerStatuses;
import com.icee.myth.protobuf.ExternalCommonProtocol.IntValueProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.IntValuesProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.LongValueProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.VariableValueProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.VariableValuesProto;

/**
 *
 * @author liuxianke
 */
public class ConsoleToManagerBuilder {

    public static ProtobufMessage buildServerStartOK(int serverId) {
        IntValueProto.Builder builder = IntValueProto.newBuilder();
        builder.setValue(serverId);

        return new ProtobufMessage(ProtobufMessageType.MANAGER2CONSOLE_SERVER_START_OK, builder.build());
    }

    public static ProtobufMessage buildServerShutdownOK(int serverId) {
        IntValueProto.Builder builder = IntValueProto.newBuilder();
        builder.setValue(serverId);

        return new ProtobufMessage(ProtobufMessageType.MANAGER2CONSOLE_SERVER_SHUTDOWN_OK, builder.build());
    }

    public static ProtobufMessage buildSyncConfigReturn(int result) {
        IntValueProto.Builder builder = IntValueProto.newBuilder();
        builder.setValue(result);

        return new ProtobufMessage(ProtobufMessageType.MANAGER2CONSOLE_SYNCCONFIG_RETURN, builder.build());
    }

    public static ProtobufMessage buildGetServerStatusReturn(int[][] statuses) {
        ServerStatuses.Builder builder = ServerStatuses.newBuilder();

        int serverNum = statuses.length;
        for (int i=0; i<serverNum; i++) {
            ServerStatus.Builder builder1 = ServerStatus.newBuilder();
            builder1.setId(statuses[i][0]);
            builder1.setStatus(statuses[i][1]);

            builder.addStatuses(builder1);
        }

        return new ProtobufMessage(ProtobufMessageType.MANAGER2CONSOLE_GET_SERVER_STATUS_RETURN, builder.build());
    }

    public static ProtobufMessage buildForceSetServerShutdownStatusOK(int serverId) {
        IntValueProto.Builder builder = IntValueProto.newBuilder();
        builder.setValue(serverId);

        return new ProtobufMessage(ProtobufMessageType.MANAGER2CONSOLE_FORCE_SET_SERVER_SHUTDOWN_STATUS_OK, builder.build());
    }

    public static ProtobufMessage buildForceSetServerShutdownStatusError(int serverId) {
        IntValueProto.Builder builder = IntValueProto.newBuilder();
        builder.setValue(serverId);

        return new ProtobufMessage(ProtobufMessageType.MANAGER2CONSOLE_FORCE_SET_SERVER_SHUTDOWN_STATUS_ERROR, builder.build());
    }

    public static ProtobufMessage buildLoadServerConfigError(int serverId, int errorCode) {
        VariableValueProto.Builder builder = VariableValueProto.newBuilder();
        builder.setId(serverId);
        builder.setValue(errorCode);

        return new ProtobufMessage(ProtobufMessageType.MANAGER2CONSOLE_LOAD_SERVER_CONFIG_ERROR, builder.build());
    }

    public static ProtobufMessage buildLoadServerConfigOK(int serverId) {
        IntValueProto.Builder builder = IntValueProto.newBuilder();
        builder.setValue(serverId);

        return new ProtobufMessage(ProtobufMessageType.MANAGER2CONSOLE_LOAD_SERVER_CONFIG_OK, builder.build());
    }

    public static ProtobufMessage buildBattleWinRateResult(IntValuesProto winRateResult) {
        return new ProtobufMessage(ProtobufMessageType.MANAGER2CONSOLE_BATTLE_WIN_RATE_RESULT, winRateResult);
    }

    public static ProtobufMessage buildCardDrawRateResult(VariableValuesProto cardDrawRateResult) {
        return new ProtobufMessage(ProtobufMessageType.MANAGER2CONSOLE_CARD_DRAW_RATE_RESULT, cardDrawRateResult);
    }

    public static ProtobufMessage buildUninitOccupyInfo(IntValuesProto intValuesProto) {
        return new ProtobufMessage(ProtobufMessageType.MANAGER2CONSOLE_UNINIT_OCCUPY_INFO, intValuesProto);
    }

    public static ProtobufMessage buildFightingOccupyInfo(IntValuesProto intValuesProto) {
        return new ProtobufMessage(ProtobufMessageType.MANAGER2CONSOLE_FIGHTING_OCCUPY_INFO, intValuesProto);
    }

    public static ProtobufMessage buildDateTime(long currentTime) {
        LongValueProto.Builder builder = LongValueProto.newBuilder();
        builder.setValue(currentTime);

        return new ProtobufMessage(ProtobufMessageType.MANAGER2CONSOLE_DATE_TIME, builder.build());
    }
}
