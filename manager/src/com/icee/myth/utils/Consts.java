/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.utils;

/**
 *
 * @author liuxianke
 */
public class Consts {
    public static final int FIRST_HEARTBEAT_NUM = 99999;    //服务启动时，channel心跳初始值
    public static final int NORMAL_HEARTBEAT_NUM = 10;  //正常情况下，channel心跳最大值
    public static final int HEARTBEAT_PERIOD = 1000;    //心跳间隔时间，1秒
    public static final int RECONNECT_PERIOD = 2000;    //断线重连间隔，2秒

    public static final int DB_KEEPALIVE_INTERVAL = 3600000; // ms, 1 hour
    public static final String DB_KEEPALIVE_TEST_STATEMENT = "SELECT 1 FROM INFORMATION_SCHEMA.VIEWS";

    public static final int MILSECOND_8HOUR = 8*3600*1000;  // 8小时的毫秒数（用于时差）
    public static final int MILSECOND_ONE_DAY = 24*3600*1000;   // 一天毫秒数
    public static final int SESSION_EXPIRE_TIME = 60*60*1000;    // 1小时
    public static final int JET_LAG = MILSECOND_8HOUR;

//    public static final String SERVERS_CONFIG_FILEPATH = "managerConfig.json";
    public static final String ACTIVITIES_CONFIG_FILEPATH = "activitiesConfig.json";

//    public static final int BOOTSTRAP_TO_MANAGER_PORT = 15687;
    public static final int START_MODE_LOCAL = 0;   // 本地资源启动
    public static final int START_MODE_REMOTE = 1;  // 远程资源启动

    public static final int CONSOLE_COMMAND_STARTSERVER = 0;    // 控制台命令：启动服务
    public static final int CONSOLE_COMMAND_STOPSERVER = 1;     // 控制台命令：停止服务
    // TODO: 其他命令

    public static final int SERVICE_TYPE_CLUSTER = 0;   // 服务类型0：Cluster
    public static final int SERVICE_TYPE_LGW = 1;       // 服务类型1：LGW
    public static final int SERVICE_TYPE_GW = 2;        // 服务类型2：GW
    public static final int SERVICE_TYPE_MAP = 3;       // 服务类型3：Map

    public static final int SERVER_STATUS_UNKNOWN = 0;  //服务状态：未知
    public static final int SERVER_STATUS_BOOTING = 1;  //服务状态：正在启动
    public static final int SERVER_STATUS_RUNNING = 2;  //服务状态：运行
    public static final int SERVER_STATUS_CLOSING = 3;  //服务状态：正在关闭
    public static final int SERVER_STATUS_SHUTDOWN = 4;  //服务状态：关闭
    public static final int SERVER_STATUS_ABNORMALRUNNING = 5;  //服务状态：运行异常
    public static final int SERVER_STATUS_DUMMY = 6;    //服务状态：无状态
    public static final int SERVER_STATUS_TESTING = 7;  //服务状态：测试运行

    public static final int ACTIVITY_PRIZE_PROBABILITY_RANGE = 10000;   //奖励概率范围
    public static final int ACTIVITY_TARGET_TYPE_ALLONLINE = 0; // 活动目标类型：所有在线玩家

    public static final int ACTIVITY_PRIZESGIVING_TYPE_PROBABILITY = 0;     // 活动奖励类型：概率奖励
    public static final int ACTIVITY_PRIZESGIVING_TYPE_FIXEDQUANTITY = 1;   // 活动奖励类型：定量奖励
    // TODO:其他活动目标类型定义

    public static final int OK = 0;
    public static final int ERR = -1;
    public static final int PRIVILEGE_NONE = 0;
    public static final int PRIVILEGE_QUERY = 1;             //查询信息
    public static final int PRIVILEGE_MODIFY = 2;            //修改玩家信息
    public static final int PRIVILEGE_POST = 4;              //发公告
    public static final int PRIVILEGE_OPERRATE = 8;          //运维
    public static final int PRIVILEGE_FORBIDLOGINORTALK = 16;   //禁言，踢人

    public static final int GENERATETTIMERANDOM = 20;             //防止同时产生，移位后在后面加入随机数
    
    public static int PRODUCEKEYMAXRETRYTIMES = 2;

    //登录失败
    public static final int GETSESSION_ERR_FORBIDDEN_LOGIN = -1 ;    //玩家被禁止登陆
    public static final int GETSESSION_ERR_SERVER = -2 ;    //系统错误
    public static final int ERR_SERVER = -1 ;    //获取排名列表错误
    public static final int PARAMETER_SERVER = -100 ;
    public static final int ERR_SERVER_NOTHISPLAYER = -3 ;    //根据passport没有找到cid。passport错误或者db查询失败

    //
    public static final int CHECKSESSION_ERR_SESSION = -1 ;    //没有这个session
    public static final int CHECKSESSION_ERR_IP = -2 ;    //没有这个session
//    public static int MAXSESSIONNUM = 1000000;
//    public static int SESSIONPERIOD = 3600000;
//    public static int MAXSESSIONNUM = 3;
//    public static int SESSIONPERIOD = 360000;

    public static final int TOPLEVELNUM = 10;       //获取一个服里排名前几名的玩家
    public static final long LEVELRANKPERIOD = 5*60*1000;

    public static final int UNION_MEMBER_NORMAL = 1;        //团员
    public static final int UNION_MEMBER_EXCELLENT = 2;     //精英
    public static final int UNION_MEMBER_ELDER = 3;        //长老
    public static final int UNION_MEMBER_VICEHEAD = 4;        //副团长
    public static final int UNION_MEMBER_HEAD = 5;        //团长

    public static final int LOADSERVERCONFIG_ERROR_NOT_SHUTDOWN = 1;    // 加载服务器配置错误：未关闭
    public static final int LOADSERVERCONFIG_ERROR_NOT_FOUND = 2;       // 加载服务器配置错误：无配置

//    public static final int RPCSET_BLUESOUL = 0;
//    public static final int RPCSET_YELLOWSOUL1 = 1;
//    public static final int RPCSET_YELLOWSOUL2 = 2;
//    public static final int RPCSET_REDSOUL = 3;
//    public static final int RPCSET_GREENSOUL = 4;
//    public static final int RPCSET_STARSOUL = 5;
//    public static final int RPCSET_ORACLE = 6;
//    public static final int RPCSET_TALENTPOINT = 7;
//
//    public static int GMOPERATELOG_POST = 1;
//    public static int GMOPERATELOG_FORBIDTALK = 2;
//    public static int GMOPERATELOG_FORBIDLOGIN = 3;
//    public static int GMOPERATELOG_PERMITLOGIN = 4;
//    public static int GMOPERATELOG_PERMITTALK = 5;
//    public static int GMOPERATELOG_SETBLUESOUL = 6;
//    public static int GMOPERATELOG_SETYELLOWSOUL1 = 7;
//    public static int GMOPERATELOG_SETYELLOWSOUL2 = 8;
//    public static int GMOPERATELOG_SETREDSOUL = 9;
//    public static int GMOPERATELOG_SETGREENSOUL = 10;
//    public static int GMOPERATELOG_SETSTARSOUL = 11;
//    public static int GMOPERATELOG_SETORACLE = 12;
//    public static int GMOPERATELOG_SETTALENTPOINT = 13;
//    public static final int GMOPERATELOG_ADDITEM = 14;
//    public static int GMOPERATELOG_REMOVEITEM = 15;
    
}
