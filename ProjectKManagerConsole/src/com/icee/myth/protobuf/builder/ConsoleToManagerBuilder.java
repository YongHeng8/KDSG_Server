/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.protobuf.builder;

import com.icee.myth.common.protobufMessage.ProtobufMessage;
import com.icee.myth.common.protobufMessage.ProtobufMessageType;
import com.icee.myth.protobuf.ConsoleToManagerProtocol.C2MGmProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.IntValueProto;
import com.icee.myth.protobuf.ExternalCommonProtocol.LongValueProto;
import java.util.ArrayList;

/**
 *
 * @author liuxianke
 */
public class ConsoleToManagerBuilder {

    public static ProtobufMessage buildRunServer(int serverId) {
        IntValueProto.Builder builder = IntValueProto.newBuilder();
        builder.setValue(serverId);

        return new ProtobufMessage(ProtobufMessageType.CONSOLE2MANAGER_RUN_SERVER, builder.build());
    }

    public static ProtobufMessage buildShutdownServer(int serverId) {
        IntValueProto.Builder builder = IntValueProto.newBuilder();
        builder.setValue(serverId);

        return new ProtobufMessage(ProtobufMessageType.CONSOLE2MANAGER_SHUTDOWN_SERVER, builder.build());
    }

    public static ProtobufMessage buildSyncConfig() {
        return new ProtobufMessage(ProtobufMessageType.CONSOLE2MANAGER_SYNCCONFIG, null);
    }

    public static ProtobufMessage buildGetServerState() {
        return new ProtobufMessage(ProtobufMessageType.CONSOLE2MANAGER_GET_SERVER_STATUS, null);
    }

    public static ProtobufMessage buildDebugRunServer(int serverId) {
        IntValueProto.Builder builder = IntValueProto.newBuilder();
        builder.setValue(serverId);

        return new ProtobufMessage(ProtobufMessageType.CONSOLE2MANAGER_DEBUG_RUN_SERVER, builder.build());
    }

    public static ProtobufMessage buildGm(ArrayList<String> strArgs) {
        C2MGmProto.Builder builder = C2MGmProto.newBuilder();
        builder.setServerId(Integer.valueOf(strArgs.get(0)));
        builder.setPlayerId(Integer.valueOf(strArgs.get(1)));
        builder.setGmCmd(strArgs.get(2));
        for(int i=3;i<strArgs.size();i++){
            builder.addArgs(strArgs.get(i));
        }

        return new ProtobufMessage(ProtobufMessageType.CONSOLE2MANAGER_GM, builder.build());
    }

    public static ProtobufMessage buildClearTestFlag(int serverId) {
        IntValueProto.Builder builder = IntValueProto.newBuilder();
        builder.setValue(serverId);

        return new ProtobufMessage(ProtobufMessageType.CONSOLE2MANAGER_CLEAR_TEST_FLAG, builder.build());
    }

    public static ProtobufMessage buildSetTestFlag(int serverId) {
        IntValueProto.Builder builder = IntValueProto.newBuilder();
        builder.setValue(serverId);

        return new ProtobufMessage(ProtobufMessageType.CONSOLE2MANAGER_SET_TEST_FLAG, builder.build());
    }

    public static ProtobufMessage buildForceSetServerShutdownStatus(int serverId) {
        IntValueProto.Builder builder = IntValueProto.newBuilder();
        builder.setValue(serverId);

        return new ProtobufMessage(ProtobufMessageType.CONSOLE2MANAGER_FORCE_SET_SERVER_SHUTDOWN_STATUS, builder.build());
    }

    public static ProtobufMessage buildLoadServerConfig(int serverId) {
        IntValueProto.Builder builder = IntValueProto.newBuilder();
        builder.setValue(serverId);

        return new ProtobufMessage(ProtobufMessageType.CONSOLE2MANAGER_LOAD_SERVER_CONFIG, builder.build());
    }

    public static ProtobufMessage buildSetDate(long date) {
        LongValueProto.Builder builder = LongValueProto.newBuilder();
        builder.setValue(date);

        return new ProtobufMessage(ProtobufMessageType.CONSOLE2MANAGER_SET_SYSTEM_DATE, builder.build());
    }

    public static ProtobufMessage buildSetTime(long time) {
        LongValueProto.Builder builder = LongValueProto.newBuilder();
        builder.setValue(time);

        return new ProtobufMessage(ProtobufMessageType.CONSOLE2MANAGER_SET_SYSTEM_TIME, builder.build());
    }

    public static ProtobufMessage buildGetDateTime() {
        return new ProtobufMessage(ProtobufMessageType.CONSOLE2MANAGER_GET_SYSTEM_DATETIME, null);
    }
}
