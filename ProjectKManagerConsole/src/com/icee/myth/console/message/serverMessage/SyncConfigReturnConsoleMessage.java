/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.console.message.serverMessage;

import com.icee.myth.common.message.serverMessage.SimpleMessage;

/**
 *
 * @author liuxianke
 */
public class SyncConfigReturnConsoleMessage extends SimpleMessage {
    public final int result;

    public SyncConfigReturnConsoleMessage(int result) {
        super(MessageType.CONSOLE_SYNCCONFIG_RETURN);

        this.result = result;
    }
}
