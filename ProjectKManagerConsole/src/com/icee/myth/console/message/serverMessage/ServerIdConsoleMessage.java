/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.console.message.serverMessage;

import com.icee.myth.common.message.serverMessage.SimpleMessage;

/**
 *
 * @author liuxianke
 */
public class ServerIdConsoleMessage extends SimpleMessage {
    public final int serverId;

    public ServerIdConsoleMessage(MessageType type, int serverId) {
        super(type);

        this.serverId = serverId;
    }
}
