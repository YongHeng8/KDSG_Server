/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.console.message.serverMessage;

import com.icee.myth.common.message.serverMessage.SimpleMessage;

/**
 *
 * @author liuxianke
 */
public class DateTimeMessage extends SimpleMessage {
    public final long datetime;

    public DateTimeMessage(long datetime) {
        super(MessageType.CONSOLE_DATE_TIME);

        this.datetime = datetime;
    }
}
