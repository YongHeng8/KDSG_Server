/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.console.message.serverMessage;

import com.icee.myth.common.message.serverMessage.SimpleMessage;
import java.util.ArrayList;

/**
 *
 * @author liuxianke
 */
public class CommandConsoleMessage extends SimpleMessage {
    public final int cmdOpcode;
    public final ArrayList<String> strArgs;

    public CommandConsoleMessage(int cmdOpcode, ArrayList<String> strArgs) {
        super(MessageType.CONSOLE_COMMAND);

        this.cmdOpcode = cmdOpcode;
        this.strArgs = strArgs;
    }
}
