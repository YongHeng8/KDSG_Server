/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.console.message.serverMessage;

import com.icee.myth.common.message.serverMessage.SimpleMessage;
import com.icee.myth.protobuf.ExternalCommonProtocol.IntValuesProto;

/**
 *
 * @author liuxianke
 */
public class UninitOccupyInfoMessage extends SimpleMessage {
    public final IntValuesProto intValuesProto;

    public UninitOccupyInfoMessage(IntValuesProto intValuesProto) {
        super(MessageType.CONSOLE_UNINIT_OCCUPY_INFO);

        this.intValuesProto = intValuesProto;
    }
}
