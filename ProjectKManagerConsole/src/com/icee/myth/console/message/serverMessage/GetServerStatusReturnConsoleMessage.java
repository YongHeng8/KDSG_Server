/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.console.message.serverMessage;

import com.icee.myth.common.message.serverMessage.SimpleMessage;
import com.icee.myth.protobuf.ConsoleToManagerProtocol.ServerStatuses;

/**
 *
 * @author liuxianke
 */
public class GetServerStatusReturnConsoleMessage extends SimpleMessage {
    public final ServerStatuses statuses;

    public GetServerStatusReturnConsoleMessage(ServerStatuses statuses) {
        super(MessageType.CONSOLE_GET_SERVER_STATUS_RETURN);

        this.statuses = statuses;
    }
}
