/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.console.encoder;

import static org.jboss.netty.buffer.ChannelBuffers.*;
import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.MessageLite;
import com.icee.myth.common.protobufMessage.ProtobufMessage;
import com.icee.myth.utils.LogConsts;
import com.icee.myth.utils.MLogger;
import com.icee.myth.utils.StackTraceUtil;
import java.io.IOException;
import java.nio.ByteOrder;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.oneone.OneToOneEncoder;

/**
 *
 * @author liuxianke
 */
public class ConsoleToManagerEncoder extends OneToOneEncoder {

    public ConsoleToManagerEncoder() {
        super();
    }

    @Override
    protected Object encode(
            ChannelHandlerContext ctx, Channel channel, Object msg) throws Exception {
        if (msg instanceof ProtobufMessage) {
            return encodeProtobufMessage(channel, (ProtobufMessage)msg);
        } else {
            return msg;
        }
    }

    private Object encodeProtobufMessage(Channel channel, ProtobufMessage pm) {
        if (pm.payload instanceof MessageLite) {
            MessageLite ml = (MessageLite) pm.payload;
            int mlsize = ml.getSerializedSize();
            byte[] res = new byte[4 + mlsize];
            res[0] = (byte) ((pm.type >> 8) & 0xFF);
            res[1] = (byte) (pm.type & 0xFF);
            res[2] = (byte) ((mlsize >> 8) & 0xFF);
            res[3] = (byte) (mlsize & 0xFF);
            CodedOutputStream cos = CodedOutputStream.newInstance(res, 4, mlsize);
            try {
                ml.writeTo(cos);
            } catch (IOException ex) {
                MLogger.getlogger().log(LogConsts.LOGTYPE_NETERR, StackTraceUtil.getStackTrace(ex));
            }
            cos = null;
            return wrappedBuffer(res);
        } else if(pm.payload == null) {
            ChannelBuffer header = channel.getConfig().getBufferFactory().getBuffer(ByteOrder.BIG_ENDIAN, 4);
            header.writeShort((short)pm.type);
            header.writeShort((short)0);
            return header;
        }

        return pm;
    }
}
