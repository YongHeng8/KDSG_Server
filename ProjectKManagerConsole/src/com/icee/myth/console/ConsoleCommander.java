/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.console;

import com.icee.myth.common.message.serverMessage.Message;
import com.icee.myth.common.messageQueue.ServerMessageQueue;
import com.icee.myth.console.message.serverMessage.builder.ConsoleMessageBuilder;
import com.icee.myth.utils.Consts;
import com.icee.myth.utils.LinkedTransferQueue;
import com.icee.myth.utils.LogConsts;
import com.icee.myth.utils.MLogger;
import com.icee.myth.utils.StackTraceUtil;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * 命令输入线程
 * @author liuxianke
 */
public class ConsoleCommander implements Runnable {
    // 全局共享的消息队列
    private final LinkedTransferQueue<Message> messageQueue = ServerMessageQueue.queue();

    @Override
    public void run() {
        // 从标准输入读取控制命令
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            String commandString;
            while ((commandString = bufferedReader.readLine()) != null) {
                executeCmd(commandString.trim());
            }
        } catch (IOException ex) {
            MLogger.getlogger().debuglog(LogConsts.LOGLEVEL_ERROR, StackTraceUtil.getStackTrace(ex));
        }
    }

    private void executeCmd(String commandString) {
        if (commandString.length() > 0) {
            Scanner s = new Scanner(commandString.toString());
            int cmdOpcode = -1;
            ArrayList<String> strArgs = new ArrayList<String>();

            while (s.hasNext()) {
                String str = s.next();

                if (cmdOpcode == -1) {
                    cmdOpcode = getCmdOpcode(str);

                    if (cmdOpcode < 0) {
                        // 找不到对应的cmd
                        return;
                    }
                } else {
                    strArgs.add(str);
                }
            }

            messageQueue.offer(ConsoleMessageBuilder.buildCommandMessage(cmdOpcode, strArgs));
        }
    }

    private int getCmdOpcode(String str) {
        str = str.toLowerCase();

        if (str.equals("syncrun")) {
            return Consts.CONSOLE_COMMAND_RUN_SERVER;
        } else if (str.equals("shutdown")) {
            return Consts.CONSOLE_COMMAND_SHUTDOWN_SERVER;
        } else if (str.equals("run")) {
            return Consts.CONSOLE_COMMAND_DEBUG_RUNSERVER;
        } else if (str.equals("syncconfig")) {
            return Consts.CONSOLE_COMMAND_SYNCCONFIG;
        } else if (str.equals("showall")) {
            return Consts.CONSOLE_COMMAND_GET_SERVER_STATE;
        } else if (str.equals("gm")) {
            return Consts.CONSOLE_COMMAND_GM;
        } else if (str.equals("cleartestflag")) {
            return Consts.CONSOLE_COMMAND_CLEAR_TEST_FLAG;
        } else if (str.equals("settestflag")) {
            return Consts.CONSOLE_COMMAND_SET_TEST_FLAG;
        } else if (str.equals("clear")) {
            return Consts.CONSOLE_COMMAND_FORCE_SET_SERVER_TO_SHUTDOWN_STATUS;
        } else if (str.equals("loadsvrconfig")) {
            return Consts.CONSOLE_COMMAND_LOAD_SERVER_CONFIG;
        } else if (str.equals("setdate")) {
            return Consts.CONSOLE_COMMAND_SET_DATE;
        } else if (str.equals("settime")) {
            return Consts.CONSOLE_COMMAND_SET_TIME;
        } else if (str.equals("getdatetime")) {
            return Consts.CONSOLE_COMMAND_GET_DATETIME;
        }

        return -1;
    }
}
