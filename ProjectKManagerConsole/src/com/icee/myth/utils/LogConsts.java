/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.icee.myth.utils;

/**
 *
 * @author chencheng
 */
public class LogConsts {
    public static final int LOGFLUSH_INTERVAL = 5000; // 5 seconds
    public static final int LOGLEVEL_NOLOG = 0;
    public static final int LOGLEVEL_ERROR = 1;
    public static final int LOGLEVEL_INFO = 2;
    public static final int LOGLEVEL_DEBUG = 3;
    public static final int LOGLEVEL = LOGLEVEL_DEBUG;
    public static final String LOGPATHROOT = "log/";
    public static final int LOGTYPE_MONEY = 0;
    public static final int LOGTYPE_ITEM = 1;
    public static final int LOGTYPE_PET = 2;
    public static final int LOGTYPE_BEHAVIOR = 100;
    public static final int LOGTYPE_DEBUG = 200;
    public static final int LOGTYPE_DBERR = 300;
    public static final int LOGTYPE_NETERR = 301;
    public static final int LOGTYPE_GAMEERR = 302;
    public static final int LOG_CONSOLE_PRINT = 1;
    public static final int LOG_BUFFER_SIZE = 8192; // 8K
}
