/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.icee.myth.common.message.serverMessage;

/**
 *
 * @author liuxianke
 */
public interface Message {
    /**
     * 消息类型                         -- 消息说明
     * MAPSCENE_XXX                    -- XXX
     *
     */
    public enum MessageType {
        CONSOLE_MANAGER_CONNECT,
        CONSOLE_MANAGER_CLOSE,
        CONSOLE_COMMAND,
        CONSOLE_SERVER_START_OK,
        CONSOLE_SERVER_SHUTDOWN_OK,
        CONSOLE_SYNCCONFIG_RETURN,
        CONSOLE_GET_SERVER_STATUS_RETURN,
        CONSOLE_FORCE_SET_SERVER_SHUTDOWN_STATUS_OK,
        CONSOLE_FORCE_SET_SERVER_SHUTDOWN_STATUS_ERROR,
        CONSOLE_LOAD_SERVER_CONFIG_OK,
        CONSOLE_LOAD_SERVER_CONFIG_ERROR,
        CONSOLE_BATTLE_WIN_RATE_RESULT,
        CONSOLE_CARD_DRAW_RATE_RESULT,
        CONSOLE_UNINIT_OCCUPY_INFO,
        CONSOLE_FIGHTING_OCCUPY_INFO,
        CONSOLE_DATE_TIME
    }

    public MessageType getType();
}
